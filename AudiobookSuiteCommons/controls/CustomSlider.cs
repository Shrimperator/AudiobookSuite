﻿using System.Windows.Controls;
using System.Windows.Input;

namespace AudiobookSuite.Commons.controls
{
    /// <summary>
    /// slider with some additional functionality
    /// </summary>
    public class CustomSlider : Slider
    {
        /// <summary>
        /// allows pressing anywhere on the slider track, then holding mouse button.
        /// By default, the user would have to click again to then drag the slider thumb
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var newEventArgs = new MouseButtonEventArgs(Mouse.PrimaryDevice, 0, MouseButton.Left);
                newEventArgs.RoutedEvent = PreviewMouseLeftButtonDownEvent;
                
                RaiseEvent(newEventArgs);
            }
        }
    }
}
