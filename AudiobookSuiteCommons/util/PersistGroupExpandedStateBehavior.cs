﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using AudiobookSuite.Commons;
using Microsoft.Xaml.Behaviors;

namespace AudiobookSuite
{
    public abstract class PersistGroupExpandedStateBehavior : Behavior<Expander>
    {
        public static readonly DependencyProperty GroupNameProperty = DependencyProperty.Register(
            "GroupName",
            typeof(object),
            typeof(PersistGroupExpandedStateBehavior),
            new PropertyMetadata(default(object)));

        public string GroupName
        {
            get{ return (string)this.GetValue(GroupNameProperty);}
            set{ this.SetValue(GroupNameProperty, value);}
        }

        public abstract Dictionary<string, bool> GetExpanderStates();

        protected override void OnAttached()
        {
            base.OnAttached();

            bool expanded = this.GetExpandedState();

            // delay to defeat race condition, where expander states are reset if viewsource is initialized later than group behavior
            Utility.DelayedAction(new Action(() => 
            {
                this.AssociatedObject.IsExpanded = expanded;
            }));

            this.AssociatedObject.Expanded += this.OnExpanded;
            this.AssociatedObject.Collapsed += this.OnCollapsed;
        }

        protected override void OnDetaching()
        {
            this.AssociatedObject.Expanded -= this.OnExpanded;
            this.AssociatedObject.Collapsed -= this.OnCollapsed;

            base.OnDetaching();
        }

        private bool GetExpandedState()
        {
            var expanderStates = GetExpanderStates();

            if (!expanderStates.ContainsKey(this.GroupName.ToLower()))
                return true;

            return expanderStates[this.GroupName.ToLower()];
        }

        private void OnCollapsed(object sender, RoutedEventArgs e)
        {
            this.SetExpanded(false);
        }

        private void OnExpanded(object sender, RoutedEventArgs e)
        {
            this.SetExpanded(true);
        }

        private void SetExpanded(bool expanded)
        {
            var expanderStates = GetExpanderStates();
            string groupNameLower = this.GroupName.ToLower();

            if (expanderStates.ContainsKey(groupNameLower))
                expanderStates[groupNameLower] = expanded;
            else
                expanderStates.Add(groupNameLower, expanded);
        }
    }
}
