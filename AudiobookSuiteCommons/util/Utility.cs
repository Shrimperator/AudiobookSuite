﻿using ATL;
using AudiobookSuite.lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.Threading.Tasks;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// static class containing various utility methods
    /// </summary>
    public static class Utility
    {
        private static Regex WrittenOutNumbersRegex;

        static Utility()
        {
            List<string> reverseNumbers = new List<string>(WrittenOutNumbers);
            reverseNumbers.Reverse();

            WrittenOutNumbersRegex = new Regex(String.Join('|', reverseNumbers),
                RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        /// <summary>
        /// given the full path of an audiofile, attempts to read the first embedded
        /// image file
        /// </summary>
        /// <param name="path">full path of an AudioFile with an embedded image file in it</param>
        /// <returns>a bitmap of the first embedded image, or null</returns>
        public static BitmapImage ReadFirstPicture(string path)
        {
            Track atlTrack;
            try
            {
                atlTrack = new Track(path);

                if (atlTrack == null)
                {
                    DebugLog.WriteLine("Warning: ATL .NET failed to read file '" + path + "' - null");
                    return null;
                }
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Warning: ATL .NET failed to read file '" + path + "' - exception");
                return null;
            }

            if (atlTrack.EmbeddedPictures.Count > 0)
            {
                var pic = atlTrack.EmbeddedPictures[0];

                try
                {
                    MemoryStream ms = new MemoryStream(pic.PictureData);
                    ms.Seek(0, SeekOrigin.Begin);

                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.StreamSource = ms;
                    bitmap.EndInit();

                    return bitmap;
                }
                catch(Exception e)
                {
                    DebugLog.WriteLine("error: failed to read image file '" + path + "'");
                }
            }

            return null;
        }

        /// <summary>
        /// when sorting by filename ascending, 4 should be sorted before 10, but by default
        /// will be sorted after 10. This method makes the string
        /// numerical, so sorting works as intended. 4 would become 00004 and 10 would become 00010
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        public static string MakeStringNumerical(string strIn, int numPadding = 4)
        {
            return Regex.Replace(strIn, @"\d+", n => n.Value.PadLeft(numPadding, '0'));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="child"></param>
        public static void RemoveChild(this DependencyObject parent, UIElement child)
        {
            var panel = parent as Panel;
            if (panel != null)
            {
                panel.Children.Remove(child);
                return;
            }

            var decorator = parent as Decorator;
            if (decorator != null)
            {
                if (decorator.Child == child)
                {
                    decorator.Child = null;
                }
                return;
            }

            var contentPresenter = parent as ContentPresenter;
            if (contentPresenter != null)
            {
                if (contentPresenter.Content == child)
                {
                    contentPresenter.Content = null;
                }
                return;
            }

            var contentControl = parent as ContentControl;
            if (contentControl != null)
            {
                if (contentControl.Content == child)
                {
                    contentControl.Content = null;
                }
                return;
            }
        }

        /// <summary>
        /// get minimum number of edits required to convert string 'first' to string 'second'. Used
        /// to determine the similarity of strings, such as filenames
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static int GetLevenshteinDistance(string first, string second)
        {
            if (first == null || second == null)
                return int.MaxValue;

            if (first.Length == 0)
                return second.Length;

            if (second.Length == 0)
                return first.Length;

            var d = new int[first.Length + 1, second.Length + 1];
            for (var i = 0; i <= first.Length; i++)
            {
                d[i, 0] = i;
            }

            for (var j = 0; j < second.Length; j++)
            {
                d[0, j] = j;
            }

            for (var i = 1; i <= first.Length; i++)
            {
                for (var j = 1; j <= second.Length; j++)
                {
                    var cost = (second[j - 1] == first[i - 1]) ? 0 : 1;
                    d[i, j] = Min(
                        d[i - 1, j] + 1,
                        d[i, j - 1] + 1,
                        d[i - 1, j - 1] + cost
                        );
                }
            }

            return d[first.Length, second.Length];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e1"></param>
        /// <param name="e2"></param>
        /// <param name="e3"></param>
        /// <returns></returns>
        private static int Min(int e1, int e2, int e3) =>
                Math.Min(Math.Min(e1, e2), e3);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="delta"></param>
        /// <returns></returns>
        public static bool IsNearlyEqual(double a, double b, double delta)
        {
            return Math.Abs(a - b) <= delta;
        }

        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dependencyItem"></param>
        /// <returns></returns>
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject dependencyItem) where T : DependencyObject
        {
            if (dependencyItem != null)
            {
                for (var index = 0; index < VisualTreeHelper.GetChildrenCount(dependencyItem); index++)
                {
                    var child = VisualTreeHelper.GetChild(dependencyItem, index);
                    if (child is T dependencyObject)
                    {
                        yield return dependencyObject;
                    }

                    foreach (var childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        /// <summary>
        /// copies data from one object to another. Obviously, the objects should
        /// be of the same type
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        public static void CopyProperties(this object source, object destination)
        {
            // Iterate the Properties of the destination instance and
            // populate them from their source counterparts
            PropertyInfo[] destinationProperties = destination.GetType().GetProperties();
            foreach (PropertyInfo destinationPi in destinationProperties)
            {
                PropertyInfo sourcePi = source.GetType().GetProperty(destinationPi.Name);
                destinationPi.SetValue(destination, sourcePi.GetValue(source, null), null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static T GetVisualChild<T>(DependencyObject parent) where T : Visual
        {
            T child = default(T);

            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);

                child = v as T;
                if (child == null)
                    child = GetVisualChild<T>(v);

                if (child != null)
                    break;
            }
            return child;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ScrollViewer GetScrollViewer(UIElement element)
        {
            if (element == null) return null;

            ScrollViewer retour = null;
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element) && retour == null; i++)
            {
                if (VisualTreeHelper.GetChild(element, i) is ScrollViewer)
                    retour = (ScrollViewer)(VisualTreeHelper.GetChild(element, i));
                else
                    retour = GetScrollViewer(VisualTreeHelper.GetChild(element, i) as UIElement);
            }
            return retour;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataGrid"></param>
        /// <returns></returns>
        public static double GetDataGridScroll(DataGrid dataGrid)
        {
            ScrollViewer scroll = GetScrollViewer(dataGrid);
            if (scroll != null)
                return scroll.VerticalOffset;
            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataGrid"></param>
        /// <param name="verticalOffset"></param>
        public static void SetDataGridScroll(DataGrid dataGrid, double verticalOffset = 0)
        {
            ScrollViewer scroll = GetScrollViewer(dataGrid);
            if (scroll != null)
            {
                scroll.ScrollToVerticalOffset(verticalOffset);
                scroll.UpdateLayout();
            }
        }

        private static List<string> WrittenOutNumbers = new List<string>()
        {
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen",
            "twenty",
            "thirty",
            "fourty",
            "fifty",
            "sixty",
            "seventy",
            "eighty",
            "ninety",
            "hundred",
        };

        /// <summary>
        /// removes simple written out numbers in the input string, eg. "one", "two"...
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        public static string RemoveFullNumbers(string strIn)
        {
            return WrittenOutNumbersRegex.Replace(strIn, "");
        }

        /// <summary>
        /// converts simple written out numbers in the input string to digits, eg. "one" to "1"
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        public static string ConvertFullNumbersToDigits(string strIn)
        {
            for (int i = WrittenOutNumbers.Count - 1; i >= 0; i--)
            {
                string numberStr = WrittenOutNumbers[i];

                // replace as-is
                if (i < 20)
                    strIn = strIn.Replace(numberStr, i.ToString());
                // only add one digit, rest should be replaced as-is
                else
                {
                    // with -18, "twenty", which is at index 20 becomes 2, "thirty" becomes 3 etc.
                    strIn = strIn.Replace(numberStr, (i - 18).ToString());
                }
            }

            return strIn;
        }

        /// <summary>
        /// string to double, with a few more parameters to sanitize user input
        /// </summary>
        /// <param name="sIn"></param>
        /// <param name="positive"></param>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns></returns>
        public static double SanitizeDouble(string sIn, bool positive = true, double minimum = 0.0, double maximum = -1.0)
        {
            double valueOut = 0.0;
            try
            {
                valueOut = double.Parse(sIn.Replace(",", "."), CultureInfo.InvariantCulture);
                if (positive && valueOut < 0)
                    valueOut = -valueOut;

                if (minimum < maximum)
                {
                    if (valueOut < minimum)
                        valueOut = minimum;
                    else if (valueOut > maximum)
                        valueOut = maximum;
                }
            }
            catch (Exception e)
            {
                valueOut = 0.0;
            }

            return valueOut;
        }

        /// <summary>
        /// string to float, with a few more parameters to sanitize user input
        /// </summary>
        /// <param name="sIn"></param>
        /// <param name="positive"></param>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns></returns>
        public static float SanitizeFloat(string sIn, bool positive = true, float minimum = 0.0f, float maximum = -1.0f)
        {
            float valueOut = 0.0f;
            try
            {
                valueOut = float.Parse(sIn.Replace(",", "."), CultureInfo.InvariantCulture);
                if (positive && valueOut < 0)
                    valueOut = -valueOut;

                if (minimum < maximum)
                {
                    if (valueOut < minimum)
                        valueOut = minimum;
                    else if (valueOut > maximum)
                        valueOut = maximum;
                }
            }
            catch (Exception e)
            {
                valueOut = 0.0f;
            }

            return valueOut;
        }

        /// <summary>
        /// string to int, with a few more parameters to sanitize user input
        /// </summary>
        /// <param name="sIn"></param>
        /// <param name="positive"></param>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns></returns>
        public static int SanitizeInt(string sIn, bool positive = true, int minimum = 0, int maximum = -1)
        {
            int valueOut = 0;
            try
            {
                valueOut = int.Parse(sIn.Replace(",", "."), CultureInfo.InvariantCulture);
                if (positive && valueOut < 0)
                    valueOut = -valueOut;

                if (minimum < maximum)
                {
                    if (valueOut < minimum)
                        valueOut = minimum;
                    else if (valueOut > maximum)
                        valueOut = maximum;
                }
            }
            catch (Exception e)
            {
                valueOut = 0;
            }

            return valueOut;
        }

        public static async void DelayedAction(Action action)
        {
            await Task.Delay(1);
            Application.Current.Dispatcher.Invoke(action);
        }
    }
}
