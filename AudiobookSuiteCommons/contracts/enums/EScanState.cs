﻿namespace AudiobookSuite.Commons
{
    /// <summary>
    /// 
    /// </summary>
    public enum EScanState
    {
        /// <summary>
        /// not scanning
        /// </summary>
        Idle,
        /// <summary>
        /// finding files on disk and ordering them by their containing folder path
        /// </summary>
        Gathering,
        /// <summary>
        /// creating AudioFiles and Audiobooks from gathered data
        /// </summary>
        Indexing,
        /// <summary>
        /// sorting newly created Audiobooks' registered files - tracknumber
        /// </summary>
        Sorting,
        /// <summary>
        /// caching new audiofile duration data
        /// </summary>
        CacheDurations,
    }
}
