﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using SharpDX.XInput;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// 
    /// </summary>
    public interface IKeyBinding
    {
        public string Identifier { get; set; }
        public List<ITrigger> Triggers { get; set; }
        public bool RequireAppFocus { get; set; }

        /**/

        public bool IsPressed();
        public bool IsPressed(KeyPressedArgs instigator);
        public bool IsPressed(GamepadButtonFlags instigator);
    }

    public interface ITrigger
    {
        public Nullable<System.Windows.Input.Key> TriggerKeyboard { get; set; }
        public Nullable<GamepadButtonFlags> TriggerGamepad { get; set; }

        /**/

        public void SetTrigger(Key triggerKeyboard);
        public void SetTrigger(GamepadButtonFlags triggerGamepad);

        public bool IsKeyboardTrigger();
        public bool IsGamepadTrigger();

        public string GetTriggerDisplayString();
    }
}
