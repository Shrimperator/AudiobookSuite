﻿namespace AudiobookSuite.Commons
{
    /// <summary>
    /// contains various instances that should be reused across the application
    /// </summary>
    public interface IAppGlobals
    {
        /// <summary>
        /// handles scanning audiofiles from disk, bundling into audiobooks and
        /// storing library data
        /// </summary>
        public IAudiobookScanner audiobookScanner { get; set; }
        /// <summary>
        /// global manager for undo/redo actions
        /// </summary>
        public IUndoRedo undoRedo { get; set; }
        /// <summary>
        /// global settings instance. Contains all user settings
        /// </summary>
        public ISettings settings { get; set; }
        /// <summary>
        /// WPF user control with media buttons and playback info. Defined globally, so
        /// the instance can be reused for every page that has PlayerControls
        /// </summary>
        public IPlayerControls playerControls { get; set; }
        /// <summary>
        /// handles grouping audiobooks (or potentially other data) into groups
        /// </summary>
        public IGroupManager groupManager { get; set; }
        /// <summary>
        /// allows listening for keyboard input, even when the app is not focused, such as for
        /// media key recognition
        /// </summary>
        public LowLevelKeyboardListener keyboardListener { get; set; }

        /// <summary>
        /// called after all managers have been initialized. Important, if for example
        /// trying to add an event handler for another manager from the constructor of
        /// one manager. Instead of the constructor, use FinishedInit in that case to make
        /// sure, references in App.Globals are set and initialized
        /// </summary>
        public void FinishedInit();
    }
}
