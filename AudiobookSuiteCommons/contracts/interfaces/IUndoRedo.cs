﻿using System;
using System.Collections.Generic;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="undoRedo"></param>
    /// <param name="action"></param>
    /// <returns></returns>
    public delegate bool UndoRedoActionDelegate(IUndoRedo undoRedo, IUndoRedoAction action);

    /// <summary>
    /// 
    /// </summary>
    public interface IUndoRedoAction
    {
        /// <summary>
        /// action to perform when undoing this action
        /// </summary>
        public UndoRedoActionDelegate UndoAction { get; set; }
        /// <summary>
        /// action to perform when redoing this action
        /// </summary>
        public UndoRedoActionDelegate RedoAction { get; set; }
        /// <summary>
        /// identifier of this action, so specific undo/redo entries can be removed later
        /// </summary>
        public string Identifier { get; set; }
    }

    /// <summary>
    /// API for easy undo redo management
    /// </summary>
    public interface IUndoRedo
    {
        /// <summary>
        /// contains all current undo/redo actions
        /// </summary>
        public List<IUndoRedoAction> UndoStack { get; set; }
        /// <summary>
        /// current index within UndoStack
        /// </summary>
        public int UndoIdx { get; set; }
        /// <summary>
        /// how many undo/redo actions to allow until oldest entry should be overridden
        /// </summary>
        public int MaxUndoActions { get; set; }

        /// <summary>
        /// called when all App.Globals have been initialized and reistered
        /// </summary>
        public void FinishedInit();

        /// <summary>
        /// add new action to the top of the UndoStack
        /// </summary>
        /// <param name="undoAction"></param>
        public void PushAction(IUndoRedoAction undoAction);

        /// <summary>
        /// removes all actions from the UndoStack that have the given identifier
        /// </summary>
        /// <param name="identifier"></param>
        public void RemoveActions(string identifier);

        /// <summary>
        /// get the undo action at the current index
        /// </summary>
        /// <returns></returns>
        public IUndoRedoAction GetCurrentUndoAction();

        /// <summary>
        /// 
        /// </summary>
        public void Undo();

        /// <summary>
        /// 
        /// </summary>
        public void Redo();

        /// <summary>
        /// removes all Undo/Redo entries above the given index, not including supplied index itself
        /// </summary>
        /// <param name="idx"></param>
        public void RemoveAbove(int idx);

        /// <summary>
        /// true if there is an undo action available
        /// </summary>
        /// <returns></returns>
        public bool CanUndo();

        /// <summary>
        /// true if there is a redo action available
        /// </summary>
        /// <returns></returns>
        public bool CanRedo();

        /// <summary>
        /// 
        /// </summary>
        public event Action PushedAction;
        /// <summary>
        /// 
        /// </summary>
        public void OnPushedAction();

        /// <summary>
        /// 
        /// </summary>
        public event Action RemovedActions;
        /// <summary>
        /// 
        /// </summary>
        public void OnRemovedActions();

        /// <summary>
        /// 
        /// </summary>
        public event Action UndoAction;
        /// <summary>
        /// 
        /// </summary>
        public void OnUndo();

        /// <summary>
        /// 
        /// </summary>
        public event Action RedoAction;
        /// <summary>
        /// 
        /// </summary>
        public void OnRedo();
    }
}
