﻿using AudiobookSuite.Commons;
using AudiobookSuite.lib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AudiobookSuite
{
    public class AudiobookScanner : IAudiobookScanner, INotifyPropertyChanged
    {
        public ObservableCollection<IAudiobook> AudioBookList { get; set; }
        public BackgroundWorker? ScanWorker { get; set; }
        public List<string> ScanWorkerPaths { get; set; }
        public EScanState ScanState { get; set; }
        public int ScanProgressPercentage { get; set; }

        /* temporary list of (audio-)filepaths, yet to be iterated over, sorted into single directories */
        public List<IEnumerable<string>> FilePathDirsOpen { get; set; }
        public Dictionary<string, IAudioFile> AudioFilesMasterList { get; set; }

        /* temporary list of newly created audiobooks. Used to determine on which audiobooks
         to call track sorting */
        public List<IAudiobook> NewAudiobooks { get; set; }
        public List<IAudioFile> NewAudioFiles { get; set; }

        public bool HasAnythingChanged { get; set; }
        public List<IAudioFile> FilesFailedToLoad { get; set; }

        public AudiobookScanner()
        {
            AudioBookList = new ObservableCollection<IAudiobook>();

            NewAudiobooks = new List<IAudiobook>();
            NewAudioFiles = new List<IAudioFile>();

            ScanWorkerPaths = new List<string>();

            FilePathDirsOpen = new List<IEnumerable<string>>();
            AudioFilesMasterList = new Dictionary<string, IAudioFile>();

            FilesFailedToLoad = new List<IAudioFile>();
        }

        public void FinishedInit()
        {
        }

        public int GetFreeID()
        {
            int lastId = -1;
            for (int i = 0; i < AudioBookList.Count; i++)
            {
                IAudiobook audiobook = AudioBookList[i];
                int id = audiobook.UniqueID;

                if (Math.Abs(id - lastId) > 1)
                {
                    return lastId + 1;
                }
                lastId = id;
            }

            return AudioBookList.Count;
        }

        /* mark changed for next autosave. As long as nothing has changed, don't autosave */
        public void MarkChanged()
        {
            HasAnythingChanged = true;
        }

        public void MarkNothingChanged()
        {
            HasAnythingChanged = false;
        }

        public IAudiobook GetAudiobookByTitle(string title)
        {
            return AudioBookList.FirstOrDefault(item => item.Title == title);
        }

        public IAudiobook GetAudiobookByID(int id)
        {
            return AudioBookList.FirstOrDefault(item => item.UniqueID == id);
        }

        public bool SaveBooksToFile(ESaveCause saveCause = ESaveCause.Manual, string? path = null)
        {
            if (HasAnythingChanged || saveCause != ESaveCause.Autosave)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    DefaultValueHandling = DefaultValueHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                };
                settings.Converters.Add(new NoIndentationJsonConverter());

                string jsonString = JsonConvert.SerializeObject(AudioBookList, Formatting.Indented, settings);

                string appdataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string storagePath = appdataPath + "\\AudiobookSuite\\";

                if (path == null)
                    path = storagePath + IAudiobookScanner.AudiobooksFilePath;
                string dir = Path.GetDirectoryName(path);

                Directory.CreateDirectory(dir);
                System.IO.File.WriteAllText(path, jsonString);

                if (saveCause == ESaveCause.Autosave)
                    MarkNothingChanged();

                return true;
            }

            return false;
        }

        public bool LoadBooksFromFile(string? path = null)
        {
            if (path == null)
            {
                string appdataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string storagePath = appdataPath + "\\AudiobookSuite\\";
                path = storagePath + IAudiobookScanner.AudiobooksFilePath;
            }

            if (!File.Exists(path))
            {
                DebugLog.WriteLine("Failed to load library file: '" + path + "' - file doesn't exist");
                return false;
            }

            var settings = new JsonSerializerSettings
            {
                Error = (se, ev) => { ev.ErrorContext.Handled = true; },
                ObjectCreationHandling = ObjectCreationHandling.Replace
            };

            List<Audiobook> audiobooks = JsonConvert.DeserializeObject<List<Audiobook>>(File.ReadAllText(path), settings);

            if (audiobooks == null || audiobooks.Count == 0)
                return false;

            RestoreLoadedBooks(audiobooks);

            OnLoadedAudiobooks();

            /* values were just reloaded, don't need to save them again */
            MarkNothingChanged();

            return true;
        }

        private void RestoreLoadedBooks(List<Audiobook> audiobooks)
        {
            ClearAudiobooks();
            FilesFailedToLoad.Clear();

            foreach (IAudiobook loadedBook in audiobooks)
            {
                if (loadedBook == null)
                    continue;

                List<IAudioFile> loadedFiles = loadedBook.AudioFiles;
                /* audiofiles might already exist, in which case loaded file needs to be replace by
                 * existing file in masterlist. To make it easier, just clear records and set again further down */
                loadedBook.AudioFiles = new List<IAudioFile>();

                foreach (IAudioFile loadedFile in loadedFiles)
                {
                    if (loadedFile != null)
                    {
                        loadedFile.MoveToAudiobook(loadedBook);

                        if (!File.Exists(loadedFile.AudioFilePath))
                        {
                            DebugLog.WriteLine("Warning: Loaded file couldn't be found at '" + loadedFile.AudioFilePath + "'");
                            FilesFailedToLoad.Add(loadedFile);
                            continue;
                        }

                        if (!(AudioFilesMasterList.ContainsKey(loadedFile.AudioFilePath)))
                        {
                            AudioFilesMasterList.Add(loadedFile.AudioFilePath, loadedFile);
                        }
                    }
                }

                /* make sure to create new group references, otherwise audiobooks will point
                 * towards groups that aren't registered with the GroupManager, and things will
                 * become weird. Basically, copy the group list, clear it, iterate it and re-establish 
                 * references by names from copied group */
                List<ISortingGroup> loadedGroups = new List<ISortingGroup>(loadedBook.Groups);
                loadedBook.Groups.Clear();

                foreach (ISortingGroup group in loadedGroups)
                {
                    App.Globals.groupManager.AddToGroup(group.GroupName, loadedBook);
                    loadedBook.AddToGroup(group.GroupName);
                }

                AddAudiobook(loadedBook);

                loadedBook.PostLoad();
            }
        }

        public void ClearAudiobooks()
        {
            foreach (IAudiobook audiobook in AudioBookList)
            {
                App.Globals.groupManager.RemoveFromAllGroups(audiobook);

                foreach (IAudioFile file in audiobook.AudioFiles)
                    AudioFilesMasterList.Remove(file.AudioFilePath);

                OnRemovedAudiobook(audiobook);
            }

            AudioBookList.Clear();
            MarkChanged();
        }

        public void RemoveAudiobook(IAudiobook audiobook)
        {
            if (audiobook != null)
            {
                App.Globals.groupManager.RemoveFromAllGroups(audiobook);

                foreach (IAudioFile file in audiobook.AudioFiles)
                    AudioFilesMasterList.Remove(file.AudioFilePath);

                lock (AudioBookList)
                {
                    AudioBookList.Remove(audiobook);
                }

                MarkChanged();
                OnRemovedAudiobook(audiobook);
            }
        }

        public void RemoveAudioFile(IAudioFile file)
        {
            if (file.AssociatedAudiobook != null)
                file.AssociatedAudiobook.RemoveAudioFile(file);

            AudioFilesMasterList.Remove(file.AudioFilePath);

            MarkChanged();
            OnRemovedAudioFile(file);
        }

        /* adds an audiobook to AudioBookList and makes sure the list remains sorted by ID */
        public void AddAudiobookIDSorted(IAudiobook audiobook)
        {
            if (audiobook.UniqueID < 0)
                audiobook.UniqueID = GetFreeID();

            int lastIdx = -1;
            for (int i = 0; i < AudioBookList.Count; i++)
            {
                IAudiobook otherAudiobook = AudioBookList[i];
                int id = otherAudiobook.UniqueID;

                if (id > audiobook.UniqueID)
                {
                    AudioBookList.Insert(lastIdx + 1, audiobook);
                    return;
                }
                
                lastIdx = i;
            }

            AudioBookList.Add(audiobook);
        }

        public void AddAudiobook(IAudiobook audiobook)
        {
            lock (AudioBookList)
            {
                AddAudiobookIDSorted(audiobook);
            }

            MarkChanged();
        }

        public string GetFreeTitle(string title)
        {
            if (title == null)
                title = "";

            string workingTitle = title;
            int numFails = 1;
            while (DoesTitleExist(workingTitle))
            {
                workingTitle = title + " Copy";
                if (numFails > 1)
                    workingTitle += "(" + numFails + ")";
                numFails++;
            }

            return workingTitle;
        }

        public bool DoesTitleExist(string title)
        {
            return AudioBookList.FirstOrDefault(item => item.Title == title) != null;
        }

        public bool AttemptMergeAudiobooks(IAudiobook first, IAudiobook second)
        {
            if (first == null || first.AudiobookPath == null ||
                second == null || second.AudiobookPath == null)
                return false;

            try
            {
                Uri firstPath = new Uri(first.AudiobookPath);
                Uri secondPath = new Uri(second.AudiobookPath);

                if (firstPath == secondPath)
                {
                    foreach (AudioFile file in first.AudioFiles)
                    {
                        second.AddAudioFile(file);
                    }
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }

        public IAudiobook CreateAudiobook(string title, string? audiobookPath = null)
        {
            IAudiobook newAudiobook = new Audiobook();
            newAudiobook.AudiobookPath = audiobookPath;

            /* audiobook with same title exists already. Attempt to merge them, if not, create new audiobook with 'copy' appended*/
            IAudiobook matchingTitle = GetAudiobookByTitle(title);
            if (matchingTitle != null)
            {
                if (AttemptMergeAudiobooks(newAudiobook, matchingTitle))
                {
                    DebugLog.WriteLine("Scanner merged two audiobooks with same title: '" + title + "'");
                    return matchingTitle;
                }
            }

            newAudiobook.Title = GetFreeTitle(title);
            newAudiobook.UniqueID = GetFreeID();

            lock (AudioBookList)
            {
                AddAudiobookIDSorted(newAudiobook);
            }

            NewAudiobooks.Add(newAudiobook);

            return newAudiobook;
        }

        public bool IsValidTitle(string title)
        {
            if (title == null)
                return false;

            /* remove whitespaces */
            string sanitizedTitle = Regex.Replace(title, @"\s+", "");

            if (sanitizedTitle.Length == 0)
                return false;

            RegexOptions options = RegexOptions.IgnoreCase;

            /* pattern for invalid title names */
            string pattern = "^(chapter[0-9]+$)|^([0-9]+$)";
            if (Regex.IsMatch(sanitizedTitle, pattern, options))
                return false;

            return true;
        }

        /* - - - */

        public bool IsScanning()
        {
            return ScanWorker != null && ScanWorker.IsBusy;
        }

        public void Cancel()
        {
            if (!IsScanning())
                return;

            if (ScanWorker != null)
            {
                ScanWorker.CancelAsync();

                DebugLog.WriteLine("Canceling ScanWorker");
            }
        }

        public void ScanDirectory(string path)
        {
            if (IsScanning())
            {
                DebugLog.WriteLine("Already scanning, wait for thread to finish");
                return;
            }

            if (!Directory.Exists(path))
            {
                DebugLog.WriteLine("Scan failed: path doesn't exist '" + path + "'");
                return;
            }

            ScanWorker = new BackgroundWorker();
            ScanWorker.WorkerReportsProgress = true;
            ScanWorker.WorkerSupportsCancellation = true;
            ScanWorker.DoWork += ScanDoWork;
            ScanWorker.ProgressChanged += ScanDirectoryProgressChanged;
            ScanWorker.RunWorkerCompleted += ScanDirectoryCompleted;

            ScanWorkerPaths.Clear();
            ScanWorkerPaths.Add(path);

            ScanWorker.RunWorkerAsync();
        }

        public void ScanDirectories(List<string> paths)
        {
            if (IsScanning())
            {
                DebugLog.WriteLine("Already scanning, wait for thread to finish");
                return;
            }

            foreach (string path in paths)
            {
                if (!Directory.Exists(path))
                {
                    DebugLog.WriteLine("Scan failed: path doesn't exist '" + path + "'");
                    return;
                }
            }

            ScanWorker = new BackgroundWorker();
            ScanWorker.WorkerReportsProgress = true;
            ScanWorker.WorkerSupportsCancellation = true;
            ScanWorker.DoWork += ScanDoWork;
            ScanWorker.ProgressChanged += ScanDirectoryProgressChanged;
            ScanWorker.RunWorkerCompleted += ScanDirectoryCompleted;

            ScanWorkerPaths = paths;

            ScanWorker.RunWorkerAsync();
        }

        private async void ScanDoWork(object sender, DoWorkEventArgs e)
        {
            await ScanDirectoryAsync(sender, e);
        }

        public async Task ScanDirectoryAsync(object sender, DoWorkEventArgs e)
        {

            DebugLog.WriteLine("Scanning Directories...");
            OnStartedScanning();

            if (!ScanWorker.CancellationPending)
            {
                ReportScannerProgress(EScanState.Gathering, 0);
                GatherFilesFromDirs(ScanWorkerPaths);
            }

            List<IAudioFile> scannedFiles = new List<IAudioFile>();
            if (!ScanWorker.CancellationPending)
            {
                ReportScannerProgress(EScanState.Indexing, 25);
                scannedFiles = IndexFilesInDir();
            }

            if (!ScanWorker.CancellationPending)
            {
                ReportScannerProgress(EScanState.Sorting, 90);
                SortNewTrackNumbers();
            }

            if (!ScanWorker.CancellationPending)
            {
                ReportScannerProgress(EScanState.CacheDurations, 95);
                CacheAudioFileDurations(scannedFiles);
            }

            /* temporary data, not needed anymore */
            FilePathDirsOpen.Clear();

            OnFinishedScanning();

            NewAudiobooks.Clear();
            NewAudioFiles.Clear();

            ReportScannerProgress(EScanState.Idle, 100);
        }

        public async Task<List<IAudioFile>> AddAudioFiles(List<string> filePaths, IAudiobook? audiobook = null)
        {
            List<IAudioFile> newFiles = IndexFilesAllowExisting(filePaths);

            if (audiobook == null)
                CreateAudiobooksFromDictionary(newFiles);
            else
            {
                foreach (AudioFile audioFile in newFiles)
                {
                    audiobook.AddAudioFile(audioFile);
                }
            }

            CacheAudioFileDurations(newFiles);

            return newFiles;
        }

        public async Task<IAudioFile> AddAudioFile(string filePath, IAudiobook? audiobook = null)
        {
            if (AudioFilesMasterList.ContainsKey(filePath))
                return null;

            IAudioFile file = CreateAudioFileFromPath(filePath);

            if (audiobook == null)
            {
                List<IAudioFile> fileList = new List<IAudioFile>();
                fileList.Add(file);
                CreateAudiobooksFromDictionary(fileList);
            }
            else
            {
                audiobook.AddAudioFile(file);
            }

            CacheAudioFileDurations(new List<IAudioFile>() { file });

            return file;
        }

        public void ReportScannerProgress(EScanState state, int progress)
        {
            ScanState = state;
            ScanProgressPercentage = progress;

            if (ScanWorker != null && ScanWorker.IsBusy)
            {
                ScanWorker.ReportProgress(progress);
            }
        }

        public void ScanDirectoryProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            OnUpdatedScanning(ScanState, e.ProgressPercentage);
        }

        public void ScanDirectoryCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ScanWorker = null;
            OnUpdatedAudiobook();
            ScanState = EScanState.Idle;
        }

        /* creates a temporary list of (audio-)filepaths, yet to be iterated over, sorted into single directories */
        public void GatherFilesFromDir(string path, bool recursive = true, int depth = 0)
        {
            if (path == null || path.Length == 0 || depth > App.Globals.settings.FolderSearchDepth)
                return;

            DebugLog.WriteLine("Indexing files in directory '" + path + "'");

            /* gets all audiofiles within this folder, excluding files within subfolders */
            IEnumerable<string> dirFilePaths = Directory.GetFiles(path).Where(s => IAudiobookScanner.SupportedMediaExtensions.Contains(Path.GetExtension(s).ToLower()));
            if (dirFilePaths.Count() > 0)
            {
                try
                {
                    FilePathDirsOpen.Add(dirFilePaths);
                }
                catch (System.UnauthorizedAccessException ex)
                {
                    DebugLog.WriteLine("Error: UnauthorizedAccessException at '" + path + "'");
                }
            }

            if (recursive)
            {
                string[] subdirectories = Directory.GetDirectories(path);
                foreach (string subdirectory in subdirectories)
                {
                    GatherFilesFromDir(subdirectory, true, depth + 1);

                    if (IsScanning() && ScanWorker.CancellationPending)
                        return;
                }
            }
        }

        /* creates a temporary list of (audio-)filepaths, yet to be iterated over, sorted into single directories */
        public void GatherFilesFromDirs(List<string> paths, bool recursive = true, int depth = 0)
        {
            foreach (string path in paths)
                GatherFilesFromDir(path);
        }

        /*  */
        public List<IAudioFile> IndexFilesInDir()
        {
            List<IAudioFile> scannedFiles = new List<IAudioFile>();

            foreach (IEnumerable<string> dir in FilePathDirsOpen)
            {
                List<IAudioFile> newFiles = IndexFiles(dir);
                scannedFiles.AddRange(newFiles);

                CreateAudiobooksFromDictionary(newFiles);

                if (ScanWorker.CancellationPending)
                    return scannedFiles;
            }

            return scannedFiles;
        }

        public List<IAudioFile> IndexFiles(IEnumerable<string> dir)
        {
            List<IAudioFile> newFiles = new List<IAudioFile>();

            foreach (string filePath in dir)
            {
                lock (AudioFilesMasterList)
                {
                    if (AudioFilesMasterList.ContainsKey(filePath))
                        continue;
                }

                IAudioFile file = CreateAudioFileFromPath(filePath);

                if (file != null)
                    newFiles.Add(file);

                if (IsScanning() && ScanWorker.CancellationPending)
                    break;
            }

            return newFiles;
        }

        /* adds file to output list, even if they already exist in masterlist. In that case, return files from masterlist */
        public List<IAudioFile> IndexFilesAllowExisting(IEnumerable<string> dir)
        {
            List<IAudioFile> newFiles = new List<IAudioFile>();

            foreach (string filePath in dir)
            {
                lock (AudioFilesMasterList)
                {
                    if (AudioFilesMasterList.ContainsKey(filePath))
                    {
                        newFiles.Add(AudioFilesMasterList[filePath]);
                        continue;
                    }
                }

                IAudioFile file = CreateAudioFileFromPath(filePath);

                if (file != null)
                {
                    newFiles.Add(file);
                }

                if (IsScanning() && ScanWorker.CancellationPending)
                    break;
            }

            return newFiles;
        }

        public void CreateAudiobooksFromDictionary(List<IAudioFile> dictionary)
        {
            if (dictionary == null || dictionary.Count == 0)
                return;
            if (dictionary.Count == 1)
            {
                var data = dictionary[0].LoadAudioFileData();
                IAudiobook newAudiobook = CreateAudiobookFromFile(dictionary[0], data);
                /* updates title, authors, narrators, icon etc. pass data so it doesn't have to be cached again */
                newAudiobook.RefreshMetadata(data);
                return;
            }

            DebugLog.WriteLine("Creating Audiobooks for folder - files: " + dictionary.Count);

            /* first cache data for all audiofiles in this dictionary (folder), because algorithm later
            will have to iterate over files multiple times, but caching multiple times would be 
            very slow */
            List<IAudioFileData> fileDatas = new List<IAudioFileData>();
            for (int i = 0; i < dictionary.Count; i++)
            {
                fileDatas.Add(dictionary[i].LoadAudioFileData());

                if (IsScanning() && ScanWorker.CancellationPending)
                    return;
            }

            for (int i = 0; i < dictionary.Count; i++)
            {
                IAudioFile file = dictionary[i];
                if (file.AssociatedAudiobook != null)
                    continue;

                IAudioFileData data = fileDatas[i];
                List<IAudioFile> matchingFiles = new List<IAudioFile>();

                for (int j = 0; j < dictionary.Count; j++)
                {
                    if (i == j)
                        continue;

                    IAudioFile otherFile = dictionary[j];
                    if (otherFile.AssociatedAudiobook != null)
                        continue;

                    IAudioFileData otherData = fileDatas[j];

                    int distance = GetFilenameLevenshteinDistance(data, otherData);
                    bool sameAlbum = !(String.IsNullOrEmpty(data.Album)) && data.Album == otherData.Album;

                    /* group into same audiobook */
                    if (sameAlbum || distance <= IAudiobookScanner.MaxStrDistanceBetweenFilenames)
                        matchingFiles.Add(otherFile);

                    if (ScanWorker.CancellationPending)
                        return;
                }

                IAudiobook newAudiobook = CreateAudiobookFromFile(file, data);
                for (int k = 0; k < matchingFiles.Count; k++)
                {
                    IAudioFile matchingFile = matchingFiles[k];
                    matchingFile.MoveToAudiobook(newAudiobook);

                    if (IsScanning() && ScanWorker.CancellationPending)
                        return;
                }

                /* updates title, authors, narrators, icon etc. pass data so it doesn't have to be cached again */
                newAudiobook.RefreshMetadata(data);
            }
        }

        private string SanitizeComparisonFileName(string fileName)
        {
            fileName = Path.GetFileNameWithoutExtension(fileName);
            fileName = fileName.Replace(" ", "");
            fileName = fileName.Replace("-", "");
            return fileName;
        }

        private int GetFilenameLevenshteinDistance(IAudioFileData data, IAudioFileData otherData)
        {
            string fileName = SanitizeComparisonFileName(data.FileName);
            string otherFileName = SanitizeComparisonFileName(otherData.FileName);

            return Utility.GetLevenshteinDistance(fileName, otherFileName);
        }

        /* optional data parameter is there so you can pass in already
         cached file data. Use if possible to improve performance by avoiding
        to read file multiple times */
        public IAudiobook CreateAudiobookFromFile(IAudioFile file, IAudioFileData? data = null)
        {
            if (data == null)
                data = file.LoadAudioFileData();

            IAudiobook newAudiobook = CreateAudiobook(data.Title, data.DirectoryPath);

            file.MoveToAudiobook(newAudiobook);

            if (App.Globals.settings.ScanGenres)
                CreateGenreGroups(newAudiobook, data);
            if (App.Globals.settings.CreateParentFolderGroup)
                CreateFolderGroup(newAudiobook, data);

            MarkChanged();
            OnAddedAudiobook(newAudiobook);

            return newAudiobook;
        }

        public List<string> CreateGenreGroups(IAudiobook audiobook, IAudioFileData? data = null)
        {
            if (audiobook == null)
                return null;

            if (data == null)
                data = audiobook.CacheSampleData();

            List<string> newGroups = new List<string>();

            foreach (string genre in data.Genres)
            {
                if (ShouldIgnoreGenreGroup(genre))
                    continue;

                if (genre != null && genre.Length > 0)
                    newGroups.Add(genre);

                if (IsScanning() && ScanWorker.CancellationPending)
                    break;
            }

            MergeGenres(newGroups);
            return AddGenreGroups(audiobook, newGroups);
        }

        public string CreateFolderGroup(IAudiobook audiobook, IAudioFileData? data = null)
        {
            if (audiobook == null)
                return null;

            if (data == null)
                data = audiobook.CacheSampleData();

            string lastFolderName = new DirectoryInfo(audiobook.AudiobookPath).Name;

            if (lastFolderName == null || lastFolderName.Length == 0 ||
                ShouldIgnoreGenreGroup(lastFolderName) ||
                IsScanning() && ScanWorker.CancellationPending)
                return "";

            App.Globals.groupManager.AddGroup(lastFolderName);
            audiobook.AddToGroup(lastFolderName);

            DebugLog.WriteLine("CreateGenreGroups: added group " + lastFolderName);

            return lastFolderName;
        }

        /* IAudiobookScanner.MergeLists ist a 2D list containing genres that should be merged
         * when creating groups out of genres eg. because they are different names of the same genre. 
         * They are merged into the first index within the merge list. Example:
         * 
         * new List<string>() { "comedy", "humor", "humour" } 
         * 
         * if the genre name is "humor" or "humour", it will become "comedy" */
        private void MergeGenres(List<string> genres)
        {
            if (genres == null)
                return;

            ISettings settings = App.Globals.settings;
            if (settings == null || settings.SettingsGroupDefinitions == null)
                return;

            GroupDefinitions groupDefs = settings.SettingsGroupDefinitions;

            for (int i = 0; i < genres.Count; i++)
            {
                string genre = genres[i];

                foreach (List<string> mergeList in groupDefs.MergeLists)
                {
                    int foundIdx = mergeList.FindIndex(item => item.ToLower() == genre.ToLower());
                    /* MergeList contains this genre, but it is not first index of mergeList, then
                     * merge into first index of mergeList */
                    if (foundIdx >= 1)
                    {
                        DebugLog.WriteLine("Merging: " + genre + " into " + mergeList[0]);

                        genres[i] = mergeList[0];
                        return;
                    }
                }
            }
        }

        private List<string> AddGenreGroups(IAudiobook audiobook, List<string> genres)
        {
            List<string> newGroups = new List<string>();

            foreach (string genre in genres)
            {
                if (genre != null && genre.Length > 0)
                {
                    App.Globals.groupManager.AddGroup(genre);
                    if (audiobook.AddToGroup(genre))
                    {
                        newGroups.Add(genre);
                    }
                }
            }

            return newGroups;
        }

        public bool ShouldIgnoreGenreGroup(string genre)
        {
            ISettings settings = App.Globals.settings;
            if (settings == null || settings.SettingsGroupDefinitions == null)
                return false;

            GroupDefinitions groupDefs = settings.SettingsGroupDefinitions;

            return groupDefs.IgnoreList.Contains(genre.ToLower());
        }

        public IAudioFile? CreateAudioFileFromPath(string path)
        {
            if (AudioFilesMasterList.ContainsKey(path))
                return AudioFilesMasterList[path];

            if (!IsValidPath(path))
            {
                DebugLog.WriteLine("Warning: Failed to create audiofile from path - invalid path");
                return null;
            }

            AudioFile audioFile = new AudioFile() { AudioFilePath = path };
            AudioFilesMasterList.Add(path, audioFile);
            NewAudioFiles.Add(audioFile);

            OnAddedAudioFile(audioFile);

            return audioFile;
        }

        public bool IsValidPath(string path)
        {
            return path != null && path.Length > 0;
        }

        /* gets a valid title for a new audiobook this audiofile should be
         * grouped into. Call with an audiofile contained in the audiobook as a sample */
        public string? GetTitleForAudiobook(IAudioFile audioFile, IAudioFileData? data = null)
        {
            if (audioFile == null)
                return null;

            if (data == null)
                data = audioFile.LoadAudioFileData();

            string fileName = audioFile.GetFileName();
            if (IsValidTitle(data.Album))
            {
                return data.Album;
            }
            else if (IsValidTitle(data.Title))
            {
                return data.Title;
            }
            else if (IsValidTitle(fileName))
            {
                return MakeTitle(fileName);
            }

            /* audiofile title is not a good fit, use name of the directory the audiobook is in */
            return MakeTitle(Path.GetFileName(Path.GetDirectoryName(audioFile.AudioFilePath)));
        }

        public string MakeTitle(string titleIn)
        {
            string titleOut = titleIn;

            RegexOptions options = RegexOptions.IgnoreCase;

            /* pattern for invalid title matches */
            string pattern = @"(?<![0-9] *)(- *[0-9]+)|
                |( *-* *chapter *-* *-*[0-9]+-* *[0-9]*)|
                |( *-* *part *-* *-*[0-9]+-* *[0-9]*)|
                |( *-* *file *-* *-*[0-9]+-* *[0-9]*)|
                |^([0-9]+)|
                |(\([0-9]*\)$)";
            titleOut = Regex.Replace(titleOut, pattern, "", options);

            /* multiple whitespaces two singular whitespaces */
            titleOut = Regex.Replace(titleOut, @"\s+", " ", options);

            /* expressions top to bottom
                - (a hyphen followed by a digit), but not preceded by a digit eg. "-02" but not "1-5"
                - "chapter" followed by zero-to-multiple whitespaces and/or hyphens followed by one-to-multiple digits,
                        followed by one or multiple hyphens/digits eg. "chapter - 02" or "chapter 1-2"
                - "part" followed by zero-to-multiple whitespaces and/or hyphens followed by one-to-multiple digits,
                        followed by one or multiple hyphens/digits eg. "part - 02" or "part 1-2"
                - "file" followed by zero-to-multiple whitespaces and/or hyphens followed by one-to-multiple digits,
                        followed by one or multiple hyphens/digits eg. "file - 02" or "file 1-2"
                - a digit preceding the entire string
                - a bracket with zero-to-multiple digits in it at the end of the string
             */

            return titleOut.Trim();
        }

        public void SortNewTrackNumbers()
        {
            foreach (IAudiobook audiobook in NewAudiobooks)
            {
                SortAudiobook(audiobook);

                if (IsScanning() && ScanWorker.CancellationPending)
                    break;
            }
        }

        public Task CacheAudioFileDurations(List<IAudioFile> files)
        {
            if (files.Count == 0)
                return null;

            return Task.Run(() =>
            {
                List<Task> tasks = new List<Task>();

                /* caching duration is slow, so duration is stored in the save file. If, after
                 loading no valid duration has been restored, try caching again */
                foreach (IAudioFile file in files)
                {
                    if (file.Duration.TotalSeconds < 1)
                    {
                        tasks.Add(file.CacheDurationAsync());
                    }
                }

                if (tasks.Count > 0)
                {
                    Task.WaitAll(tasks.ToArray());

                    List<IAudiobook> closedList = new List<IAudiobook>();
                    foreach (IAudioFile file in files)
                    {
                        if (file.AssociatedAudiobook != null && !closedList.Contains(file.AssociatedAudiobook))
                        {
                            file.AssociatedAudiobook.RefreshChapterData();
                            closedList.Add(file.AssociatedAudiobook);
                        }
                    }
                }
            });
        }

        public void SortAudiobook(IAudiobook audiobook)
        {
            audiobook.AutoSortAudioFiles();
        }

        public EScanState GetScannerState()
        {
            return ScanState;
        }

        public int GetScannerProgress()
        {
            return ScanProgressPercentage;
        }

        public event UpdatedScannerProgressHandler UpdatedScanning;
        public void OnUpdatedScanning(EScanState scanState, int progress)
        {
            UpdatedScanning?.Invoke(scanState, progress);
        }

        public event Action FinishedScanning;
        public void OnFinishedScanning()
        {
            DebugLog.WriteLine("Finished scanning directory");
            FinishedScanning?.Invoke();
        }

        public event Action StartedScanning;
        public void OnStartedScanning()
        {
            StartedScanning?.Invoke();
        }

        public event Action UpdatedAudiobook;
        public void OnUpdatedAudiobook()
        {
            UpdatedAudiobook?.Invoke();
        }

        public event AddedAudiobookHandler AddedAudiobook;
        public void OnAddedAudiobook(IAudiobook newBook)
        {
            AddedAudiobook?.Invoke(newBook);
        }

        public event AddedAudiobookHandler RemovedAudiobook;
        public void OnRemovedAudiobook(IAudiobook newBook)
        {
            RemovedAudiobook?.Invoke(newBook);
        }

        public event AddedAudioFileHandler AddedAudioFile;
        public void OnAddedAudioFile(IAudioFile newBook)
        {
            AddedAudioFile?.Invoke(newBook);
        }

        public event AddedAudioFileHandler RemovedAudioFile;
        public void OnRemovedAudioFile(IAudioFile newBook)
        {
            RemovedAudioFile?.Invoke(newBook);
        }

        public event Action LoadedAudiobooks;
        public void OnLoadedAudiobooks()
        {
            LoadedAudiobooks?.Invoke();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

