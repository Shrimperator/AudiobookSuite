﻿using AudiobookSuite.Commons;
using System;
using System.Collections.Generic;

namespace AudiobookSuite.lib
{
    public class UndoRedoAction : IUndoRedoAction
    {
        public Commons.UndoRedoActionDelegate UndoAction { get; set; }
        public Commons.UndoRedoActionDelegate RedoAction { get; set; }

        public string Identifier { get; set; }

        public UndoRedoAction(UndoRedoActionDelegate undoAction, UndoRedoActionDelegate redoAction, string identifier = "default")
        {
            UndoAction = undoAction;
            RedoAction = redoAction;
            Identifier = identifier;
        }
    }

    public class UndoRedo : IUndoRedo
    {
        public List<IUndoRedoAction> UndoStack { get; set; }
        public int UndoIdx { get; set; } = 0;

        public int MaxUndoActions { get; set; } = 50;

        public UndoRedo()
        {
            UndoStack = new List<IUndoRedoAction>();
        }

        public void FinishedInit()
        {

        }

        public void PushAction(IUndoRedoAction undoAction)
        {
            RemoveAbove(UndoIdx);
            UndoStack.Add(undoAction);

            if (UndoStack.Count > MaxUndoActions)
            {
                UndoStack.RemoveAt(0);
            }

            UndoIdx = UndoStack.Count - 1;

            OnPushedAction();
        }

        public void RemoveActions(string identifier)
        {
            bool removedSomething = false;
            for (int i = 0; i < UndoStack.Count; i++)
            {
                IUndoRedoAction action = UndoStack[i];

                if (action.Identifier == identifier)
                {
                    if (i <= UndoIdx)
                        UndoIdx--;

                    removedSomething = true;
                    UndoStack.RemoveAt(i);
                    i--;
                }
            }
            if (removedSomething)
                OnRemovedActions();
        }

        public IUndoRedoAction GetCurrentUndoAction()
        {
            if (UndoIdx < UndoStack.Count && UndoIdx >= 0)
                return UndoStack[UndoIdx];
            return null;
        }

        public void Undo()
        {
            if (CanUndo())
            {
                IUndoRedoAction action = UndoStack[UndoIdx];

                if (action.UndoAction(this, action))
                {
                    UndoIdx--;

                    OnUndo();
                }
            }
        }

        public void Redo()
        {
            if (CanRedo())
            {
                IUndoRedoAction action = UndoStack[UndoIdx + 1];

                if (action.RedoAction(this, action))
                {
                    UndoIdx++;

                    OnRedo();
                }
            }
        }

        public void RemoveAbove(int idx)
        {
            for (int i = UndoStack.Count - 1; i > idx && i >= 0; i--)
            {
                UndoStack.RemoveAt(i);
            }
        }

        public bool CanUndo()
        {
            return UndoIdx >= 0 && UndoStack.Count > 0;
        }

        public bool CanRedo()
        {
            return UndoIdx < UndoStack.Count - 1 && UndoStack.Count > 0;
        }

        public event Action PushedAction;
        public void OnPushedAction()
        {
            if (PushedAction != null)
                PushedAction();
        }

        public event Action RemovedActions;
        public void OnRemovedActions()
        {
            if (RemovedActions != null)
                RemovedActions();
        }

        public event Action UndoAction;
        public void OnUndo()
        {
            if (UndoAction != null)
                UndoAction();
        }

        public event Action RedoAction;
        public void OnRedo()
        {
            if (RedoAction != null)
                RedoAction();
        }
    }
}
