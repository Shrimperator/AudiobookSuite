﻿using AudiobookSuite.Commons;
using System;
using System.Globalization;
using System.Speech.Synthesis;

namespace AudiobookSuite.lib
{
    public class TextToSpeech
    {
        public SpeechSynthesizer SpeechSynth { get; set; }

        public string SpeechCulture { get; set; }

        private int VoiceTypeIdx = -1;

        /**/

        public TextToSpeech()
        {
            SpeechSynth = new SpeechSynthesizer();

            SpeechSynth.SetOutputToDefaultAudioDevice();

            SetVoice("en-US");
        }

        ~TextToSpeech()
        {
            App.Globals.settings.LoadedSettings -= OnLoadedSettings;
            App.Globals.settings.ChangeOption -= OnChangeOption;
        }

        /**/

        public void FinishedInit()
        {
            App.Globals.settings.LoadedSettings += OnLoadedSettings;
            App.Globals.settings.ChangeOption += OnChangeOption;
        }

        public void OnChangeOption(string identifier)
        {
            switch (identifier)
            {
                case nameof(App.Globals.settings.TTSVoiceType):
                    ResetVoice();
                    break;

                case nameof(App.Globals.settings.Volume):
                    SetTTSVolume(App.Globals.settings.Volume);
                    break;

                case nameof(App.Globals.settings.IsTTSEnabled):
                    if (!App.Globals.settings.IsTTSEnabled)
                        SpeechSynth.SpeakAsyncCancelAll();
                    break;
            }
        }

        public void OnLoadedSettings()
        {
            ResetVoice();
        }

        public void SetTTSVolume(int volume)
        {
            SpeechSynth.Volume = volume;
        }

        public void SetTTSVoiceType(ETTSVoiceType voiceType)
        {
            App.Globals.settings.TTSVoiceType = voiceType;
            ResetVoice();
        }

        public ETTSVoiceType GetTTSVoiceType()
        {
            return App.Globals.settings.TTSVoiceType;
        }

        public void ResetVoice()
        {
            SetVoice(SpeechCulture);
        }

        public bool SetVoice(string cultureStr)
        {
            var culture = CultureInfo.GetCultureInfo(cultureStr);
            if (culture == null)
            {
                DebugLog.WriteLine("error: TextToSpeech.SetVoice failed to find cultureInfo '" + cultureStr + "'");
                return false;
            }

            var voices = SpeechSynth.GetInstalledVoices(culture);
            if (voices == null)
            {
                DebugLog.WriteLine("error: TextToSpeech.SetVoice found no installed voices");
                return false;
            }

            int voiceTypeIdx = (int)GetTTSVoiceType();
            if (voiceTypeIdx == VoiceTypeIdx)
            {
                DebugLog.WriteLine("call to ResetVoice ignored: voice already set");
                return false;
            }
            VoiceTypeIdx = voiceTypeIdx;

            if (voiceTypeIdx < voices.Count && voiceTypeIdx >= 0)
            {
                var voiceInfo = voices[voiceTypeIdx].VoiceInfo;

                // culture has a voice of the desired type
                SpeechSynth.SelectVoice(voiceInfo.Name);
                SpeechCulture = cultureStr;

                DebugLog.WriteLine("info: TextToSpeech voice set to '" + voiceInfo.Name + "'");

                return true;
            }
            else if (voices.Count > 0)
            {
                var voiceInfo = voices[0].VoiceInfo;

                // culture doesn't have desired voice type, but does have another voice
                SpeechSynth.SelectVoice(voiceInfo.Name);
                SpeechCulture = cultureStr;

                DebugLog.WriteLine("info: TextToSpeech voice set to '" + voiceInfo.Name + "'");

                return true;
            }

            // culture doesn't have an associated voice at all
            return false;
        }

        public void Speak(string speech)
        {
            if (!App.Globals.settings.IsTTSEnabled)
                return;
            try
            {
                SpeechSynth.SpeakAsyncCancelAll();
                SpeechSynth.SpeakAsync(speech);
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("warning: TextToSpeach.Speak encountered an exception. The system either doesn't support TTS, or no voice was available for the required culture.");
            }
        }
    }
}
