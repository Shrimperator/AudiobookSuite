﻿using AudiobookSuite.Commons;
using Newtonsoft.Json;
using System;

namespace AudiobookSuite
{
    [JsonObject(MemberSerialization.OptIn)]
    class BookMark : IBookMark
    {
        [JsonProperty]
        public string Identifier { get; set; }
        [JsonProperty]
        public TimeSpan Position { get; set; }

        public IAudiobook AssociatedAudiobook { get; set; }

        public BookMark(IAudiobook associatedAudiobook, string identifier, TimeSpan position)
        {
            Identifier = identifier;
            Position = position;
            AssociatedAudiobook = associatedAudiobook;
        }
    }
}
