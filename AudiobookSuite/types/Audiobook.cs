﻿using AudiobookSuite.Commons;
using AudiobookSuite.lib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AudiobookSuite
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Audiobook : IAudiobook, INotifyPropertyChanged
    {
        static string ImageFileFilters = "*.jpg,*.gif,*.png,*.bmp,*.jpe,*.jpeg,*.wmf,*.emf,*.xbm,*.ico,*.eps,*.tif,*.tiff,*.g01,*.g02,*.g03,*.g04,*.g05,*.g06,*.g07,*.g08";

        private string _Title = "";
        [DefaultValue("")]
        [JsonProperty("Title")]
        public string Title
        {
            get { return _Title; }
            set
            {
                _Title = value;
                NotifyPropertyChanged(nameof(Title));
            }
        }

        private int _UniqueID;
        [DefaultValue(-1)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int UniqueID
        {
            get { return _UniqueID; }
            set { _UniqueID = value; NotifyPropertyChanged(nameof(_UniqueID)); }
        }

        private DateTime? _CreationDate;
        [JsonProperty("CreationDate")]
        public DateTime? CreationDate
        {
            get { return _CreationDate; }
            set { _CreationDate = value; NotifyPropertyChanged(nameof(CreationDate)); }
        }

        private DateTime? _LastPlayedDate;
        [JsonProperty("LastPlayedDate")]
        public DateTime? LastPlayedDate
        {
            get { return _LastPlayedDate; }
            set { _LastPlayedDate = value; NotifyPropertyChanged(nameof(LastPlayedDate)); }
        }

        [DefaultValue("")]
        [JsonProperty("Path")]
        public string AudiobookPath { get; set; }

        /* index within AudioFiles list of currently played file */
        public int AudioFileIdx { get; set; }

        /* like AudioFilePosition only using total time instead of time in current audiofile */

        private TimeSpan _AudiobookPosition;
        [JsonProperty("Pos")]
        public TimeSpan AudiobookPosition
        {
            get { return _AudiobookPosition; }
            set
            {
                if (_AudiobookPosition.TotalMilliseconds != value.TotalMilliseconds)
                {
                    _AudiobookPosition = value;
                    if (App.Globals.audiobookScanner != null)
                        App.Globals.audiobookScanner.MarkChanged();
                }
            }
        }

        private bool _IsPlaying;
        public bool IsPlaying
        {
            get { return _IsPlaying; }
            set { _IsPlaying = value; NotifyPropertyChanged(nameof(IsPlaying)); }
        }

        private bool _IsFinished;
        public bool IsFinished
        {
            get { return _IsFinished; }
            set { _IsFinished = value; NotifyPropertyChanged(nameof(IsFinished)); }
        }

        private bool _IsHidden;
        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsHidden
        {
            get { return _IsHidden; }
            set { _IsHidden = value; NotifyPropertyChanged(nameof(IsHidden)); }
        }

        private List<string> _Authors;
        [JsonProperty("Authors")]
        public List<string> Authors
        {
            get { return _Authors; }
            set {
                _Authors = value;
                RefreshJoinedAuthors();
                NotifyPropertyChanged(nameof(Authors));
            }
        }

        private List<string> _Narrators;
        [JsonProperty("Narrators")]
        public List<string> Narrators
        {
            get { return _Narrators; }
            set {
                _Narrators = value;
                RefreshJoinedNarrators();
                NotifyPropertyChanged(nameof(Narrators));
            }
        }

        /* this method is automatically recognized by JsonConverter, ignore array if empty */
        public bool ShouldSerializeAuthors() => Authors != null && Authors.Count > 0;

        /* this method is automatically recognized by JsonConverter, ignore array if empty */
        public bool ShouldSerializeNarrators() => Narrators != null && Narrators.Count > 0;

        private string _IconPath;
        [DefaultValue("")]
        [JsonProperty("Icon")]
        public string IconPath
        {
            get { return _IconPath; }
            set
            {
                _IconPath = value;
                NotifyPropertyChanged(nameof(IconPath));
            }
        }

        public double Played { get; set; }

        private TimeSpan _FullDuration;
        /* duration of all audiofiles combined */
        public TimeSpan FullDuration
        {
            get { return _FullDuration; }
            set
            {
                _FullDuration = value;
                NotifyPropertyChanged(nameof(FullDuration));
                UpdatePlayedPercent();
            }
        }

        private TimeSpan _AudioFilePosition;

        public TimeSpan AudioFilePosition
        {
            get { return _AudioFilePosition; }
            set
            {
                if (_AudioFilePosition != value)
                {
                    _AudioFilePosition = value;

                    TimeSpan totalDuration = GetTotalDurationFromLocal(_AudioFilePosition, GetCurrentAudioFile());
                    AudiobookPosition = totalDuration;
                    UpdatePlayedPercent();

                    if (App.Globals.audiobookScanner != null)
                        App.Globals.audiobookScanner.MarkChanged();
                }
            }
        }

        /* when json is deserialized, files are interpreted as IAudioFile, but since IAudioFile is an
         * interface and cannot be instanciated, deserialization fails. JsonGenericTypeConverter
         * specifies, which type should be instanciated on deserialization */
        [JsonProperty("Files", ItemConverterType = typeof(JsonGenericTypeConverter<AudioFile>))]
        public List<IAudioFile> AudioFiles { get; set; }

        public string JoinedAuthors { get; set; } = "";
        public string JoinedNarrators { get; set; } = "";

        [JsonProperty(ItemConverterType = typeof(JsonGenericTypeConverter<SortingGroup>))]
        public List<ISortingGroup> Groups { get; set; }

        /* this method is automatically recognized by JsonConverter, ignore array if empty */
        public bool ShouldSerializeGroups() => Groups != null && Groups.Count > 0;

        public List<string> GroupStrings
        {
            get
            {
                List<string> listOut = new List<string>();
                foreach (ISortingGroup group in Groups)
                    listOut.Add(group.GroupName);

                return listOut;
            }
        }
        private List<IBookMark> _BookMarks;

        [JsonProperty("BookMarks", ItemConverterType = typeof(JsonGenericTypeConverter<BookMark>))]
        public List<IBookMark> BookMarks
        {
            get { return _BookMarks; }
            set
            {
                _BookMarks = value;
                OnChangedBookMarks();
            }
        }

        /* this method is automatically recognized by JsonConverter, ignore array if empty */
        public bool ShouldSerializeBookMarks() => BookMarks != null && BookMarks.Count > 0;

        [JsonProperty("Chapters", ItemConverterType = typeof(JsonGenericTypeConverter<AudioFileChapterData>))]
        public List<IAudioFileChapterData> Chapters { get; set; }

        /* this method is automatically recognized by JsonConverter, ignore array if empty */
        public bool ShouldSerializeChapters() => Chapters != null && Chapters.Count > 0;


        public Audiobook()
        {
            UniqueID = -1;
            Title = "";
            AudioFiles = new List<IAudioFile>();
            AudioFilePosition = new TimeSpan();
            FullDuration = new TimeSpan();
            AudiobookPosition = new TimeSpan();
            CreationDate = DateTime.Now;
            Authors = new List<string>();
            Narrators = new List<string>();
            Groups = new List<ISortingGroup>();
            Chapters = new List<IAudioFileChapterData>();
            BookMarks = new List<IBookMark>();
            IsHidden = false;

            AudioFileIdx = 0;
        }

        public bool AddToGroup(ISortingGroup group)
        {
            if (group != null && !Groups.Contains(group))
            {
                App.Globals.groupManager.AddToGroup(group, this);
                Groups.Add(group);

                return true;
            }

            return false;
        }

        public bool AddToGroup(string groupName)
        {
            /* already added to group? */
            if (Groups.Find(item => item.GroupName.ToLower() == groupName.ToLower()) != null)
                return false;

            ISortingGroup group = App.Globals.groupManager.GetGroup(groupName);

            return AddToGroup(group);
        }

        /* this is called from the GroupManager, using reflection, when the audiobook is
         * removed from its group. */
        public void GroupManager_RemoveFromGroup(object[] parameters)
        {
            if (parameters.Length > 0)
            {
                object firstParam = parameters[0];

                ISortingGroup group = (ISortingGroup)firstParam;

                if (group != null)
                {
                    int test = Groups.RemoveAll(item => item.GroupName.ToLower() == group.GroupName.ToLower());
                }
            }
        }

        public void RefreshJoinedStrings()
        {
            RefreshJoinedAuthors();
            RefreshJoinedNarrators();
        }

        public void RefreshJoinedAuthors()
        {
            JoinedAuthors = String.Join(", ", (Authors).ToArray());
        }

        public void RefreshJoinedNarrators()
        {
            JoinedNarrators = String.Join(", ", (Narrators).ToArray());
        }

        public void RefreshMetadata(IAudioFileData? data = null)
        {
            IAudioFile file = GetFirstValidAudioFile();
            if (file != null)
            {
                if (data == null)
                    data = file.LoadAudioFileData();

                Title = "";
                string baseTitle = App.Globals.audiobookScanner.GetTitleForAudiobook(file, data);
                Title = App.Globals.audiobookScanner.GetFreeTitle(baseTitle);

                Authors = data.Authors;
                Narrators = data.Narrators;

                RefreshChapterData();

                NotifyPropertyChanged("Authors");
                NotifyPropertyChanged("Narrators");

                RefreshIcon();
            }

            Authors.Sort();
            Narrators.Sort();
        }

        public void RefreshMetadataLimited()
        {
            RefreshChapterData();
        }

        public bool IsDoneCachingDuration()
        {
            foreach (IAudioFile file in AudioFiles)
                if (file.IsCachingDuration())
                    return false;

            return true;
        }

        public void RefreshChapterData()
        {
            if (!IsDoneCachingDuration())
                return;

            Chapters.Clear();

            TimeSpan currentDuration = new TimeSpan();
            int idxOffset = 0;
            for (int i = 0; i < AudioFiles.Count; i++)
            {
                IAudioFile file = AudioFiles[i];
                var chapterData = file.LoadChapterData(currentDuration, idxOffset);
                Chapters.AddRange(chapterData);

                currentDuration += file.Duration;
                idxOffset += chapterData.Count;
            }

            // no chapter metadata supplied but multiple audiofiles, create
            // chapter markers at file beginnings
            if (Chapters.Count == 0 && AudioFiles.Count > 1)
                CreateChapterDataFromFileBeginnings();

            NotifyPropertyChanged("Chapters");
            OnChangedChapters();
        }

        /// <summary>
        /// when no chapters are supplied but the audiobook has multiple audiofiles,
        /// create chapters at file beginnings
        /// </summary>
        public void CreateChapterDataFromFileBeginnings()
        {
            TimeSpan currentDuration = new TimeSpan();
            for (int i = 0; i < AudioFiles.Count; i++)
            {
                IAudioFile file = AudioFiles[i];
                Chapters.Add(new AudioFileChapterData((uint) currentDuration.TotalMilliseconds, "Chapter " + (i + 1), i));

                currentDuration += file.Duration;
            }
        }

        public bool HasIcon()
        {
            return IconPath != null && IconPath.Length > 0;
        }

        public bool CheckHasDuplicateOrInvalidTrackNumbers()
        {
            foreach (AudioFile file in AudioFiles)
            {
                int trackNum = file.TrackNumber;
                if (trackNum < 0)
                    return true;

                foreach (AudioFile otherFile in AudioFiles)
                {
                    if (file == otherFile)
                        continue;

                    int otherTrackNum = file.TrackNumber;

                    if (trackNum == otherTrackNum)
                        return true;
                }
            }

            return true;
        }

        public bool CheckHasValidTrackNumbers()
        {
            return !CheckHasDuplicateOrInvalidTrackNumbers();
        }

        public void AutoSortAudioFiles()
        {
            if (CheckHasValidTrackNumbers())
            {
                AudioFiles.Sort(
                    (first, second) =>
                        first.TrackNumber.CompareTo(second.TrackNumber));
            }
            else
            {
                AudioFiles.Sort(
                    (first, second) =>
                        Utility.MakeStringNumerical(first.GetFileName()).CompareTo(Utility.MakeStringNumerical(second.GetFileName())));
            }
        }

        /* either choose the audiofile itself as icon source, if an image file
        is embedded, or alternatively scan the containing folder for an image file.
        Audiofiles will be converted to BitMap format in ImageSourceConverter */
        public void RefreshIcon(IAudioFileData? data = null)
        {
            IAudioFile file = GetFirstValidAudioFile();

            if (data == null)
                data = file.LoadAudioFileData();

            IconPath = null;

            if (data != null)
            {
                if (data.ContainsPicture && (IconPath == null || IconPath.Length == 0))
                    IconPath = file.AudioFilePath;

                if (IconPath == null || IconPath.Length == 0)
                {
                    if (data.ContainsPicture)
                        IconPath = file.AudioFilePath;
                    else
                        ScanForIcon();
                }
            }
        }

        public IAudioFile GetFirstValidAudioFile()
        {
            foreach (IAudioFile otherFile in AudioFiles)
            {
                if (otherFile != null &&
                    otherFile.AudioFilePath != null)
                {
                    return otherFile;
                }
            }

            return null;
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void MarkRead()
        {
            if (AudiobookPosition == FullDuration)
                return;

            AudiobookPosition = FullDuration;

            if (AudioFiles.Count > 0)
            {
                AudioFileIdx = AudioFiles.Count - 1;
                IAudioFile lastFile = GetCurrentAudioFile();

                if (lastFile != null)
                {
                    AudioFilePosition = lastFile.Duration;
                }

                Played = 100.0;
                NotifyPropertyChanged("Played");

                if (App.Globals.audiobookScanner != null)
                    App.Globals.audiobookScanner.MarkChanged();
            }
        }

        public void MarkUnread()
        {
            if (IsFinished)
            {
                AudiobookPosition = TimeSpan.FromSeconds(0);
                AudioFilePosition = TimeSpan.FromSeconds(0);
                Played = 0.0;
                IsFinished = false;

                NotifyPropertyChanged("Played");

                if (App.Globals.audiobookScanner != null)
                    App.Globals.audiobookScanner.MarkChanged();
            }
        }

        public void UpdatePlayedPercent()
        {
            if (FullDuration.TotalSeconds == 0.0)
            {
                Played = 0.0;
            }
            else
            {
                Played = (AudiobookPosition.TotalSeconds / FullDuration.TotalSeconds) * 100;
                if (Played > 100.0)
                    Played = 100.0;
                else if (Played < 0.0)
                    Played = 0.0;

                double nearlyEqualDelta = 0.1;
                if (Played >= 100.0 || Utility.IsNearlyEqual(Played, 100.0, nearlyEqualDelta))
                    IsFinished = true;
                else
                    IsFinished = false;
            }

            NotifyPropertyChanged("Played");
        }

        public IAudioFile GetCurrentAudioFile()
        {
            if (AudioFileIdx < AudioFiles.Count && AudioFileIdx >= 0)
            {
                return AudioFiles[AudioFileIdx];
            }

            return null;
        }

        public bool NextFile()
        {
            if (AudioFileIdx + 1 < AudioFiles.Count)
            {
                AudioFileIdx++;
                AudioFilePosition = new TimeSpan();
                return true;
            }

            return false;
        }

        public bool PreviousFile()
        {
            AudioFileIdx--;
            if (AudioFileIdx < 0)
                AudioFileIdx = 0;

            AudioFilePosition = new TimeSpan();
            return true;
        }

        public void SetAudioFile(IAudioFile file)
        {
            int idx = AudioFiles.FindIndex(item => item == file);

            if (idx >= 0)
            {
                AudioFileIdx = idx;
            }
        }

        public void AddAudioFile(IAudioFile file)
        {
            if (file == null || file.AudioFilePath == null)
                return;

            if (!AudioFiles.Exists(item => item == file))
            {
                /* set audiobook path when first file is added. Might need to update when removing files too - todo */
                if (AudioFiles.Count == 0)
                {
                    SetAudiobookPath(Path.GetDirectoryName(file.AudioFilePath));
                }

                file.MoveToAudiobook(this);

                UpdateFullDuration();
            }
        }

        public bool InsertAudioFile(IAudioFile file, int targetIdx)
        {
            if (file == null || file.AudioFilePath == null)
                return false;

            /* set audiobook path when first file is added. Might need to update when removing files too - todo */
            if (AudioFiles.Count == 0)
            {
                string directoryName = Path.GetDirectoryName(file.AudioFilePath);

                if (directoryName != null)
                    SetAudiobookPath(directoryName);
            }

            if (!AudioFiles.Exists(item => item == file))
            {
                AudioFiles.Insert(targetIdx, file);
                UpdateFullDuration();

                return true;
            }

            /* allow autosave */
            if (App.Globals.audiobookScanner != null)
                App.Globals.audiobookScanner.MarkChanged();

            return false;
        }

        public void RemoveAudioFile(IAudioFile file)
        {
            AudioFiles.Remove(file);
            UpdateFullDuration();

            /* allow one autosave */
            if (App.Globals.audiobookScanner != null)
                App.Globals.audiobookScanner.MarkChanged();

            RefreshMetadataLimited();
        }

        /* moves audiofile from one index in the list to another, rearranging the other files accordingly */
        public bool RearrangeAudiofile(int idxFrom, int idxTo)
        {
            if (idxFrom < 0 || idxFrom >= AudioFiles.Count || idxTo < 0 || idxTo > AudioFiles.Count || idxFrom == idxTo)
                return false;

            IAudioFile fileFrom = AudioFiles[idxFrom];
            AudioFiles.Insert(idxTo, fileFrom);

            if (idxFrom > idxTo)
                AudioFiles.RemoveAt(idxFrom + 1);
            else
                AudioFiles.RemoveAt(idxFrom);

            /* allow one autosave */
            if (App.Globals.audiobookScanner != null)
                App.Globals.audiobookScanner.MarkChanged();

            RefreshMetadataLimited();

            return true;
        }

        public void SetAudiobookPath(string path)
        {
            AudiobookPath = path;
            ScanForIcon();

            /* allow one autosave */
            if (App.Globals.audiobookScanner != null)
                App.Globals.audiobookScanner.MarkChanged();
        }

        public void ScanForIcon()
        {
            if ((AudiobookPath != null && AudiobookPath.Length != 0) && (IconPath == null || IconPath.Length == 0))
            {
                if (!Directory.Exists(AudiobookPath))
                {
                    DebugLog.WriteLine("warning: ScanForIcon failed, because path '" + AudiobookPath + "' does not exist.");
                    return;
                }

                IEnumerable<string> imageFilePaths = Directory.GetFiles(AudiobookPath, "*.*", SearchOption.TopDirectoryOnly).Where(s => ImageFileFilters.Contains(Path.GetExtension(s).ToLower()));

                /* prefer ideal icon (file with fitting name), else use first valid file as icon */
                string idealIconPath = GetIdealIcon(imageFilePaths);
                if (idealIconPath.Length > 0)
                {
                    IconPath = idealIconPath;
                }
                else
                {
                    IconPath = GetAnyIcon(imageFilePaths);
                }
            }
        }

        /* gets a filename within the list of audiofiles to use as comparison */
        public string GetSampleFilename()
        {
            if (AudioFiles.Count > 0)
            {
                return AudioFiles[0].GetFileName();
            }

            return Title;
        }

        public IAudioFile GetSampleFile()
        {
            if (AudioFiles.Count > 0)
            {
                return AudioFiles[0];
            }

            return null;
        }

        public IAudioFileData CacheSampleData()
        {
            IAudioFile sampleFile = GetSampleFile();

            if (sampleFile != null)
                return sampleFile.LoadAudioFileData();
            return null;
        }

        /* attempts to find an icon with ideal name among the given paths */
        public string GetIdealIcon(IEnumerable<string> paths)
        {
            int lastDistance = 999;
            string lastFile = "";

            foreach (string file in paths)
            {
                if (file == null || file.Length == 0)
                    continue;

                string fileName = Path.GetFileName(file);
                int distance = Utility.GetLevenshteinDistance(fileName, GetSampleFilename());

                if (lastFile == "" || distance < lastDistance)
                {
                    lastDistance = distance;
                    lastFile = file;
                }
            }

            return lastFile;
        }

        public string GetAnyIcon(IEnumerable<string> paths)
        {
            foreach (string file in paths)
            {
                if (file == null || file.Length == 0)
                    continue;

                return file;
            }

            return "";
        }

        /* called after fully loading from JSON */
        public void PostLoad()
        {
            CheckFileDurationPostLoad();
            UpdateFullDuration();
            RefreshJoinedStrings();

            /* only AudiobookPosition is saved/loaded, get fileIdx and file position from that */
            AudioFileIdx = GetFileIdxAtDuration(AudiobookPosition);

            AudioFilePosition = GetLocalDurationFromTotal(AudiobookPosition, GetCurrentAudioFile());

            RefreshChapterIndizes();
            RefreshBookMarkReferences();
            SortBookMarks();

            if (IconPath != null && !File.Exists(IconPath))
            {
                DebugLog.WriteLine("warning: Icon for audiobook '" + Title + "' does not exist at '" + IconPath + "', attempting to find new image file.");
                RefreshIcon();
            }
        }

        private void RefreshChapterIndizes()
        {
            for (int i = 0; i < Chapters.Count; i++)
            {
                Chapters[i].ChapterIdx = i;
            }
        }

        private void RefreshBookMarkReferences()
        {
            foreach (IBookMark bookMark in BookMarks)
            {
                bookMark.AssociatedAudiobook = this;
            }
        }

        public void CheckFileDurationPostLoad()
        {
            /* caching duration is slow, so duration is stored in the save file. If, after
             loading no valid duration has been restored for one file, recache whole audiobook */
            foreach (AudioFile file in AudioFiles)
            {
                if (!file.HasValidDuration())
                {
                    DebugLog.WriteLine("Warning: not all audio files have a valid cached duration. Attempting to cache again... (" + Title + ")");

                    CacheDuration();
                    return;
                }
            }
        }

        public void CacheDuration()
        {
            Task.Run(() =>
            {
                List<Task> tasks = new List<Task>();

                /* caching duration is slow, so duration is stored in the save file. If, after
                 loading no valid duration has been restored, try caching again */
                foreach (AudioFile file in AudioFiles)
                {
                    if (file.Duration.TotalSeconds < 1)
                    {
                        //file.CacheDuration();

                        Task task = new Task(file.CacheDuration);
                        tasks.Add(task);

                        task.Start();
                    }
                }

                if (tasks.Count > 0)
                {
                    Task.WaitAll(tasks.ToArray());
                }
            });
        }

        public void UpdateAudiobookPosition()
        {
            TimeSpan newAudiobookPosition = GetTotalDurationFromLocal(AudioFilePosition, GetCurrentAudioFile());
            UpdatePlayedPercent();

            if (newAudiobookPosition != AudiobookPosition)
            {
                AudiobookPosition = newAudiobookPosition;
            }
        }

        public void UpdateFullDuration()
        {
            List<IAudioFile> files;

            lock (AudioFiles)
            {
                files = new List<IAudioFile>(AudioFiles);
            }

            FullDuration = new TimeSpan();
            foreach (IAudioFile file in files)
            {
                lock (file)
                {
                    FullDuration += file.Duration;
                }
            }
        }

        /* chapterThreshholdMS to avoid rounding issues with player position */
        private int chapterThreshholdMS = 100;
        public IAudioFileChapterData? GetChapterAtDuration(TimeSpan duration)
        {
            IAudioFileChapterData? lastChapter = null;

            for (int i = 0; i < Chapters.Count; i++)
            {
                var chapter = Chapters[i];
                if (chapter.StartTime - chapterThreshholdMS > duration.TotalMilliseconds)
                    return lastChapter;

                lastChapter = chapter;
            }

            return lastChapter;
        }

        public IAudioFile? GetFileAtDuration(TimeSpan duration)
        {
            IAudioFile lastFile = null;
            TimeSpan elapsedTime = new TimeSpan();

            for (int i = 0; i < AudioFiles.Count; i++)
            {
                IAudioFile file = AudioFiles[i];

                /* this file is the next in the queue, so return last one */
                if (elapsedTime > duration)
                {
                    return lastFile;
                }

                elapsedTime += file.Duration;
                lastFile = file;
            }

            return lastFile;
        }

        public int GetFileIdxAtDuration(TimeSpan duration)
        {
            TimeSpan elapsedTime = new TimeSpan();

            for (int i = 0; i < AudioFiles.Count; i++)
            {
                /* this file is the next in the queue, so return last one */
                if (elapsedTime > duration)
                    return i - 1;

                IAudioFile file = AudioFiles[i];
                elapsedTime += file.Duration;
            }

            return AudioFiles.Count - 1;
        }

        /* converts the total duration (taking into account all of the audiofiles)
         to the duration within the current audiofile */
        public TimeSpan GetLocalDurationFromTotal(TimeSpan duration, IAudioFile file)
        {
            TimeSpan elapsedTime = new TimeSpan();

            for (int i = 0; i < AudioFiles.Count; i++)
            {
                IAudioFile currentFile = AudioFiles[i];

                if (currentFile == file)
                    return duration - elapsedTime;

                elapsedTime += currentFile.Duration;
            }

            return new TimeSpan();
        }

        /* gives time within audioBOOK, takes time within audioFILE (contained in same audiobook)
         * 
         * iterates all audiofile before parameter file (active audiofile) and adds up their
         * durations. This total + parameter duration is the global timespan */
        public TimeSpan GetTotalDurationFromLocal(TimeSpan duration, IAudioFile file)
        {
            TimeSpan elapsedTime = new TimeSpan();

            for (int i = 0; i < AudioFiles.Count; i++)
            {
                IAudioFile currentFile = AudioFiles[i];

                if (currentFile == file)
                    return elapsedTime + duration;

                elapsedTime += currentFile.Duration;
            }

            return duration;
        }

        private double MaxBookMarkPositionDelta = 0.1d;
        public bool HasBookMarkAtPosition(TimeSpan position)
        {
            foreach (var bookMark in BookMarks)
            {
                double posDelta = Math.Abs(bookMark.Position.TotalSeconds - position.TotalSeconds);
                if (posDelta <= MaxBookMarkPositionDelta)
                    return true;
            }

            return false;
        }

        public string GetFreeBookMarkIdentifier()
        {
            string freeIdentifier = "NONE";
            for (int i = 1; i < int.MaxValue; i++)
            {
                bool available = true;
                string currentIdentifier = i.ToString();

                foreach (var bookMark in BookMarks)
                {
                    if (bookMark.Identifier == currentIdentifier)
                    {
                        available = false;
                        break;
                    }
                }

                if (available)
                {
                    freeIdentifier = currentIdentifier;
                    break;
                }
            }

            return freeIdentifier;
        }

        public bool AddBookMark(string identifier, TimeSpan position)
        {
            if (identifier.Length == 0 || position.TotalSeconds < 0)
                return false;

            IBookMark bookMark = new BookMark(this, identifier, position);

            BookMarks.Add(bookMark);
            SortBookMarks();

            OnChangedBookMarks();

            /* allow one autosave */
            if (App.Globals.audiobookScanner != null)
                App.Globals.audiobookScanner.MarkChanged();

            App.Globals.textToSpeech.Speak("Added Bookmark: " + identifier);

            return true;
        }

        public void SortBookMarks()
        {
            BookMarks = BookMarks.OrderBy(item => item.Position).ToList();
        }

        public bool RemoveBookMark(IBookMark bookMark)
        {
            bool success = BookMarks.Remove(bookMark);

            if (success)
            {
                OnChangedBookMarks();

                /* allow one autosave */
                if (App.Globals.audiobookScanner != null)
                    App.Globals.audiobookScanner.MarkChanged();

                App.Globals.textToSpeech.Speak("Removed Bookmark: " + bookMark.Identifier);
            }

            return success;
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public event IAudiobook.ChangedChaptersDelegate ChangedChapters;
        public void OnChangedChapters()
        {
            ChangedChapters?.Invoke();
        }

        public event IAudiobook.ChangedBookMarksDelegate ChangedBookMarks;
        public void OnChangedBookMarks()
        {
            ChangedBookMarks?.Invoke();
        }
    }
}
