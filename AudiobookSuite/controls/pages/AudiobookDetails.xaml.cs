﻿using AudiobookSuite.Commons;
using AudiobookSuite.controls.usercontrols;
using AudiobookSuite.lib;
using GongSolutions.Wpf.DragDrop;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AudiobookSuite.controls;

namespace AudiobookSuite
{
    public partial class AudiobookDetails : Page, IDropTarget, INotifyPropertyChanged, IUndoRedoHandler
    {
        public IAudiobook LinkedAudiobook { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<IAudioFileData> LibraryAudioFiles { get; set; }

        public IAudioFile PlayingAudioFile { get; set; }

        public string JoinedGroups { get; set; }

        public AudiobookDetails(IAudiobook linkedAudiobook)
        {
            DataContext = this;

            LibraryAudioFiles = new ObservableCollection<IAudioFileData>();

            LinkedAudiobook = linkedAudiobook;
            RefreshLinkedGroups();

            InitializeComponent();

            this.Unloaded += OnUnloaded;
            this.Loaded += OnLoaded;
        }

        void OnUnloaded(object sender, EventArgs e)
        {
            LibraryDataGrid.CommitEdit(DataGridEditingUnit.Row, true);

            App.Globals.audiobookScanner.FinishedScanning -= OnFinishedScanning;
            App.Globals.playerControls.PlayedAudioFile -= OnPlayedAudioFile;

            var controls = App.Globals.playerControls;
            controls.ClearInputDisabled();

            ((IUndoRedoHandler)this).RemoveUndoRedo();
        }

        void OnLoaded(object sender, EventArgs e)
        {
            UserControl playUserCtrl = App.Globals.playerControls as UserControl;
            Utility.RemoveChild(playUserCtrl.Parent, playUserCtrl);
            Grid_PlayerControls.Children.Add(playUserCtrl);

            App.Globals.audiobookScanner.FinishedScanning += OnFinishedScanning;
            App.Globals.playerControls.PlayedAudioFile += OnPlayedAudioFile;

            ((IUndoRedoHandler)this).InitUndoRedo();

            RefreshFiles();
        }

        public void RefreshLinkedGroups()
        {
            if (LinkedAudiobook == null)
                return;

            List<string> groupStrings = new List<string>();
            foreach (ISortingGroup group in LinkedAudiobook.Groups)
                groupStrings.Add(group.GroupName);

            JoinedGroups = String.Join(',', groupStrings);

            NotifyPropertyChanged(nameof(JoinedGroups));
        }

        public void OnFinishedScanning()
        {
            RefreshFiles();
        }

        public void OnPlayedAudioFile(IAudioFile file)
        {
            RefreshFiles();
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        void IDropTarget.DragOver(IDropInfo dropInfo)
        {
            ListSortDirection? sortDir = LibraryDataGrid.Columns[0].SortDirection;
            if (sortDir != ListSortDirection.Ascending)
                return;

            dropInfo.Effects = DragDropEffects.Move;
            dropInfo.DropTargetAdorner = DropTargetAdorners.Insert;
        }

        void IDropTarget.Drop(IDropInfo dropInfo)
        {
            DataObject dataObject = dropInfo.Data as DataObject;

            if (dataObject != null && dataObject.ContainsFileDropList())
            {
                var files = dataObject.GetFileDropList();

                int targetIdx = dropInfo.InsertIndex;
                foreach (string filePath in files)
                {
                    /* ignore folders */
                    if (Directory.Exists(filePath))
                        continue;
                    
                    /* is extension supported? */
                    string extension = Path.GetExtension(filePath).ToLower();
                    if (!(IAudiobookScanner.SupportedMediaExtensions.Contains(extension)))
                        continue;

                    Task<IAudioFile> taskOut = App.Globals.audiobookScanner.AddAudioFile(filePath, LinkedAudiobook);
                    IAudioFile scannedFile = taskOut.Result;

                    if (scannedFile != null)
                    {
                        int idx = LinkedAudiobook.AudioFiles.FindIndex(item => item == scannedFile);

                        /* if file was added successfully, increase index for next file so it is added after the previous */
                        if (LinkedAudiobook.RearrangeAudiofile(idx, targetIdx))
                            targetIdx++;
                    }
                }

                RefreshFiles();
            }
            else if (dropInfo.DragInfo != null)
            {
                /* only allow drag/drop if sorting by tracknumber ascending, which is the first column 
                otherwise, things get weird */
                ListSortDirection? sortDir = LibraryDataGrid.Columns[0].SortDirection;
                if (sortDir != ListSortDirection.Ascending)
                    return;

                LinkedAudiobook.RearrangeAudiofile(dropInfo.DragInfo.SourceIndex, dropInfo.InsertIndex);
                RefreshFiles();
            }
        }

        public void RefreshFiles()
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                if (LibraryDataGrid == null || LinkedAudiobook == null)
                    return;

                foreach (IAudioFileData data in LibraryAudioFiles)
                    data.SourceFile.CacheDurationFinished -= CacheDurationFinished;

                LibraryAudioFiles.Clear();

                foreach (IAudioFile audioFile in LinkedAudiobook.AudioFiles)
                {
                    LibraryAudioFiles.Add(audioFile.LoadAudioFileData());
                    
                    audioFile.CacheDurationFinished += CacheDurationFinished;
                }

                NotifyPropertyChanged(nameof(LibraryAudioFiles));
                NotifyPropertyChanged(nameof(LinkedAudiobook));
            });
        }

        public void CacheDurationFinished()
        {
            RefreshFiles();
        }

        private void LibraryDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var controls = App.Globals.playerControls;

            /* ignore space key presses except when in edit mode */
            if (e.Key == Key.Space && controls.IsInputEnabled())
            {
                e.Handled = true;
            }
        }

        private void LibraryDataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("datagrid");
        }

        private void LibraryDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputEnabled("datagrid");
        }

        private void LibraryDataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputEnabled("datagrid");
        }

        private void LibraryDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                RemoveSelectedAudioFiles();
            }
        }

        public void RemoveSelectedAudioFiles()
        {
            List<IAudioFileData> audioFiles = LibraryDataGrid.SelectedItems.Cast<IAudioFileData>().ToList();

            Dialog_ConfirmDeleteAudioFiles newWindow = new Dialog_ConfirmDeleteAudioFiles(audioFiles);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            if (newWindow.ShowDialog() == true)
            {
                foreach (var item in audioFiles)
                {
                    IAudioFileData data = (AudioFileData)item;
                    RemoveAudioFile(data.SourceFile);
                }

                RefreshFiles();
            }
        }

        public void RemoveAudioFile(IAudioFile audioFile)
        {
            App.Globals.audiobookScanner.RemoveAudioFile(audioFile);
        }

        private void Click_PlayFile(object sender, RoutedEventArgs e)
        {
            PlaySelectedFile();
        }

        private void Click_RecacheDuration(object sender, RoutedEventArgs e)
        {
            List<IAudioFileData> audioFiles = LibraryDataGrid.SelectedItems.Cast<IAudioFileData>().ToList();

            foreach (IAudioFileData file in audioFiles)
            {
                if (file != null && file.SourceFile != null)
                {
                    file.SourceFile.CacheDuration();
                }
            }
        }

        private void Click_RemoveFile(object sender, RoutedEventArgs e)
        {
            RemoveSelectedAudioFiles();
        }

        private void Button_AddAudiofiles_Click(object sender, RoutedEventArgs e)
        {
            DropDownFile.IsOpen = false;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = IAudiobookScanner.SupportedMediaExtensionsFilter;
            openFileDialog.Multiselect = true;

            if (openFileDialog.ShowDialog() == true)
            {
                List<string> filePaths = openFileDialog.FileNames.ToList<string>();
                List<IAudioFile> audioFiles = App.Globals.audiobookScanner.AddAudioFiles(filePaths, LinkedAudiobook).Result;

                /* allow one autosave */
                if (App.Globals.audiobookScanner != null)
                    App.Globals.audiobookScanner.MarkChanged();

                RefreshFiles();
            }
        }

        private void Button_SortAudiofiles_Click(object sender, RoutedEventArgs e)
        {
            LinkedAudiobook.AutoSortAudioFiles();

            RefreshFiles();
            DropDownFile.IsOpen = false;
        }

        private void Button_RefreshMetadata_Click(object sender, RoutedEventArgs e)
        {
            LinkedAudiobook.RefreshMetadata();

            RefreshFiles();
            DropDownFile.IsOpen = false;
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("controls/pages/Homepage.xaml", UriKind.Relative));
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            Dialog_GenericConfirmDeny newDiag = new Dialog_GenericConfirmDeny("Open Help Page", "This will open an external browser page to display AudiobookSuite's wiki page.");

            Window parentWindow = Window.GetWindow(this);
            newDiag.Owner = parentWindow;

            IPlayerControls controls = App.Globals.playerControls;
            controls.SetInputDisabled("message");

            if (newDiag.ShowDialog() == true)
            {
                string url = "https://gitlab.com/Shrimperator/AudiobookSuite/-/wikis/home";
                Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
            }

            controls.SetInputEnabled("message");
        }

        private void Click_OpenFolder(object sender, RoutedEventArgs e)
        {
            if (LinkedAudiobook != null)
            {
                DebugLog.WriteLine("info: opening explorer directory '" + LinkedAudiobook.AudiobookPath + "'");
                Process.Start(new ProcessStartInfo(LinkedAudiobook.AudiobookPath) { UseShellExecute = true });
            }
        }

        private void Button_Settings_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("controls/pages/SettingsPage.xaml", UriKind.Relative));
        }

        private void ClickRescanLibrary(object sender, RoutedEventArgs e)
        {
            if (LinkedAudiobook == null)
            {
                DebugLog.WriteLine("warning: rescan library failed because LinkedAudiobook is null");
                return;
            }
            if (LinkedAudiobook.AudiobookPath == null)
            {
                DebugLog.WriteLine("warning: rescan library failed because LinkedAudiobook.AudiobookPath is null");
                return;
            }

            string scanDir = LinkedAudiobook.AudiobookPath;
            App.Globals.audiobookScanner.ScanDirectory(scanDir);
        }

        private void Button_Authors_Click(object sender, RoutedEventArgs e)
        {
            ListEditDialog newWindow = new ListEditDialog(LinkedAudiobook.Authors, "Authors", "New Author");
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            IPlayerControls controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_authors");

            if (newWindow.ShowDialog() == true)
            {
                LinkedAudiobook.Authors = newWindow.DataList;
                NotifyPropertyChanged(nameof(LinkedAudiobook));
            }

            controls.SetInputEnabled("menu_authors");
        }

        private void PanelNarrators_Click(object sender, RoutedEventArgs e)
        {
            ListEditDialog newWindow = new ListEditDialog(LinkedAudiobook.Narrators, "Narrators", "New Narrator");
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            IPlayerControls controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_narrators");

            if (newWindow.ShowDialog() == true)
            {
                LinkedAudiobook.Narrators = newWindow.DataList;
                NotifyPropertyChanged(nameof(LinkedAudiobook));
            }

            controls.SetInputEnabled("menu_narrators");
        }

        private void PanelGroups_Click(object sender, RoutedEventArgs e)
        {
            if (LinkedAudiobook == null)
                return;

            GroupEditDialog newWindow = new GroupEditDialog(LinkedAudiobook.Groups, "Groups", "New Group");
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            IPlayerControls controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_groups");

            if (newWindow.ShowDialog() == true)
            {
                UpdateAudiobookGroupsFromGroupNames(newWindow.GroupNameList);
            }

            controls.SetInputEnabled("menu_groups");
        }

        private void UpdateAudiobookGroupsFromGroupNames(List<string> groupNames)
        {
            /* add new groups to audiobook */
            foreach (string groupName in groupNames)
            {
                if (LinkedAudiobook.Groups.FindIndex(item => item.GroupName.ToLower() == groupName.ToLower()) < 0)
                {
                    App.Globals.groupManager.AddGroup(groupName);

                    LinkedAudiobook.AddToGroup(groupName);
                }
            }

            /* remove groups from audiobook that have been removed from the list  */
            for (int i = 0; i < LinkedAudiobook.Groups.Count; i++)
            {
                ISortingGroup group = LinkedAudiobook.Groups[i];

                if (groupNames.FindIndex(item => item.ToLower() == group.GroupName.ToLower()) < 0)
                {
                    App.Globals.groupManager.RemoveFromGroup(group, LinkedAudiobook);
                    i--;
                }
            }

            RefreshLinkedGroups();
        }

        private void TitleTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputEnabled("textbox_title");
        }

        private void TitleTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("textbox_title");
        }

        private void Icon_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                ShowLargeIconDialog();
        }

        public void OpenManageBookMarksDialog()
        {
            if (LinkedAudiobook == null)
                return;

            IAudiobook audiobook = LinkedAudiobook;

            Dialog_ManageBookMarks newWindow = new Dialog_ManageBookMarks(LinkedAudiobook);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_managebookmarks");

            if (newWindow.ShowDialog() == true && audiobook.BookMarks.Count != newWindow.TempBookMarks.Count)
            {
                audiobook.BookMarks = newWindow.TempBookMarks;
            }

            controls.SetInputEnabled("menu_managebookmarks");
        }

        private void ShowLargeIconDialog()
        {
            if (LinkedAudiobook == null || !LinkedAudiobook.HasIcon() || Dialog_Image.IsOpen)
                return;

            Dialog_Image newWindow = new Dialog_Image(LinkedAudiobook.IconPath);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            newWindow.Show();
        }

        private void ShowSetIconDialog()
        {
            if (LinkedAudiobook != null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "All Images Files (*.png;*.jpeg;*.gif;*.jpg;*.bmp;*.tiff;*.tif;*.mp3;*.mp4;*.m4b;*.m4a)|*.png;*.jpeg;*.gif;*.jpg;*.bmp;*.tiff;*.tif;*.mp3;*.mp4;*.m4b;*.m4a;" +
                "|PNG Portable Network Graphics (*.png)|*.png" +
                "|JPEG File Interchange Format (*.jpg *.jpeg *jfif)|*.jpg;*.jpeg;*.jfif" +
                "|BMP Windows Bitmap (*.bmp)|*.bmp" +
                "|TIF Tagged Imaged File Format (*.tif *.tiff)|*.tif;*.tiff" +
                "|GIF Graphics Interchange Format (*.gif)|*.gif" +
                "|Embedded audio file covers (*.mp3;*.mp4;*.m4b;*.m4a)|*.mp3;*.mp4;*.m4b;*.m4a" +
                "|All Files|*";

                if (openFileDialog.ShowDialog() == true)
                {
                    LinkedAudiobook.IconPath = openFileDialog.FileName;

                    /* allow one autosave */
                    if (App.Globals.audiobookScanner != null)
                        App.Globals.audiobookScanner.MarkChanged();
                }
            }
        }

        private void Button_SetIcon_Click(object sender, RoutedEventArgs e)
        {
            ShowSetIconDialog();   
        }

        private void Image_SetIcon_Click(object sender, RoutedEventArgs e)
        {
            ShowSetIconDialog();
        }

        private void Image_View_Click(object sender, RoutedEventArgs e)
        {
            ShowLargeIconDialog();
        }

        private void Button_BookMark_Click(object sender, RoutedEventArgs e)
        {
            OpenManageBookMarksDialog();
        }

        private void LibraryDataGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            PlaySelectedFile();
        }

        private void PlaySelectedFile()
        {
            if (LibraryDataGrid != null && LibraryDataGrid.SelectedItems != null && LibraryDataGrid.SelectedItems.Count == 1)
            {
                DataGridRow dgr = LibraryDataGrid.ItemContainerGenerator.ContainerFromItem(LibraryDataGrid.SelectedItem) as DataGridRow;
                AudioFileData fileData = (AudioFileData)dgr.Item;
                IAudioFile audioFile = fileData.SourceFile;

                // finds the tracknumber from the IAudioFile of the selected audio file. 
                int selectedChapterTrackNumber = audioFile.TrackNumber;

                TimeSpan fileStartOffset = new TimeSpan();

                // iterate through the audio files and sum up durations for all files before the selected chapter
                for (int i = 0; i < LibraryAudioFiles.Count; i++)
                {
                    IAudioFileData currFile = LibraryAudioFiles[i];

                    int chapterDifference = currFile.SourceFile.TrackNumber;
                    if (chapterDifference < selectedChapterTrackNumber)
                    {
                        fileStartOffset += currFile.SourceFile.Duration;
                    }
                    else
                        break;
                }

                PlayerControls playerControls = (PlayerControls) App.Globals.playerControls;

                var prevPos = playerControls.GetCurrentAudiobookPosition();

                App.Globals.playerControls.PlayAudiobook(LinkedAudiobook);
                App.Globals.playerControls.SetAudiobookToPosition(fileStartOffset);
                App.Globals.playerControls.Play();

                var newPos = playerControls.GetCurrentAudiobookPosition();

                playerControls.PushUndoRedoActionTimeline(prevPos, newPos);

                DebugLog.WriteLine("info: jumping to file '" + fileData.SourceFile.AudioFilePath + "' at " + fileStartOffset.ToString());
            }
        }

        private void ClickUndo(object sender, RoutedEventArgs e)
        {
            App.Globals.undoRedo.Undo();
            DropDownEdit.IsOpen = false;
        }

        private void ClickRedo(object sender, RoutedEventArgs e)
        {
            App.Globals.undoRedo.Redo();
            DropDownEdit.IsOpen = false;
        }

        public void OnUndo()
        {
            NotifyPropertyChanged("CanUndoProperty");
            NotifyPropertyChanged("CanRedoProperty");
        }

        public void OnRedo()
        {
            NotifyPropertyChanged("CanUndoProperty");
            NotifyPropertyChanged("CanRedoProperty");
        }

        public void OnPushedUndoAction()
        {
            NotifyPropertyChanged("CanUndoProperty");
            NotifyPropertyChanged("CanRedoProperty");
        }

        public void OnRemovedUndoAction()
        {
            NotifyPropertyChanged("CanUndoProperty");
            NotifyPropertyChanged("CanRedoProperty");
        }

        public bool CanUndoProperty
        {
            get { return ((IUndoRedoHandler)this).CanUndo(); }
        }

        public bool CanRedoProperty
        {
            get { return ((IUndoRedoHandler)this).CanRedo(); }
        }
    }
}