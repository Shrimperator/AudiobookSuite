﻿using AudiobookSuite.Commons;
using AudiobookSuite.controls.usercontrols;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using AudiobookSuite.lib;

namespace AudiobookSuite
{
    public partial class FullScreenPlayer : Page, INotifyPropertyChanged
    {
        public bool IsTimelineIndicatorVisible { get; set; }
        private bool IsMouseDragging = false;

        public bool IsPlaying { get; set; } = false;
        public bool IsMuted { get; set; } = false;

        public int Volume { get; set; }
        public float PlaybackSpeed { get; set; }

        public IAudiobook? CurrentAudiobook { get; set; }
        public IAudioFile? CurrentAudioFile { get; set; }
        public IAudioFileChapterData? CurrentChapter { get; set; }
        public int AudiobookProgressMaxSeconds { get; set; }
        public int AudiobookProgressSeconds { get; set; }

        private DispatcherTimer ProgressTimer = new DispatcherTimer(DispatcherPriority.Render);

        /* for undo/redo action, need to cache position in audiobook before the first click on the timeline */
        private TimeSpan AudiobookPositionPreDrag = TimeSpan.FromSeconds(0);

        /**/

        public FullScreenPlayer()
        {
            DataContext = this;
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /**/

        private void OnChangeChapter(object sender, EventArgs e)
        {
            CurrentChapter = GetPlayerControls().CurrentChapter;
            NotifyPropertyChanged(nameof(CurrentChapter));
        }

        private void OnUpdateBookMarks(object sender, EventArgs e)
        {
            Timeline_CreateBookMarks();
        }

        private void OnUpdateBookMarkToolTips(object sender, EventArgs e)
        {
            Timeline_UpdateBookMarkToolTips();
        }

        private void OnChangeVolume(object sender, EventArgs e)
        {
            Volume = GetPlayerControls().Volume;
            NotifyPropertyChanged(nameof(Volume));
        }

        private void OnPlay(object sender, EventArgs e)
        {
            IsPlaying = GetPlayerControls().IsPlaying;
            NotifyPropertyChanged(nameof(IsPlaying));
        }

        private void OnPause(object sender, EventArgs e)
        {
            IsPlaying = GetPlayerControls().IsPlaying;
            NotifyPropertyChanged(nameof(IsPlaying));
        }

        private void OnStop(object sender, EventArgs e)
        {
            IsPlaying = GetPlayerControls().IsPlaying;
            NotifyPropertyChanged(nameof(IsPlaying));
        }

        private void OnTimelineUpdate(object sender, EventArgs e)
        {
            UpdateTimelineIndicator();
            UpdateProgress();
        }

        private void OnControlDataUpdate(object sender, EventArgs e)
        {
            UpdateControlData();
        }

        private void OnPlaybackSpeedChange(object sender, EventArgs e)
        {
            PlaybackSpeed = GetPlayerControls().PlaybackSpeed;
            NotifyPropertyChanged(nameof(PlaybackSpeed));
        }

        private void OnIsMutedChange(object sender, EventArgs e)
        {
            IsMuted = GetPlayerControls().IsMuted;
            NotifyPropertyChanged(nameof(IsMuted));
        }

        private void OnPlayedAudioFile(IAudioFile file)
        {
            CurrentAudioFile = file;
            NotifyPropertyChanged(nameof(CurrentAudioFile));
        }

        private void OnFinishedAudiobook(IAudiobook audiobook)
        {
            CurrentAudiobook = null;

            UpdateControlData();
            Timeline_CreateBookMarks();
            Timeline_CreateChapterMarkers();

            NotifyPropertyChanged(nameof(CurrentAudiobook));
        }

        private void OnPlayedAudiobook(IAudiobook audiobook)
        {
            CurrentAudiobook = audiobook;

            UpdateControlData();
            Timeline_CreateBookMarks();
            Timeline_CreateChapterMarkers();

            NotifyPropertyChanged(nameof(CurrentAudiobook));
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            CurrentAudiobook = playerControls.CurrentAudiobook;
            CurrentAudioFile = playerControls.CurrentAudioFile;
            CurrentChapter = playerControls.CurrentChapter;
            IsPlaying = playerControls.IsPlaying;
            AudiobookProgressMaxSeconds = playerControls.AudiobookProgressMaxSeconds;
            AudiobookProgressSeconds = playerControls.AudiobookProgressSeconds;

            playerControls.OnChangeVolume += OnChangeVolume;
            playerControls.OnPlay += OnPlay;
            playerControls.OnPause += OnPause;
            playerControls.OnStop += OnStop;
            playerControls.OnTimelineUpdate += OnTimelineUpdate;
            playerControls.OnControlDataUpdate += OnControlDataUpdate;
            playerControls.OnPlaybackSpeedChange += OnPlaybackSpeedChange;
            playerControls.OnIsMutedChange += OnIsMutedChange;
            playerControls.PlayedAudioFile += OnPlayedAudioFile;
            playerControls.FinishedAudiobook += OnFinishedAudiobook;
            playerControls.PlayedAudiobook += OnPlayedAudiobook;
            playerControls.OnChangeChapter += OnChangeChapter;
            playerControls.OnUpdateBookMarks += OnUpdateBookMarks;
            playerControls.OnUpdateBookMarkToolTips += OnUpdateBookMarkToolTips;

            App.Globals.undoRedo.UndoAction += OnUndo;
            App.Globals.undoRedo.RedoAction += OnRedo;
            App.Globals.undoRedo.PushedAction += OnPushedUndoAction;
            App.Globals.undoRedo.RemovedActions += OnRemovedUndoAction;

            ProgressTimer.Interval = TimeSpan.FromMilliseconds(250);

            ProgressTimer.Tick += UpdateProgressTick;
            ProgressTimer.Start();

            Volume = App.Globals.settings.Volume;
            PlaybackSpeed = App.Globals.settings.PlaybackSpeed;
            IsMuted = App.Globals.settings.IsMuted;

            NotifyPropertyChanged(nameof(Volume));
            NotifyPropertyChanged(nameof(PlaybackSpeed));
            NotifyPropertyChanged(nameof(IsMuted));
            NotifyPropertyChanged(nameof(CurrentAudiobook));
            NotifyPropertyChanged(nameof(CurrentAudioFile));
            NotifyPropertyChanged(nameof(CurrentChapter));
            NotifyPropertyChanged(nameof(IsPlaying));
            NotifyPropertyChanged(nameof(AudiobookProgressMaxSeconds));
            NotifyPropertyChanged(nameof(AudiobookProgressSeconds));

            Timeline_CreateChapterMarkers();
            Timeline_CreateBookMarks();

            UpdateControlData();
            UpdateProgress();

            SetUndoRedoButtonsVisibility();
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.OnChangeVolume -= OnChangeVolume;
            playerControls.OnPlay -= OnPlay;
            playerControls.OnPause -= OnPause;
            playerControls.OnStop -= OnStop;
            playerControls.OnTimelineUpdate -= OnTimelineUpdate;
            playerControls.OnControlDataUpdate -= OnControlDataUpdate;
            playerControls.OnPlaybackSpeedChange -= OnPlaybackSpeedChange;
            playerControls.OnIsMutedChange -= OnIsMutedChange;
            playerControls.PlayedAudioFile -= OnPlayedAudioFile;
            playerControls.FinishedAudiobook -= OnFinishedAudiobook;
            playerControls.PlayedAudiobook -= OnPlayedAudiobook;
            playerControls.OnChangeChapter -= OnChangeChapter;
            playerControls.OnUpdateBookMarks -= OnUpdateBookMarks;
            playerControls.OnUpdateBookMarkToolTips -= OnUpdateBookMarkToolTips;

            App.Globals.undoRedo.UndoAction -= OnUndo;
            App.Globals.undoRedo.RedoAction -= OnRedo;
            App.Globals.undoRedo.PushedAction -= OnPushedUndoAction;
            App.Globals.undoRedo.RemovedActions -= OnRemovedUndoAction;

            ProgressTimer.Tick -= UpdateProgressTick;
            ProgressTimer.Stop();
        }

        private void VolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var playerControls = GetPlayerControls();

            if (PlayerControls.GlobalMediaPlayer != null)
            {
                int value = (int)VolumeSlider.Value;
                playerControls.SetVolume(value);
            }
        }

        private PlayerControls GetPlayerControls()
        {
            return (PlayerControls)App.Globals.playerControls;
        }

        private void Button_PlaybackSpeed_Click(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.SetPlaybackSpeed((float)playerControls.OpenPlaybackSpeedDialog(this));
        }

        private void Button_SleepTimer_Click(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.ToggleSleepTimerDialog(this);
        }

        private void Click_BookMark(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            OpenBookMarContextMenu();
        }

        public void OpenBookMarContextMenu()
        {
            ContextMenu contextMenu = Button_BookMark.ContextMenu;
            contextMenu.PlacementTarget = Button_BookMark;
            contextMenu.IsOpen = true;
        }

        private void Click_NewBookMark(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.OpenNewBookMarkDialog(this);
        }

        private void Click_ManageBookMarks(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.OpenManageBookMarksDialog(this);
        }

        private void ButtonMute_Click(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            bool newMuteState = !playerControls.IsMuted;
            playerControls.SetIsMuted(newMuteState);
        }

        private void Title_Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            if (playerControls.CurrentAudiobook != null)
                playerControls.OpenAudiobookDetails();
        }

        private void Icon_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                ShowLargeIconDialog();
        }

        private void GlobalPlayerControls_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Timeline_CreateChapterMarkers();
            Timeline_CreateBookMarks();
        }

        private void Image_View_Click(object sender, RoutedEventArgs e)
        {
            ShowLargeIconDialog();
        }

        public void ShowLargeIconDialog()
        {
            if (CurrentAudiobook == null || !CurrentAudiobook.HasIcon() || Dialog_Image.IsOpen)
                return;

            Dialog_Image newWindow = new Dialog_Image(CurrentAudiobook.IconPath);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            newWindow.Show();
        }

        private void Image_SetIcon_Click(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.ShowSetIconDialog();
        }

        private void Audiobook_Details_Click(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.OpenAudiobookDetails();
        }

        private void OnAudiobookBookmarksChanged()
        {
            App.Current.Dispatcher.Invoke(new Action(() => { Timeline_CreateBookMarks(); }));
        }

        private void OnAudiobookChaptersChanged()
        {
            App.Current.Dispatcher.Invoke(new Action(() => { Timeline_CreateChapterMarkers(); }));
        }

        public void UpdateControlData()
        {
            if (CurrentAudiobook != null)
            {
                var timeSpan = CurrentAudiobook.FullDuration;
                string fullDuration = String.Format("{0}:{1:00}:{2:00}", Math.Truncate(timeSpan.TotalHours), timeSpan.Minutes, timeSpan.Seconds);

                if (CurrentAudiobook.Chapters.Count < 1)
                    ChapterDisplay.Visibility = Visibility.Collapsed;
                else
                    ChapterDisplay.Visibility = Visibility.Visible;

                UpdateMaxProgress();
            }
            else
            {
                MediaControlTextTimePlayed.Text = "";

                ChapterDisplay.Visibility = Visibility.Collapsed;

                AudiobookProgressMaxSeconds = 1;
                AudiobookProgressSeconds = 0;

                NotifyPropertyChanged("AudiobookProgressMaxSeconds");
                NotifyPropertyChanged("AudiobookProgressSeconds");

                Timeline_RemoveChapterMarkers();
                Timeline_RemoveBookMarks();
            }

            UpdateProgress();
        }

        void UpdateProgressTick(object sender, EventArgs e)
        {
            UpdateProgress();
        }

        public void UpdateProgress()
        {
            if (CurrentAudiobook != null)
            {
                IAudioFile currentFile = CurrentAudiobook.GetCurrentAudioFile();

                if (currentFile != null)
                {
                    try
                    {
                        CurrentAudiobook.AudioFilePosition = TimeSpan.FromMilliseconds(PlayerControls.GlobalMediaPlayer.Time);
                        AudiobookProgressSeconds = (int)CurrentAudiobook.AudiobookPosition.TotalSeconds;

                        NotifyPropertyChanged("AudiobookProgressSeconds");

                        var fullDuration = CurrentAudiobook.FullDuration;
                        string sFullDuration = String.Format("{0}:{1:00}:{2:00}", Math.Truncate(fullDuration.TotalHours), fullDuration.Minutes, fullDuration.Seconds);

                        string currentPosition = CurrentAudiobook.AudiobookPosition.ToString(@"hh\:mm\:ss");
                        MediaControlTextTimePlayed.Text = currentPosition + " / " + sFullDuration;
                    }
                    catch (NullReferenceException e)
                    { }
                }
            }
        }

        public void UpdateMaxProgress()
        {
            AudiobookProgressMaxSeconds = (int)CurrentAudiobook.FullDuration.TotalSeconds;

            NotifyPropertyChanged("AudiobookProgressMaxSeconds");
        }

        public void IncrementVolume()
        {
            var playerControls = GetPlayerControls();

            var currVolume = App.Globals.settings.Volume;

            playerControls.SetVolume(currVolume + 1);
        }

        private void Timeline_RemoveChapterMarkers()
        {
            TimelineContainer.Children.Clear();
        }

        private void Timeline_CreateChapterMarkers()
        {
            Timeline_RemoveChapterMarkers();

            if (CurrentAudiobook == null || CurrentAudiobook.Chapters.Count == 0 || CurrentAudiobook.AudioFiles.Count == 0)
                return;

            double timelineWidth = Timeline.ActualWidth;
            double durationMs = CurrentAudiobook.FullDuration.TotalMilliseconds;

            Brush recBrush = (Brush)App.Current.TryFindResource("Style_TimelineChapterMarkerColor");

            for (int i = 0; i < CurrentAudiobook.Chapters.Count; i++)
            {
                var chapter = CurrentAudiobook.Chapters[i];

                double percentage = chapter.StartTime / durationMs;
                double posX = percentage * timelineWidth;

                Rectangle rec = new Rectangle()
                {
                    Width = 2,
                    Height = Timeline.ActualHeight + 5,
                    Fill = recBrush,
                    IsHitTestVisible = false,
                };

                TimelineContainer.Children.Add(rec);
                Canvas.SetLeft(rec, posX - rec.Width / 2);
            }
        }

        private void Timeline_CreateBookMarks()
        {
            Timeline_RemoveBookMarks();

            if (CurrentAudiobook == null || CurrentAudiobook.BookMarks.Count == 0 || CurrentAudiobook.AudioFiles.Count == 0)
                return;

            double timelineWidth = Timeline.ActualWidth;
            double durationMs = CurrentAudiobook.FullDuration.TotalMilliseconds;

            for (int i = 0; i < CurrentAudiobook.BookMarks.Count; i++)
            {
                var bookMark = CurrentAudiobook.BookMarks[i];

                double percentage = bookMark.Position.TotalMilliseconds / durationMs;
                double posX = percentage * timelineWidth;

                TimelineBookMark marker = new TimelineBookMark(bookMark);

                TimelineBookMarks.Children.Add(marker);
                Canvas.SetLeft(marker, posX - marker.Width / 2);
                Canvas.SetBottom(marker, 15);
            }
        }

        private void Timeline_UpdateBookMarkToolTips()
        {
            foreach (TimelineBookMark bookMarkElem in TimelineBookMarks.Children)
            {
                if (bookMarkElem != null)
                {
                    bookMarkElem.NotifyPropertyChanged("AssociatedBookMark");
                }
            }
        }

        private void Timeline_RemoveBookMarks()
        {
            TimelineBookMarks.Children.Clear();
        }

        private void Timeline_CreateBookMark(IBookMark bookMark)
        {
        }

        private void Timeline_RemoveBookMark(IBookMark bookMark)
        {
        }

        private void Timeline_MouseLeave(object sender, MouseEventArgs e)
        {
            var playerControls = GetPlayerControls();

            IsTimelineIndicatorVisible = false;
            NotifyPropertyChanged("IsTimelineIndicatorVisible");

            if (IsMouseDragging)
            {
                IsMouseDragging = false;
                playerControls.PushUndoRedoActionTimeline(AudiobookPositionPreDrag, playerControls.GetCurrentAudiobookPosition());
            }
        }

        private void Timeline_MouseDown(object sender, MouseEventArgs e)
        {
            var playerControls = GetPlayerControls();

            IsMouseDragging = true;

            AudiobookPositionPreDrag = playerControls.GetCurrentAudiobookPosition();

            Timeline_MouseMove(sender, e);
        }

        public void UpdateTimelineIndicator()
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (CurrentAudiobook == null)
                    return;

                double maxWidth = Timeline.ActualWidth;
                // get mousepos relative to Timeline
                Point mousePos = Mouse.GetPosition(Timeline);
                Point newIndicatorPos = mousePos;

                // translate mouse pos to relative TimelineIndicator pos
                newIndicatorPos = Timeline.TranslatePoint(mousePos, TimelineIndicator);

                newIndicatorPos = TimelineIndicator.RenderTransform.Transform(newIndicatorPos);
                newIndicatorPos.X -= TimelineIndicator.ActualWidth / 2.0;

                double xMin = -(maxWidth / 2.0) + TimelineIndicator.ActualWidth / 2.0;
                if (newIndicatorPos.X < xMin)
                    newIndicatorPos.X = xMin;
                double xMax = (maxWidth / 2.0) - TimelineIndicator.ActualWidth / 2.0;
                if (newIndicatorPos.X > xMax)
                    newIndicatorPos.X = xMax;

                TimelineIndicator.RenderTransform = new TranslateTransform(newIndicatorPos.X, 0);

                double maximum = CurrentAudiobook.FullDuration.TotalSeconds;

                double factor = mousePos.X / maxWidth;
                int newPositionSeconds = (int)(maximum * factor);

                TimeSpan currPosTimeSpan = TimeSpan.FromSeconds(newPositionSeconds);
                IAudioFileChapterData currPosChapter = CurrentAudiobook.GetChapterAtDuration(currPosTimeSpan);

                TimelineIndicator.SetTimelinePosition(currPosTimeSpan);
                TimelineIndicator.SetChapterName(currPosChapter != null ? currPosChapter.Title : null);
            }));
        }

        private void Timeline_MouseMove(object sender, MouseEventArgs e)
        {
            var playerControls = GetPlayerControls();

            UpdateTimelineIndicator();

            if (!IsMouseDragging || e.LeftButton != MouseButtonState.Pressed || CurrentAudiobook == null || playerControls.IsMediaPlayerBuffering)
                return;

            double MousePosition = e.GetPosition(Timeline).X;
            double width = Timeline.ActualWidth;
            double maximum = CurrentAudiobook.FullDuration.TotalSeconds;

            double factor = MousePosition / width;

            int newPositionSeconds = (int)(maximum * factor);

            playerControls.SetAudiobookToPosition(TimeSpan.FromSeconds(newPositionSeconds));
        }

        private void Timeline_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.PushUndoRedoActionTimeline(AudiobookPositionPreDrag, playerControls.GetCurrentAudiobookPosition());

            IsMouseDragging = false;
        }

        private void Timeline_MouseEnter(object sender, MouseEventArgs e)
        {
            var playerControls = GetPlayerControls();

            UpdateTimelineIndicator();

            if (CurrentAudiobook != null)
            {
                IsTimelineIndicatorVisible = true;
                NotifyPropertyChanged("IsTimelineIndicatorVisible");
            }
        }

        private void ClickPlay(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.Play();
        }

        private void ClickPause(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.Pause();
        }

        private void ClickPlayPause(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.PlayPause();
        }

        private void ClickPrevious(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            AudiobookPositionPreDrag = playerControls.GetCurrentAudiobookPosition();

            playerControls.PreviousChapter();
            playerControls.PushUndoRedoActionTimeline(AudiobookPositionPreDrag, playerControls.GetCurrentAudiobookPosition());
        }

        private void ClickNext(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            AudiobookPositionPreDrag = playerControls.GetCurrentAudiobookPosition();

            playerControls.NextChapter();
            playerControls.PushUndoRedoActionTimeline(AudiobookPositionPreDrag, playerControls.GetCurrentAudiobookPosition());
        }

        private void ClickRewind(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.Rewind();
        }

        private void ClickForward(object sender, RoutedEventArgs e)
        {
            var playerControls = GetPlayerControls();

            playerControls.Forward();
        }

        private void GlobalPlayerControls_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        public void SetUndoRedoButtonsVisibility()
        {
            if (App.Globals.undoRedo.CanUndo())
                UndoButton.Visibility = Visibility.Visible;
            else
                UndoButton.Visibility = Visibility.Hidden;

            if (App.Globals.undoRedo.CanRedo())
                RedoButton.Visibility = Visibility.Visible;
            else
                RedoButton.Visibility = Visibility.Hidden;

            NotifyPropertyChanged("CanUndoProperty");
            NotifyPropertyChanged("CanRedoProperty");
        }

        public bool CanUndo()
        {
            return App.Globals.undoRedo.CanUndo();
        }

        public bool CanRedo()
        {
            return App.Globals.undoRedo.CanRedo();
        }

        private void ClickUndo(object sender, RoutedEventArgs e)
        {
            App.Globals.undoRedo.Undo();
        }

        private void ClickRedo(object sender, RoutedEventArgs e)
        {
            App.Globals.undoRedo.Redo();
        }

        private void OnUndo()
        {
            SetUndoRedoButtonsVisibility();
        }

        private void OnRedo()
        {
            SetUndoRedoButtonsVisibility();
        }

        private void OnPushedUndoAction()
        {
            SetUndoRedoButtonsVisibility();
        }

        private void OnRemovedUndoAction()
        {
            SetUndoRedoButtonsVisibility();
        }

        private void ButtonRestore_Click(object sender, RoutedEventArgs e)
        {
            var window = App.Current.MainWindow as NavigationWindow;
            window.NavigationService.GoBack();
        }
    }
}
