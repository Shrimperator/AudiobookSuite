﻿using AudiobookSuite.Commons;
using AudiobookSuite.controls.usercontrols;
using AudiobookSuite.lib;
using Microsoft.Win32;
using SharpDX.DXGI;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using ABI.Windows.Networking.Sockets;
using static Microsoft.WindowsAPICodePack.Shell.PropertySystem.SystemProperties.System;

namespace AudiobookSuite.controls.pages
{
    /// <summary>
    /// Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : Page, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ISettings Settings { get; set; }

        public SettingsPage()
        {
            DataContext = this;
            Settings = App.Globals.settings;

            InitializeComponent();

            UserControl playUserCtrl = App.Globals.playerControls as UserControl;
            Utility.RemoveChild(playUserCtrl.Parent, playUserCtrl);
            Grid_PlayerControls.Children.Add(playUserCtrl);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            InitValues();

            Settings.LoadedSettings += OnLoadedSettings;
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            Settings.LoadedSettings -= OnLoadedSettings;
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void OnLoadedSettings()
        {
            InitValues();

            NotifyPropertyChanged(nameof(Settings));
        }

        public void InitValues()
        {
            Txt_AutosaveInterval.Text = Settings.AutosaveIntervalMinutes.ToString();
            Txt_ForwardBackwardInterval.Text = Settings.ForwardBackwardIntervalSeconds.ToString();
            Txt_FolderSearchDepth.Text = Settings.FolderSearchDepth.ToString();
            Txt_SleepTimerInterval.Text = Settings.SleepTimerInterval.TotalMinutes.ToString();
            Txt_SleepTimerRewindSeconds.Text = Settings.SleepTimerRewindSeconds.ToString();
            Txt_NumRecentlyPlayed.Text = Settings.MaxLastPlayedAudiobooks.ToString();

            DropDownTTSVoiceType.Content = Enum.GetName(typeof(ETTSVoiceType), Settings.TTSVoiceType);
            DropDownATLChapterFormat.Content = Enum.GetName(typeof(EATLReadChaptersFormat), Settings.ATLReadChaptersFormat);
        }

        private void Apply_AutosaveIntervalMinutes()
        {
            int interval = Utility.SanitizeInt(Txt_AutosaveInterval.Text, true, 1, 9999);
            Txt_AutosaveInterval.Text = interval.ToString();

            Settings.AutosaveIntervalMinutes = interval;
        }

        private void Apply_ForwardBackwardInterval()
        {
            int interval = Utility.SanitizeInt(Txt_ForwardBackwardInterval.Text, true, 1, 9999);
            Txt_ForwardBackwardInterval.Text = interval.ToString();

            Settings.ForwardBackwardIntervalSeconds = interval;
        }

        private void Apply_SleepTimerInterval()
        {
            int interval = Utility.SanitizeInt(Txt_SleepTimerInterval.Text, true, 1, 9999);
            Txt_SleepTimerInterval.Text = interval.ToString();

            Settings.SleepTimerInterval = TimeSpan.FromMinutes(interval);
        }

        private void Apply_SleepTimerRewindSeconds()
        {
            int interval = Utility.SanitizeInt(Txt_SleepTimerRewindSeconds.Text, true, 1, 9999);
            Txt_SleepTimerRewindSeconds.Text = interval.ToString();

            Settings.SleepTimerRewindSeconds = interval;
        }

        private void Apply_NumRecentlyPlayed()
        {
            int num = Utility.SanitizeInt(Txt_NumRecentlyPlayed.Text, true, 0, 9999);
            Txt_NumRecentlyPlayed.Text = num.ToString();

            Settings.MaxLastPlayedAudiobooks = num;
        }

        private void Apply_FolderSearchDepth()
        {
            int interval = Utility.SanitizeInt(Txt_FolderSearchDepth.Text, true, 1, 9999);
            Txt_FolderSearchDepth.Text = interval.ToString();

            Settings.FolderSearchDepth = interval;
        }

        private void Button_OpenSaveFolder_Click(object sender, RoutedEventArgs e)
        {
            var settings = App.Globals.settings;

            DebugLog.WriteLine("info: opening explorer directory '" + settings.GetSettingsDirectory() + "'");
            Process.Start(new ProcessStartInfo(settings.GetSettingsDirectory()) { UseShellExecute = true });
        }

        private void Button_Help_Click(object sender, RoutedEventArgs e)
        {
            Dialog_GenericConfirmDeny newDiag = new Dialog_GenericConfirmDeny("Open Help Page", "This will open an external browser page to display AudiobookSuite's wiki page.");

            Window parentWindow = Window.GetWindow(this);
            newDiag.Owner = parentWindow;

            IPlayerControls controls = App.Globals.playerControls;
            controls.SetInputDisabled("message");

            if (newDiag.ShowDialog() == true)
            {
                string url = "https://gitlab.com/Shrimperator/AudiobookSuite/-/wikis/home";
                Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
            }

            controls.SetInputEnabled("message");
        }

        private void ShowConsole_Click(object sender, RoutedEventArgs e)
        {
            ConsoleAllocator.ToggleConsoleWindow();
            Settings.IsConsoleOpen = ConsoleAllocator.IsConsoleOpen();
        }

        private void Button_ExportSettings_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "AudiobookSuite Settings (*.json)|*.json";

            bool? saveFileResult = saveFileDialog.ShowDialog();
            if (saveFileResult == true)
            {
                string path = saveFileDialog.FileName;

                if (Settings != null)
                {
                    Settings.SaveSettings(ESaveCause.Manual, path);
                }
            }
        }

        private void Button_ImportSettings_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "AudiobookSuite Settings (*.json)|*.json";

            bool? openFileResult = openFileDialog.ShowDialog();
            if (openFileResult == true)
            {
                string path = openFileDialog.FileName;

                if (Settings != null)
                {
                    Settings.LoadSettings(path);
                }
            }
        }

        private void BtnChangeScanPaths_Click(object sender, RoutedEventArgs e)
        {
            PathListDialog pathListDialog = new PathListDialog(Settings.ScanDirectories, "Scan Path Directories");

            Window parentWindow = Window.GetWindow(this);
            pathListDialog.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_changescanpaths");

            if (pathListDialog.ShowDialog() == true)
            {
                Settings.ScanDirectories = pathListDialog.PathList;
            }

            controls.SetInputEnabled("menu_changescanpaths");
        }

        private void Txt_AutosaveInterval_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Apply_AutosaveIntervalMinutes();
        }

        private void Txt_ForwardBackwardInterval_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Apply_ForwardBackwardInterval();
        }

        private void Txt_FolderSearchDepth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Apply_FolderSearchDepth();
        }

        private void Txt_SleepTimerInterval_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Apply_SleepTimerInterval();
        }

        private void Txt_SleepTimerRewindSeconds_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Apply_SleepTimerRewindSeconds();
        }

        private void Txt_NumRecentlyPlayed_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Apply_NumRecentlyPlayed();
        }

        private void ResetShortcuts_Click(object sender, RoutedEventArgs e)
        {
            App.Globals.inputListener.InitDefaultKeyBindings();

            KeyBindingPanel.Reconstruct();
        }

        private void SelectMaleVoice_Click(object sender, RoutedEventArgs e)
        {
            DropDownTTSVoiceType.IsOpen = false;

            Settings.TTSVoiceType = ETTSVoiceType.Male;

            DropDownTTSVoiceType.Content = Enum.GetName(typeof(ETTSVoiceType), Settings.TTSVoiceType);
        }

        private void SelectFemaleVoice_Click(object sender, RoutedEventArgs e)
        {
            DropDownTTSVoiceType.IsOpen = false;

            Settings.TTSVoiceType = ETTSVoiceType.Female;

            DropDownTTSVoiceType.Content = Enum.GetName(typeof(ETTSVoiceType), Settings.TTSVoiceType);
        }

        private void ButtonShortcutsInfo_Click(object sender, RoutedEventArgs e)
        {
            Dialog_Message newWindow = new Dialog_Message
                ("Shortcuts",
                "To customize shortcuts, click the '+' button next to the shortcut" +
                " you're interested in. A new button will appear next to the shortcut." +
                " Click it, then enter the key that should trigger the shortcut.\n\n" +
                "If you add more triggers to a shortcut" +
                " than just one, all of them have to be pressed at the same time.\n\n" +
                "XBox gamepad buttons are supported, though other gamepads may or may not work.\n\n" +
                "The 'Requires Focus' checkbox determines whether the app needs to have keyboard focus" +
                " in order for the shortcut to be recognized.");

            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_message");

            newWindow.ShowDialog();

            controls.SetInputEnabled("menu_message");
        }

        private void DropDownATLChapterFormat_PreferQuickTime_Click(object sender, RoutedEventArgs e)
        {
            Settings.ATLReadChaptersFormat = EATLReadChaptersFormat.PreferQuickTime;
            ATL.Settings.MP4_readChaptersFormat = (int)Settings.ATLReadChaptersFormat;

            DropDownATLChapterFormat.Content = Enum.GetName(typeof(EATLReadChaptersFormat), Settings.ATLReadChaptersFormat);
        }

        private void DropDownATLChapterFormat_QuickTime_Click(object sender, RoutedEventArgs e)
        {
            Settings.ATLReadChaptersFormat = EATLReadChaptersFormat.QuickTime;
            ATL.Settings.MP4_readChaptersFormat = (int)Settings.ATLReadChaptersFormat;

            DropDownATLChapterFormat.Content = Enum.GetName(typeof(EATLReadChaptersFormat), Settings.ATLReadChaptersFormat);
        }

        private void DropDownATLChapterFormat_PreferNero_Click(object sender, RoutedEventArgs e)
        {
            Settings.ATLReadChaptersFormat = EATLReadChaptersFormat.PreferNero;
            ATL.Settings.MP4_readChaptersFormat = (int)Settings.ATLReadChaptersFormat;

            DropDownATLChapterFormat.Content = Enum.GetName(typeof(EATLReadChaptersFormat), Settings.ATLReadChaptersFormat);
        }

        private void DropDownATLChapterFormat_Nero_Click(object sender, RoutedEventArgs e)
        {
            Settings.ATLReadChaptersFormat = EATLReadChaptersFormat.Nero;
            ATL.Settings.MP4_readChaptersFormat = (int)Settings.ATLReadChaptersFormat;

            DropDownATLChapterFormat.Content = Enum.GetName(typeof(EATLReadChaptersFormat), Settings.ATLReadChaptersFormat);
        }

        private void ButtonChapterFormatInfo_OnClick(object sender, RoutedEventArgs e)
        {
            Dialog_Message newWindow = new Dialog_Message
            ("Shortcuts",
            "Audiofiles may use QuickTime or Nero chapter metadata (or both at the same time), " +
            "depending on the software used to encode the file. If the chapters aren't " +
            "read correctly by AudiobookSuite, change this around and see if it helps. " +
            "Some software that edits chapter metadata might change Nero chapters, but leave " +
            "QuickTime chapters intact or vice versa for example.");

            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_message");

            newWindow.ShowDialog();

            controls.SetInputEnabled("menu_message");
        }
    }
}
