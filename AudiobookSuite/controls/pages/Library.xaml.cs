﻿using AudiobookSuite.Commons;
using AudiobookSuite.controls.usercontrols;
using AudiobookSuite.lib;
using Microsoft.UI.Xaml.Controls;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Border = System.Windows.Controls.Border;
using Expander = System.Windows.Controls.Expander;
using Grid = System.Windows.Controls.Grid;
using Page = System.Windows.Controls.Page;
using ScrollViewer = System.Windows.Controls.ScrollViewer;
using TextBlock = System.Windows.Controls.TextBlock;
using TextChangedEventArgs = System.Windows.Controls.TextChangedEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace AudiobookSuite
{
    public class GroupSorter : IComparer
    {
        public int Compare(object? x, object? y)
        {
            CollectionViewGroup xGroup = x as CollectionViewGroup;
            CollectionViewGroup yGroup = y as CollectionViewGroup;

            if (xGroup == null || yGroup == null)
                return -1;

            string defaultGroupName = App.Globals.groupManager.GetDefaultGroupName();

            // sort default group at the very top
            if (yGroup.Name.ToString() == defaultGroupName)
                return 1;

            return String.Compare(xGroup.Name.ToString(), yGroup.Name.ToString());
        }
    }

    public partial class Library : Page, ILibrary, INotifyPropertyChanged, IUndoRedoHandler
    {
        public static Library _Instance = null;
        public static Library Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new Library();
                return _Instance;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<LibraryCollectionItem> LibraryAudiobooksFlattened { get; set; }
        public CollectionViewSource LibraryCollectionViewSource { get; set; }

        public bool VirtualizeDataGrid { get; set; } = true;

        private Window ParentWindow;

        public bool IsScanning { get { return App.Globals.audiobookScanner.IsScanning(); } }

        private double ScrollViewerVerticalOffset { get; set; }

        private string _SearchString = "";
        public string SearchString
        {
            get => _SearchString;
            set
            {
                if (_SearchString != value)
                {
                    _SearchString = value;
                    App.Globals.settings.SearchFilter = value;
                }
            }
        }

        public Library()
        {
            DataContext = this;

            SearchString = App.Globals.settings.SearchFilter;

            InitializeComponent();

            LibraryAudiobooksFlattened = new ObservableCollection<LibraryCollectionItem>();

            LibraryCollectionViewSource = new CollectionViewSource { Source = LibraryAudiobooksFlattened };
            NotifyPropertyChanged("LibraryCollectionViewSource");

            LibraryCollectionViewSource.SortDescriptions.Clear();
            LibraryCollectionViewSource.SortDescriptions.Add(new SortDescription("GroupName", ListSortDirection.Ascending));
            LibraryCollectionViewSource.SortDescriptions.Add(new SortDescription("LinkedAudiobook.CreationDate", ListSortDirection.Descending));

            LibraryDataGrid.ItemContainerGenerator.StatusChanged += DataGridStatusChanged;

            DataGridStatusTimer = new DispatcherTimer();
            DataGridStatusTimer.Interval = TimeSpan.FromMilliseconds(10);
            DataGridStatusTimer.Tick += DataGridStatusSetFinished;

            if (App.Globals.audiobookScanner == null)
                return;

            EScanState scanState = App.Globals.audiobookScanner.GetScannerState();
            int scanProgress = App.Globals.audiobookScanner.GetScannerProgress();
            OnScannerUpdated(scanState, scanProgress);

            if (scanState != EScanState.Idle)
                OnScannerStarted();

            SaveLibraryLayoutDefault();
            ResetLibraryLayout();

            SetupCollectionFilters();
            SetupSortingGroups();
        }

        private DispatcherTimer DataGridStatusTimer;
        private bool IsDataGridLoading = false;

        private void SetDataGridLoading(bool mode)
        {
            IsDataGridLoading = mode;

            ScrollViewer scrollViewer =  Utility.GetScrollViewer(LibraryDataGrid);
            if (scrollViewer != null)
            {
                scrollViewer.IsEnabled = !mode;
            }
        }

        private void DataGridStatusChanged(object sender, EventArgs e)
        {
            if (IsDataGridLoading && LibraryDataGrid.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                DataGridStatusTimer.Stop();
                DataGridStatusTimer.Start();
            }
        }

        private void DataGridStatusSetFinished(object sender, EventArgs e)
        {
            DataGridStatusTimer.Stop();

            SetDataGridLoading(false);

            /*Utility.DelayedAction(new Action(() =>
            {
                ResetLibraryScrollOffset();
            }));*/
        }

        void OnLoaded(object sender, EventArgs e)
        {
            App.Globals.audiobookScanner.StartedScanning += OnScannerStarted;
            App.Globals.audiobookScanner.FinishedScanning += OnScannerFinished;
            App.Globals.audiobookScanner.UpdatedScanning += OnScannerUpdated;
            App.Globals.audiobookScanner.AddedAudiobook += OnAddedAudiobook;
            App.Globals.audiobookScanner.RemovedAudiobook += OnRemovedAudiobook;
            App.Globals.audiobookScanner.LoadedAudiobooks += OnScannerLoadedAudiobooks;
            App.Globals.audiobookScanner.FinishedScanning += OnFinishedScanning;
            App.Globals.settings.ChangeOption += OnChangeOption;
            App.Globals.groupManager.RemovedGroup += OnRemovedGroup;
            Application.Current.Exit += OnUnloaded;

            ((IUndoRedoHandler)this).InitUndoRedo();

            SetUndoRedoButtonsVisibility();

            ParentWindow = Window.GetWindow(this);

            UserControl playUserCtrl = App.Globals.playerControls as UserControl;
            Utility.RemoveChild(playUserCtrl.Parent, playUserCtrl);
            Grid_PlayerControls.Children.Add(playUserCtrl);
        }

        void OnUnloaded(object sender, EventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.ClearInputDisabled();

            LibraryDataGrid.CommitEdit(DataGridEditingUnit.Row, true);

            App.Globals.audiobookScanner.StartedScanning -= OnScannerStarted;
            App.Globals.audiobookScanner.FinishedScanning -= OnScannerFinished;
            App.Globals.audiobookScanner.UpdatedScanning -= OnScannerUpdated;
            App.Globals.audiobookScanner.AddedAudiobook -= OnAddedAudiobook;
            App.Globals.audiobookScanner.RemovedAudiobook -= OnRemovedAudiobook;
            App.Globals.audiobookScanner.FinishedScanning -= OnFinishedScanning;

            ((IUndoRedoHandler)this).RemoveUndoRedo();

            App.Globals.settings.ChangeOption -= OnChangeOption;

            Application.Current.Exit -= OnUnloaded;

            SaveLibraryLayout();
        }

        public void OnChangeOption(string identifier)
        {
            if (identifier == "LibraryFilters")
                ApplyFilters();
        }

        public void SaveLibraryLayoutDefault()
        {
            /* only allow once */
            if (App.Globals.settings.LibraryColumnsDefault.Count > 0)
                return;

            List<IDataGridLayoutInfoSettings> layoutInfos = new List<IDataGridLayoutInfoSettings>();
            for (int i = 0; i < LibraryDataGrid.Columns.Count; i++)
            {
                DataGridColumn currentColumn = LibraryDataGrid.Columns[i];

                IDataGridLayoutInfoSettings newInfo = new DataGridLayoutInfoSettings(
                    currentColumn.DisplayIndex,
                    currentColumn.Width,
                    currentColumn.SortDirection);

                layoutInfos.Add(newInfo);
            }

            App.Globals.settings.LibraryColumnsDefault = layoutInfos;
        }

        public void ResetLibraryLayoutDefault()
        {
            List<IDataGridLayoutInfoSettings> columns = App.Globals.settings.LibraryColumnsDefault;
            LibraryCollectionViewSource.SortDescriptions.Clear();
            LibraryCollectionViewSource.SortDescriptions.Add(new SortDescription("GroupName", ListSortDirection.Ascending));

            for (int i = 0; i < LibraryDataGrid.Columns.Count && i < columns.Count; i++)
            {
                DataGridColumn currentColumn = LibraryDataGrid.Columns[i];
                IDataGridLayoutInfoSettings columnLayoutInfo = columns[i];

                currentColumn.Width = columnLayoutInfo.Width;
                if (columnLayoutInfo.DisplayIndex >= 0)
                {
                    currentColumn.DisplayIndex = columnLayoutInfo.DisplayIndex;

                    if (columnLayoutInfo.SortDirection == null)
                    {
                        currentColumn.SortDirection = null;
                        continue;
                    }

                    ListSortDirection sortDirection = (ListSortDirection)columnLayoutInfo.SortDirection;
                    currentColumn.SortDirection = sortDirection;

                    SortDirection = sortDirection;
                    SortColumn = currentColumn;

                    LibraryCollectionViewSource.SortDescriptions.Add(new SortDescription(currentColumn.SortMemberPath, sortDirection));
                }
            }

            SetupCollectionFilters();
            SetupSortingGroups();
        }

        public void ResetLibraryLayout()
        {
            List<IDataGridLayoutInfoSettings> columns = App.Globals.settings.LibraryColumns;
            LibraryCollectionViewSource.SortDescriptions.Clear();
            LibraryCollectionViewSource.SortDescriptions.Add(new SortDescription("GroupName", ListSortDirection.Ascending));

            for (int i = 0; i < LibraryDataGrid.Columns.Count && i < columns.Count; i++)
            {
                DataGridColumn currentColumn = LibraryDataGrid.Columns[i];
                IDataGridLayoutInfoSettings columnLayoutInfo = columns[i];

                currentColumn.Width = columnLayoutInfo.Width;
                if (columnLayoutInfo.DisplayIndex >= 0)
                {
                    currentColumn.DisplayIndex = columnLayoutInfo.DisplayIndex;

                    if (columnLayoutInfo.SortDirection == null)
                    {
                        currentColumn.SortDirection = null;
                        continue;
                    }

                    ListSortDirection sortDirection = (ListSortDirection)columnLayoutInfo.SortDirection;
                    currentColumn.SortDirection = sortDirection;

                    SortDirection = sortDirection;
                    SortColumn = currentColumn;

                    LibraryCollectionViewSource.SortDescriptions.Add(new SortDescription(currentColumn.SortMemberPath, sortDirection));
                }
            }
        }

        public void SaveLibraryLayout()
        {
            List<IDataGridLayoutInfoSettings> layoutInfos = new List<IDataGridLayoutInfoSettings>();
            for (int i = 0; i < LibraryDataGrid.Columns.Count; i++)
            {
                DataGridColumn currentColumn = LibraryDataGrid.Columns[i];

                IDataGridLayoutInfoSettings newInfo = new DataGridLayoutInfoSettings(
                    currentColumn.DisplayIndex,
                    currentColumn.Width,
                    currentColumn.SortDirection);

                layoutInfos.Add(newInfo);
            }

            App.Globals.settings.LibraryColumns = layoutInfos;
        }

        public void AddToFlattenedList(IAudiobook audiobook)
        {
            if (audiobook == null)
                return;

            // only add books that don't already exist
            if (!LibraryAudiobooksFlattened.Any(p => p.LinkedAudiobook == audiobook))
            {
                LibraryAudiobooksFlattened.Add(new LibraryCollectionItem(App.Globals.groupManager.GetDefaultGroupName(), audiobook));

                foreach (ISortingGroup group in audiobook.Groups)
                {
                    LibraryAudiobooksFlattened.Add(new LibraryCollectionItem(group.GroupName, audiobook));
                }
            }
        }

        public void RemoveFromGroup(IAudiobook audiobook, ISortingGroup group)
        {
            if (audiobook == null)
                return;

            for (int i = 0; i < LibraryAudiobooksFlattened.Count; i++)
            {
                LibraryCollectionItem collectionItem = LibraryAudiobooksFlattened[i];
                if (collectionItem.LinkedAudiobook == audiobook && collectionItem.GroupName == group.GroupName)
                {
                    LibraryAudiobooksFlattened.RemoveAt(i);
                    i--;
                }
            }
        }

        public void RemoveFromFlattenedList(IAudiobook audiobook)
        {
            if (audiobook == null)
                return;

            for (int i = 0; i < LibraryAudiobooksFlattened.Count; i++)
            {
                LibraryCollectionItem collectionItem = LibraryAudiobooksFlattened[i];
                if (collectionItem.LinkedAudiobook == audiobook)
                {
                    LibraryAudiobooksFlattened.RemoveAt(i);
                    i--;
                }
            }
        }

        private void CollectionFilter(object sender, FilterEventArgs e)
        {
            var collectionItem = (LibraryCollectionItem)e.Item;

            if (collectionItem.LinkedAudiobook != null)
            {
                IAudiobook audiobook = collectionItem.LinkedAudiobook;

                foreach (LibraryFilter filter in App.Globals.settings.LibraryFilters)
                {
                    if (!(filter.CheckFilter(audiobook)))
                    {
                        e.Accepted = false;
                        return;
                    }
                }

                if (String.IsNullOrEmpty(SearchString))
                {
                    e.Accepted = true;
                    return;
                }

                string searchLower = SearchString.ToLower();

                if (audiobook.Title.ToLower().Contains(searchLower) ||
                    audiobook.JoinedAuthors.ToLower().Contains(searchLower) ||
                    audiobook.JoinedNarrators.ToLower().Contains(searchLower))
                {
                    e.Accepted = true;
                    return;
                }
            }

            e.Accepted = false;
        }

        public void SetupSortingGroups()
        {
            try
            {
                LibraryCollectionViewSource.GroupDescriptions.Clear();
                var groupDesc = new PropertyGroupDescription("GroupName");
                groupDesc.CustomSort = new GroupSorter();
                groupDesc.StringComparison = StringComparison.OrdinalIgnoreCase;
                LibraryCollectionViewSource.GroupDescriptions.Add(groupDesc);

                /* DataGrid sorting doesn't support one item being part of multiple groups, so the list has to be flattened into
                    * key value pairs with the group-title as key. An audiobook can be contained in
                    * LibraryAudiobooksFlattened multiple times */
                var audiobooks = App.Globals.audiobookScanner.AudioBookList;

                ISortingGroup defaultGroup = App.Globals.groupManager.DefaultGroup;

                LibraryAudiobooksFlattened.Clear();

                if (defaultGroup == null)
                {
                    DebugLog.WriteLine("Error in Library.xaml.cs: App.Globals.groupManager.DefaultGroup not defined");
                    return;
                }

                Dispatcher.BeginInvoke(() =>
                {
                    lock (audiobooks)
                    {
                        foreach (IAudiobook audiobook in audiobooks)
                        {
                            LibraryAudiobooksFlattened.Add(new LibraryCollectionItem(defaultGroup.GroupName, audiobook));
                            foreach (ISortingGroup group in audiobook.Groups)
                            {
                                LibraryAudiobooksFlattened.Add(new LibraryCollectionItem(group.GroupName, audiobook));
                            }
                        }
                    }
                }, DispatcherPriority.Send);
                
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("SetupSortingGroups, exception: " + e.Message + "\n\nat " + e.TargetSite + " in " + e.Source + "\n\n" + e.StackTrace);
            }
        }

        /* viewsource is an interface between data backend and WPF frontend, can contain 
         * efficient filters, sorting etc. without altering the actual data */
        public void SetupCollectionFilters()
        {
            try
            {
                if (LibraryCollectionViewSource == null)
                    return;

                Dispatcher.BeginInvoke(() =>
                {
                    LibraryCollectionViewSource.Filter -= CollectionFilter;
                    LibraryCollectionViewSource.Filter += CollectionFilter;
                }, DispatcherPriority.Send);

                Filter_HideFinished.IsChecked = App.Globals.settings.IsLibraryFilterActive("HideFinished");
                Filter_ShowHidden.IsChecked = App.Globals.settings.IsLibraryFilterActive("ShowOnlyHidden");
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("SetupCollectionFilters, exception: " + e.Message + "\n\nat " + e.TargetSite + " in " + e.Source + "\n\n" + e.StackTrace);
            }
        }

        public void OnScannerStarted()
        {
            /* not sure why, but if called from main thread freezes thread */
            LoadingSpinner.Dispatcher.BeginInvoke((Action)(() =>
            {
                LoadingSpinner.Visibility = Visibility.Visible;
            }));
        }

        public void OnScannerFinished()
        {
            /* not sure why, but if called from main thread freezes thread */
            LoadingSpinner.Dispatcher.BeginInvoke((Action)(() =>
            {
                LoadingSpinner.Visibility = Visibility.Hidden;
            }));
        }

        public void OnAddedAudiobook(IAudiobook newBook)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                AddToFlattenedList(newBook);
                NotifyPropertyChanged("LibraryCollectionViewSource");
            });
        }

        public void OnRemovedAudiobook(IAudiobook removedBook)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                RemoveFromFlattenedList(removedBook);

                /* audiobook was removed, stop playing */
                IPlayerControls pControls = App.Globals.playerControls;
                if (pControls != null && pControls.CurrentAudiobook == removedBook)
                {
                    pControls.Stop();
                }

                NotifyPropertyChanged("LibraryCollectionViewSource");
            });
        }

        public void OnFinishedScanning()
        {
            NotifyPropertyChanged("IsScanning");
        }

        public void OnScannerLoadedAudiobooks()
        {
            ReloadLibrary();
        }

        public void OnScannerUpdated(EScanState scanState, int progress)
        {
            switch (scanState)
            {
                case (EScanState.Gathering):
                    LoadingSpinnerLabel.Visibility = Visibility.Visible;
                    LoadingSpinnerLabel.Content = "Indexing Files...";
                    break;
                case (EScanState.Indexing):
                    LoadingSpinnerLabel.Visibility = Visibility.Visible;
                    LoadingSpinnerLabel.Content = "Creating Audiobooks...";
                    break;
                case (EScanState.Sorting):
                    LoadingSpinnerLabel.Visibility = Visibility.Visible;
                    LoadingSpinnerLabel.Content = "Sorting...";
                    break;
                default:
                    LoadingSpinnerLabel.Visibility = Visibility.Hidden;
                    LoadingSpinnerLabel.Content = "Idle";
                    break;
            }
        }

        public void RefreshView()
        {
            try
            {
                if (LibraryCollectionViewSource != null)
                {
                    LibraryCollectionViewSource.View.Refresh();
                }
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Exception during RefreshView: " + e.Message);
            }
        }

        public void ReloadLibrary()
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                SetupSortingGroups();
                NotifyPropertyChanged("LibraryCollectionViewSource");
                ApplyFilters();
            });
        }

        public void ApplyFilters()
        {
            RefreshView();
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Click_RefreshMetadata(object sender, RoutedEventArgs e)
        {
            List<IAudiobook> audiobooks = GetSelectedAudiobooks();
            foreach (var item in audiobooks)
            {
                Audiobook audiobook = (Audiobook)item;
                audiobook.RefreshMetadata();
                audiobook.CacheDuration();
            }

            RefreshView();
        }

        public IAudiobook GetSelectedAudiobook()
        {
            var selectedCollection = (LibraryCollectionItem)LibraryDataGrid.SelectedItem;

            if (selectedCollection != null)
                return (IAudiobook)selectedCollection.LinkedAudiobook;

            return null;
        }

        public List<IAudiobook> GetSelectedAudiobooks()
        {
            List<IAudiobook> audiobooks = LibraryDataGrid.SelectedItems.Cast<LibraryCollectionItem>().Select(x => x.LinkedAudiobook).Distinct().ToList();

            return audiobooks;
        }

        private void Click_AudiobookDetails(object sender, RoutedEventArgs e)
        {
            IAudiobook selectedAudiobook = GetSelectedAudiobook();
            OpenAudiobookDetails(selectedAudiobook);
        }

        public void OpenAudiobookDetails(IAudiobook audiobook)
        {
            AudiobookDetails audiobookDetails = new AudiobookDetails(audiobook);
            NavigationService.Navigate(audiobookDetails);
        }

        private void LibraryDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (e.ChangedButton != MouseButton.Left)
                return;

            IAudiobook selectedAudiobook = GetSelectedAudiobook();

            if (selectedAudiobook != null)
                App.Globals.playerControls.PlayAudiobook(selectedAudiobook);
        }

        private void LibraryDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                List<IAudiobook> audiobooks = GetSelectedAudiobooks();

                OpenRemoveAudiobooksDialog(audiobooks);
            }
        }

        public void OpenDeleteGroupDialog(ISortingGroup group)
        {
            if (group == null)
                return;

            Dialog_ConfirmDeleteGroup newWindow = new Dialog_ConfirmDeleteGroup(group);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            if (newWindow.ShowDialog() == true)
            {
                App.Globals.groupManager.RemoveGroup(group);
            }
        }

        public void OpenRenameGroupDialog(ISortingGroup group)
        {
            if (group == null)
                return;

            Dialog_Rename newWindow = new Dialog_Rename(group.GroupName);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_renamegroup");

            if (newWindow.ShowDialog() == true)
            {
                App.Globals.groupManager.RenameGroup(group, newWindow.NewNameText);

                ReloadLibrary();
            }

            controls.SetInputEnabled("menu_renamegroup");
        }

        public void OpenRemoveEmptyAudiobooksDialog(List<IAudiobook> audiobooks)
        {
            Dialog_ConfirmDeleteAudiobooks newWindow = new Dialog_ConfirmDeleteAudiobooks(audiobooks);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_removeaudiobook");

            if (newWindow.ShowDialog() == true)
            {
                foreach (var item in audiobooks)
                {
                    IAudiobook audiobook = (IAudiobook)item;
                    RemoveAudiobook(audiobook);
                }
            }

            controls.SetInputEnabled("menu_removeaudiobook");
        }

        public void OpenRemoveAudiobooksDialog(List<IAudiobook> audiobooks)
        {
            Dialog_ConfirmDeleteAudiobooks newWindow = new Dialog_ConfirmDeleteAudiobooks(audiobooks);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_removeaudiobook");

            if (newWindow.ShowDialog() == true)
            {
                foreach (var item in audiobooks)
                {
                    IAudiobook audiobook = (IAudiobook)item;
                    RemoveAudiobook(audiobook);
                }
            }

            controls.SetInputEnabled("menu_removeaudiobook");
        }

        public void OpenMessageDialog(string title, List<Run> messageParts)
        {
            Dialog_Message newWindow = new Dialog_Message(title, messageParts);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_message");

            newWindow.ShowDialog();

            controls.SetInputEnabled("menu_message");
        }

        public void OpenMessageDialog(string title, string message)
        {
            Dialog_Message newWindow = new Dialog_Message(title, message);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_message");

            newWindow.ShowDialog();

            controls.SetInputEnabled("menu_message");
        }

        public void RemoveAudiobook(IAudiobook audiobook)
        {
            App.Globals.audiobookScanner.RemoveAudiobook(audiobook);

            IPlayerControls pControls = App.Globals.playerControls;
            if (pControls != null && pControls.CurrentAudiobook == audiobook)
            {
                pControls.Stop();
            }
        }

        private void ClickRescanLibrary(object sender, RoutedEventArgs e)
        {
            var scanDirs = App.Globals.settings.ScanDirectories;
            IAudiobookScanner scanner = App.Globals.audiobookScanner;

            if (!App.Globals.settings.HasValidScanDirectory())
            {
                DebugLog.WriteLine("warning: cannot rescan library: no valid ScanDirectory is set");

                var textA = new Run("You don't have a scan directory setup yet, so AudiobookSuite doesn't know where to look for your files. Head to the settings page, click on the");
                var textB = new Run(" \"Change Paths\"") { FontWeight = FontWeights.Bold };
                var textC = new Run(" button and add at least one valid directory.");

                var textParts = new List<Run>();
                textParts.Add(textA);
                textParts.Add(textB);
                textParts.Add(textC);

                OpenMessageDialog("Info:", textParts);
                return;
            }

            if (IsScanning)
            {
                scanner.Cancel();
                NotifyPropertyChanged("IsScanning");
            }
            else
            {
                scanner.ScanDirectories(scanDirs);
                NotifyPropertyChanged("IsScanning");
            }
        }

        private void ClickSaveLibrary(object sender, RoutedEventArgs e)
        {
            App.Globals.audiobookScanner.SaveBooksToFile();
        }

        private void LibraryDataGrid_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                List<String> scanDirectories = new List<string>();

                foreach (string file in files)
                {
                    if (Directory.Exists(file))
                    {
                        scanDirectories.Add(file);
                    }
                    else
                        App.Globals.audiobookScanner.AddAudioFile(file);
                }

                App.Globals.audiobookScanner.ScanDirectories(scanDirectories);
            }
        }

        private void LibraryDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var controls = App.Globals.playerControls;

            /* ignore space key presses except when in edit mode */
            if (e.Key == Key.Space && controls.IsInputEnabled())
            {
                e.Handled = true;
            }
        }

        private void LibraryDataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("datagrid");
        }

        private void LibraryDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputEnabled("datagrid");
        }

        private void LibraryDataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputEnabled("datagrid");
        }

        private void Click_SetIcon(object sender, RoutedEventArgs e)
        {
            IAudiobook selectedAudiobook = GetSelectedAudiobook();
            if (selectedAudiobook != null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "All Images Files (*.png;*.jpeg;*.gif;*.jpg;*.bmp;*.tiff;*.tif;*.mp3;*.mp4;*.m4b;*.m4a)|*.png;*.jpeg;*.gif;*.jpg;*.bmp;*.tiff;*.tif;*.mp3;*.mp4;*.m4b;*.m4a;" +
                "|PNG Portable Network Graphics (*.png)|*.png" +
                "|JPEG File Interchange Format (*.jpg *.jpeg *jfif)|*.jpg;*.jpeg;*.jfif" +
                "|BMP Windows Bitmap (*.bmp)|*.bmp" +
                "|TIF Tagged Imaged File Format (*.tif *.tiff)|*.tif;*.tiff" +
                "|GIF Graphics Interchange Format (*.gif)|*.gif" +
                "|Embedded audio file covers (*.mp3;*.mp4;*.m4b;*.m4a)|*.mp3;*.mp4;*.m4b;*.m4a" +
                "|All Files|*";

                if (openFileDialog.ShowDialog() == true)
                {
                    selectedAudiobook.IconPath = openFileDialog.FileName;

                    /* allow one autosave */
                    if (App.Globals.audiobookScanner != null)
                        App.Globals.audiobookScanner.MarkChanged();
                }
            }
        }

        private void Click_AddToGroup(object sender, RoutedEventArgs e)
        {
            List<IAudiobook> audiobooks = GetSelectedAudiobooks();

            Dialog_AddToGroup newWindow = new Dialog_AddToGroup();
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_addtogroup");

            if (newWindow.ShowDialog() == true)
            {
                List<ISortingGroup> groups = new List<ISortingGroup>();
                foreach (string groupName in newWindow.GroupNames)
                {
                    if (!App.Globals.groupManager.DoesGroupExist(groupName))
                        App.Globals.groupManager.AddGroup(groupName);
                    groups.Add(App.Globals.groupManager.GetGroup(groupName));
                }

                foreach (IAudiobook audiobook in audiobooks)
                {
                    foreach (ISortingGroup group in groups)
                    {
                        if (!audiobook.Groups.Contains(group))
                        {
                            audiobook.AddToGroup(group);
                            LibraryAudiobooksFlattened.Add(new LibraryCollectionItem(group.GroupName, audiobook));
                        }
                    }
                }
            }

            controls.SetInputEnabled("menu_addtogroup");
        }

        private void Click_ClearGroups(object sender, RoutedEventArgs e)
        {
            List<IAudiobook> audiobooks = GetSelectedAudiobooks();

            foreach (IAudiobook audiobook in audiobooks)
            {
                App.Globals.groupManager.RemoveFromAllGroups(audiobook);

                for (int i = 0; i < LibraryAudiobooksFlattened.Count; i++)
                {
                    LibraryCollectionItem item = LibraryAudiobooksFlattened[i];
                    if (item.LinkedAudiobook == audiobook && item.GroupName != App.Globals.groupManager.GetDefaultGroupName())
                    {
                        LibraryAudiobooksFlattened.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        private void Click_OpenFolder(object sender, RoutedEventArgs e)
        {
            IAudiobook audiobook = GetSelectedAudiobook();

            if (audiobook != null)
            {
                DebugLog.WriteLine("info: opening explorer directory '" + audiobook.AudiobookPath + "'");
                Process.Start(new ProcessStartInfo(audiobook.AudiobookPath) { UseShellExecute = true });
            }
        }

        private void Click_MarkRead(object sender, RoutedEventArgs e)
        {
            List<IAudiobook> audiobooks = GetSelectedAudiobooks();

            foreach (IAudiobook audiobook in audiobooks)
            {
                if (audiobook.IsFinished)
                {
                    audiobook.MarkUnread();
                }
                else
                {
                    audiobook.MarkRead();

                    IPlayerControls pControls = App.Globals.playerControls;
                    if (pControls != null && pControls.CurrentAudiobook == audiobook)
                    {
                        pControls.Stop();
                    }
                }
            }
        }

        private void Click_MarkHidden(object sender, RoutedEventArgs e)
        {
            List<IAudiobook> audiobooks = GetSelectedAudiobooks();

            foreach (IAudiobook audiobook in audiobooks)
            {
                if (audiobook != null)
                {
                    audiobook.IsHidden = !audiobook.IsHidden;
                }
            }

            ApplyFilters();
        }

        private void Click_Details(object sender, RoutedEventArgs e)
        {
            IAudiobook selectedAudiobook = GetSelectedAudiobook();
            if (selectedAudiobook != null)
            {
                OpenAudiobookDetails(selectedAudiobook);
            }
        }

        private void Click_DeleteEmpty(object sender, RoutedEventArgs e)
        {
            List<IAudiobook> selectedAudiobooks = GetSelectedAudiobooks();
            List<IAudiobook> invalidAudiobooks = new List<IAudiobook>();

            foreach(IAudiobook audiobook in selectedAudiobooks)
            {
                bool isValid = false;

                foreach (IAudioFile file in audiobook.AudioFiles)
                {
                    if (file != null && File.Exists(file.AudioFilePath))
                    {
                        isValid = true;
                        break;
                    }
                }

                if (!isValid)
                    invalidAudiobooks.Add(audiobook);
            }

            

            if (invalidAudiobooks.Count > 0)
            {
                OpenRemoveEmptyAudiobooksDialog(invalidAudiobooks);
            }
        }

        private void Click_Delete(object sender, RoutedEventArgs e)
        {
            List<IAudiobook> audiobooks = GetSelectedAudiobooks();

            if (audiobooks.Count > 0)
            {
                OpenRemoveAudiobooksDialog(audiobooks);
            }
        }

        private void Click_Play(object sender, RoutedEventArgs e)
        {
            IPlayerControls pControls = App.Globals.playerControls;
            IAudiobook selectedAudiobook = GetSelectedAudiobook();
            if (pControls != null && selectedAudiobook != null)
            {
                pControls.PlayAudiobook(selectedAudiobook);
            }
        }

        private void Click_CreateGroupsFromGenres(object sender, RoutedEventArgs e)
        {
            List<IAudiobook> selectedAudiobooks = GetSelectedAudiobooks();
            IAudiobookScanner scanner = App.Globals.audiobookScanner;

            if (scanner == null)
                return;

            foreach (IAudiobook audiobook in selectedAudiobooks)
            {
                List<string> newGroups = scanner.CreateGenreGroups(audiobook);
                if (newGroups == null)
                    continue;

                foreach (string groupName in newGroups)
                {
                    LibraryAudiobooksFlattened.Add(new LibraryCollectionItem(groupName, audiobook));
                }
            }
        }

        public void SetUndoRedoButtonsVisibility()
        {
            if (App.Globals.undoRedo.CanUndo())
                UndoButton.Visibility = Visibility.Visible;
            else
                UndoButton.Visibility = Visibility.Hidden;

            if (App.Globals.undoRedo.CanRedo())
                RedoButton.Visibility = Visibility.Visible;
            else
                RedoButton.Visibility = Visibility.Hidden;

            NotifyPropertyChanged("CanUndoProperty");
            NotifyPropertyChanged("CanRedoProperty");
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void Click_ResetLibraryLayout(object sender, RoutedEventArgs e)
        {
            ResetLibraryLayoutDefault();
            DropDownFile.IsOpen = false;
        }

        private void SearchBar_LostFocus(object sender, RoutedEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputEnabled("searchbar");
        }

        private void SearchBar_GotFocus(object sender, RoutedEventArgs e)
        {
            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("searchbar");
        }

        private void SearchBar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Escape)
            {
                ApplyFilters();

                Keyboard.ClearFocus();
            }
        }

        private void Page_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Keyboard.ClearFocus();
        }

        private void Filter_HideFinished_Checked(object sender, RoutedEventArgs e)
        {
            App.Globals.settings.ToggleLibraryFilter("HideFinished", true);
        }

        private void Filter_HideFinished_Unchecked(object sender, RoutedEventArgs e)
        {
            App.Globals.settings.ToggleLibraryFilter("HideFinished", false);
        }


        private void Filter_ShowHidden_Checked(object sender, RoutedEventArgs e)
        {
            App.Globals.settings.ToggleLibraryFilter("ShowOnlyHidden", true);
            App.Globals.settings.ToggleLibraryFilter("HideHidden", false);
        }

        private void Filter_ShowHidden_Unchecked(object sender, RoutedEventArgs e)
        {
            App.Globals.settings.ToggleLibraryFilter("ShowOnlyHidden", false);
            App.Globals.settings.ToggleLibraryFilter("HideHidden", true);
        }

        private void Button_AddAudiofiles_Click(object sender, RoutedEventArgs e)
        {
            DropDownFile.IsOpen = false;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = IAudiobookScanner.SupportedMediaExtensionsFilter;
            openFileDialog.Multiselect = true;

            if (openFileDialog.ShowDialog() == true)
            {
                List<string> filePaths = openFileDialog.FileNames.ToList<string>();

                /* allow one autosave */
                if (App.Globals.audiobookScanner != null)
                    App.Globals.audiobookScanner.MarkChanged();

                foreach (string file in filePaths)
                {
                    if (Directory.Exists(file))
                    {
                        App.Globals.audiobookScanner.ScanDirectory(file);
                    }
                    else
                        App.Globals.audiobookScanner.AddAudioFile(file);
                }
            }
        }

        private void Button_NewAudiobook_Click(object sender, RoutedEventArgs e)
        {
            Dialog_NewAudiobook newWindow = new Dialog_NewAudiobook();
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_newaudiobook");

            if (newWindow.ShowDialog() == true)
            {
                IAudiobook newAudiobook = App.Globals.audiobookScanner.CreateAudiobook(newWindow.AudiobookNameText);

                if (newAudiobook != null)
                    OnAddedAudiobook(newAudiobook);
            }

            controls.SetInputEnabled("menu_newaudiobook");
        }

        private void Button_ExportLibrary_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "AudiobookSuite Library (*.json)|*.json";

            bool? saveFileResult = saveFileDialog.ShowDialog();
            if (saveFileResult == true)
            {
                string path = saveFileDialog.FileName;

                if (App.Globals.audiobookScanner != null)
                {
                    App.Globals.audiobookScanner.SaveBooksToFile(ESaveCause.Manual, path);
                }
            }
        }

        private void Button_ImportLibrary_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "AudiobookSuite Library (*.json)|*.json";

            bool? openFileResult = openFileDialog.ShowDialog();
            if (openFileResult == true)
            {
                string path = openFileDialog.FileName;

                if (App.Globals.audiobookScanner != null)
                {
                    App.Globals.audiobookScanner.LoadBooksFromFile(path);
                }
            }
        }

        public void OnRemovedGroup(ISortingGroup group)
        {
            if (group == null)
                return;

            for (int i = 0; i < LibraryAudiobooksFlattened.Count(); i++)
            {
                LibraryCollectionItem item =  LibraryAudiobooksFlattened[i];
                if (item != null && item.GroupName.ToLower() == group.GroupName.ToLower())
                {
                    LibraryAudiobooksFlattened.RemoveAt(i);
                }
            }
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("controls/pages/Homepage.xaml", UriKind.Relative));
        }

        private void Button_Settings_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("controls/pages/SettingsPage.xaml", UriKind.Relative));
        }

        private void Button_Help_Click(object sender, RoutedEventArgs e)
        {
            Dialog_GenericConfirmDeny newDiag = new Dialog_GenericConfirmDeny("Open Help Page", "This will open an external browser page to display AudiobookSuite's wiki page.");

            Window parentWindow = Window.GetWindow(this);
            newDiag.Owner = parentWindow;

            IPlayerControls controls = App.Globals.playerControls;
            controls.SetInputDisabled("message");

            if (newDiag.ShowDialog() == true)
            {
                string url = "https://gitlab.com/Shrimperator/AudiobookSuite/-/wikis/home";
                Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
            }

            controls.SetInputEnabled("message");
        }

        private void Click_DeleteGroup(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            if (menuItem == null)
                return;

            Border border = ((ContextMenu)menuItem.Parent).PlacementTarget as Border;
            if (border == null)
                return;

            TextBlock textBlock = border.FindName("TextBlockGroupName") as TextBlock;

            if (textBlock == null)
                return;

            string groupName = textBlock.Text;
            ISortingGroup group = App.Globals.groupManager.GetGroup(groupName);

            OpenDeleteGroupDialog(group);
        }

        private void Click_RenameGroup(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            if (menuItem == null)
                return;

            Border border = ((ContextMenu)menuItem.Parent).PlacementTarget as Border;
            if (border == null)
                return;

            TextBlock textBlock = border.FindName("TextBlockGroupName") as TextBlock;

            if (textBlock == null)
                return;

            string groupName = textBlock.Text;
            ISortingGroup group = App.Globals.groupManager.GetGroup(groupName);

            OpenRenameGroupDialog(group);
        }

        private void LibraryDataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            e.Handled = true;
            SortDataGrid(e.Column);
        }

        private ListSortDirection SortDirection;
        private DataGridColumn SortColumn;

        /* allows to set different default sorting directions (on first click) for specific columns */
        private ListSortDirection GetDefaultSortDirection(DataGridColumn column)
        {
            switch (column.Header.ToString())
            {
                case "Last Played":
                    return ListSortDirection.Descending;
                case "Played":
                    return ListSortDirection.Descending;
                case "Duration":
                    return ListSortDirection.Descending;
                case "Created":
                    return ListSortDirection.Descending;
            }

            return ListSortDirection.Ascending;
        }

        private void SortDataGrid(DataGridColumn column)
        {
            string columnHeader = column.Header.ToString();
            string oldColumnHeader = SortColumn?.Header.ToString();

            if (oldColumnHeader != columnHeader)
                SortDirection = GetDefaultSortDirection(column);
            else if (SortDirection == ListSortDirection.Ascending)
                SortDirection = ListSortDirection.Descending;
            else
                SortDirection = ListSortDirection.Ascending;

            SortColumn = column;
            column.SortDirection = SortDirection;

            try
            {
                using (LibraryCollectionViewSource.View.DeferRefresh())
                {
                    SetupSortingGroups();

                    LibraryCollectionViewSource.SortDescriptions.Clear();
                    LibraryCollectionViewSource.SortDescriptions.Add(new SortDescription("GroupName", ListSortDirection.Ascending));
                    LibraryCollectionViewSource.SortDescriptions.Add(new SortDescription(column.SortMemberPath, SortDirection));
                }
            }
            catch(Exception e)
            {
                DebugLog.WriteLine("SortDataGrid, exception: " + e.Message + "\n\nat " + e.TargetSite + " in " + e.Source);
            }
        }

        private void LibraryDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            SetDataGridLoading(true);
        }

        private void LibraryDataGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            DependencyObject parent = (DependencyObject) e.OriginalSource;

            try
            {
                while ((parent != null) && !(parent is DataGridColumnHeader) && !(parent is DataGridRow))
                    parent = VisualTreeHelper.GetParent(parent);
            }
            catch (Exception ex) { }

            if (parent == null)
                return;

            /* disable input (aka opening context menu), when clicking anything other than a DataGridRow */
            if (!(parent is DataGridRow))
                e.Handled = true;
        }

        private void ClickUndo(object sender, RoutedEventArgs e)
        {
            App.Globals.undoRedo.Undo();
            DropDownEdit.IsOpen = false;
        }

        private void ClickRedo(object sender, RoutedEventArgs e)
        {
            App.Globals.undoRedo.Redo();
            DropDownEdit.IsOpen = false;
        }

        public void OnUndo()
        {
            SetUndoRedoButtonsVisibility();
        }

        public void OnRedo()
        {
            SetUndoRedoButtonsVisibility();
        }

        public void OnPushedUndoAction()
        {
            SetUndoRedoButtonsVisibility();
        }

        public void OnRemovedUndoAction()
        {
            SetUndoRedoButtonsVisibility();
        }

        public bool CanUndoProperty
        {
            get { return ((IUndoRedoHandler)this).CanUndo(); }
        }

        public bool CanRedoProperty
        {
            get { return ((IUndoRedoHandler)this).CanRedo(); }
        }
    }
}
