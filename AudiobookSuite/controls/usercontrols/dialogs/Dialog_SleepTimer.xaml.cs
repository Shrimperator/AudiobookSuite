﻿using AudiobookSuite.Commons;
using AudiobookSuite.lib;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_SleepTimer.xaml
    /// </summary>
    public partial class Dialog_SleepTimer : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public DispatcherTimer UpdateTimer { get; set; }
        public bool IsTimerStarted { get; set; }

        public ISettings Settings { get; set; }
        public IPlayerControls PlayerControlsRef { get; set; }

        private TimeSpan PlusMinusTime = TimeSpan.FromSeconds(60);

        private TimeSpan SleepTimerInterval;
        private TimeSpan SleepTimerWarningInterval;

        private bool IsSleepTimerPaused = false;

        private DispatcherTimer SleepTimerFadeoutHandle = new DispatcherTimer(DispatcherPriority.Render);
        public Stopwatch SleepTimerHandle = new Stopwatch();

        public TimeSpan SleepTimerWarningSound_TimeOffset = TimeSpan.FromMinutes(1);
        private bool WarningPlayed = false;

        public Dialog_SleepTimer()
        {
            DataContext = this;
            IsTimerStarted = false;

            Settings = App.Globals.settings;
            PlayerControlsRef = App.Globals.playerControls;

            SleepTimerFadeoutHandle.Interval = TimeSpan.FromMilliseconds(250);

            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateTimer = new DispatcherTimer(DispatcherPriority.Render);
            UpdateTimer.Interval = TimeSpan.FromMilliseconds(100);
            UpdateTimer.Tick += UpdateTimerTick;
            UpdateTimer.Start();

            WarningSoundVolumeSlider.Value = App.Globals.settings.SleepTimerWarningSoundVolume;

            UpdateTimerTick(this, new EventArgs());

            InitComboBoxState();

            SleepTimerFadeoutHandle.Tick += FadeOutAudio;

            var playerControls = (PlayerControls)App.Globals.playerControls;

            playerControls.OnPause += OnPlayerPause;
            playerControls.OnPlay += OnPlayerPlay;
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            UpdateTimer.Tick -= UpdateTimerTick;
            UpdateTimer.Stop();

            SleepTimerFadeoutHandle.Tick -= FadeOutAudio;

            var playerControls = (PlayerControls)App.Globals.playerControls;

            playerControls.OnPause -= OnPlayerPause;
            playerControls.OnPlay -= OnPlayerPlay;
        }

        private void OnPlayerPause(object? sender, EventArgs e)
        {
            PauseSleepTimer();
        }

        private void OnPlayerPlay(object? sender, EventArgs e)
        {
            ResumeSleepTimer();
        }

        void FadeOutAudio(object sender, EventArgs e)
        {
            var playerControls = (PlayerControls)App.Globals.playerControls;
            if (playerControls == null)
                return;

            int currentVolume = PlayerControls.GlobalMediaPlayer.Volume;
            if (currentVolume > 10)
            {
                PlayerControls.GlobalMediaPlayer.Volume -= App.Globals.settings.SleepTimerFadeOutSpeed;
            }
            else
            {
                DebugLog.WriteLine("Info: finished fading out audio");
                SleepTimerFadeoutHandle.Stop();

                playerControls.Pause();

                // rewind by seconds, because listener is presumably asleep. At least if this was triggered by SleepTimer,
                // which I'm assuming here (because I don't trigger FadeOutAudio from anywhere else yet)
                playerControls.RewindBy(App.Globals.settings.SleepTimerRewindSeconds);
            }
        }

        void SleepTimerElapsed()
        {
            var playerControls = (PlayerControls)App.Globals.playerControls;
            if (playerControls == null)
                return;

            if (SleepTimerHandle.IsRunning)
            {
                SleepTimerHandle.Stop();
                DebugLog.WriteLine("Info: SleepTimer stopped");
            }

            OnSleepTimerStopped();

            if (playerControls.IsPlaying)
            {
                DebugLog.WriteLine("Info: SleepTimer triggered");
                SleepTimerFadeoutHandle.Start();
            }
        }

        private void SleepTimerElapsed_Warning()
        {
            WarningPlayed = true;

            var playerControls = (PlayerControls)App.Globals.playerControls;
            if (playerControls == null)
                return;

            string filePath = AppDomain.CurrentDomain.BaseDirectory + "SoundEffects\\" + App.Globals.settings.SleepTimerWarningSound;
            DebugLog.WriteLine("Info: SleepTimerElapsed_Warning triggered, playing file '" + filePath + "'");

            playerControls.PlaySFXAudioFile(filePath, App.Globals.settings.SleepTimerWarningSoundVolume);
        }

        private void UpdateTimerTick(object? sender, EventArgs e)
        {
            TimeSpan remainingTime = SleepTimerInterval - TimeSpan.FromMilliseconds(SleepTimerHandle.ElapsedMilliseconds);

            if (IsTimerStarted)
            {
                Lbl_RemainingTime.Text = String.Format("{0:hh\\:mm\\:ss}", remainingTime);

                if (!IsSleepTimerPaused)
                {
                    if (remainingTime.TotalMilliseconds <= 0.001d)
                    {
                        SleepTimerElapsed();
                    }
                    else if (!WarningPlayed && remainingTime <= SleepTimerWarningInterval)
                    {
                        SleepTimerElapsed_Warning();
                    }
                }
            }
            else
            {
                Lbl_RemainingTime.Text = "--:--:--";
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void StartSleepTimer(TimeSpan duration)
        {
            var playerControls = (PlayerControls)App.Globals.playerControls;
            if (playerControls == null)
                return;

            if (!SleepTimerHandle.IsRunning && playerControls.IsPlaying && duration.TotalMilliseconds > 0)
            {
                SleepTimerInterval = duration;
                SleepTimerHandle.Restart();

                // in case timer is triggered before fadeout has finished
                SleepTimerFadeoutHandle.Stop();
                PlayerControls.GlobalMediaPlayer.Volume = App.Globals.settings.Volume;

                SleepTimerWarningInterval = SleepTimerWarningSound_TimeOffset;

                OnSleepTimerStarted();
                DebugLog.WriteLine("Info: SleepTimer started");

                OnSleepTimerStarted();
            }
        }

        public void StopSleepTimer()
        {
            if (SleepTimerHandle.IsRunning)
            {
                SleepTimerHandle.Stop();
                OnSleepTimerStopped();
                DebugLog.WriteLine("Info: SleepTimer stopped");

                SleepTimerFadeoutHandle.Stop();

                OnSleepTimerStopped();
            }
        }

        public void PauseSleepTimer()
        {
            if (!IsSleepTimerPaused)
            {
                SleepTimerHandle.Stop();
                OnSleepTimerPaused();
                DebugLog.WriteLine("Info: SleepTimer paused");

                SleepTimerFadeoutHandle.Stop();

                IsSleepTimerPaused = true;
            }
        }

        public void ResumeSleepTimer()
        {
            if (IsSleepTimerPaused)
            {
                SleepTimerHandle.Start();
                DebugLog.WriteLine("Info: SleepTimer resumed");

                IsSleepTimerPaused = false;

                OnSleepTimerResumed();
            }
        }

        private void Button_StartStopTimer_Click(object sender, RoutedEventArgs e)
        {
            if (!IsTimerStarted)
                StartSleepTimer(App.Globals.settings.SleepTimerInterval);
            else
                StopSleepTimer();
        }

        public void OnSleepTimerStarted()
        {
            IsTimerStarted = true;
            NotifyPropertyChanged(nameof(IsTimerStarted));
            WarningPlayed = false;
        }

        public void OnSleepTimerStopped()
        {
            IsTimerStarted = false;
            NotifyPropertyChanged(nameof(IsTimerStarted));
            WarningPlayed = false;
        }

        public void OnSleepTimerPaused()
        {
        }

        public void OnSleepTimerResumed()
        {
            IsTimerStarted = true;
            NotifyPropertyChanged(nameof(IsTimerStarted));
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            App.Globals.playerControls.CloseSleepTimerDialog();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back || e.Key == Key.Return)
            {
                App.Globals.playerControls.CloseSleepTimerDialog();
            }
        }

        private void InitComboBoxState()
        {
            switch (App.Globals.settings.SleepTimerWarningSound)
            {
                case "SleepTimer_Warning_01.mp3":
                    DropDownWarningSound.SelectedIndex = 1;
                    break;
                case "SleepTimer_Warning_02.mp3":
                    DropDownWarningSound.SelectedIndex = 2;
                    break;
            }
        }

        private void DropDownWarningSound_DropDownClosed(object sender, EventArgs e)
        {
            if (!IsInitialized)
                return;

            ComboBoxItem selectedItem = (ComboBoxItem)DropDownWarningSound.SelectedItem;

            string selectedSound = selectedItem.Content.ToString();
            string newFile = "";

            switch (selectedSound)
            {
                default:
                    App.Globals.settings.SleepTimerWarningSound = "";

                    // stop previous sfx playback
                    App.Globals.playerControls.PlaySFXAudioFile("", App.Globals.settings.SleepTimerWarningSoundVolume);
                    return;

                case "SleepTimer_Warning_01":
                    newFile = "SleepTimer_Warning_01.mp3";
                    break;

                case "SleepTimer_Warning_02":
                    newFile = "SleepTimer_Warning_02.mp3";
                    break;
            }

            if (newFile != "")
            {
                App.Globals.settings.SleepTimerWarningSound = newFile;

                string filePath = AppDomain.CurrentDomain.BaseDirectory + "SoundEffects\\" + newFile;
                App.Globals.playerControls.PlaySFXAudioFile(filePath, App.Globals.settings.SleepTimerWarningSoundVolume);
            }
        }

        private void ButtonTimerDec_Click(object sender, RoutedEventArgs e)
        {
            if (IsTimerStarted)
            {
                SleepTimerInterval -= PlusMinusTime;
            }
        }

        private void ButtonTimerInc_Click(object sender, RoutedEventArgs e)
        {
            if (IsTimerStarted)
            {
                SleepTimerInterval += PlusMinusTime;
            }
        }

        private void WindowHeader_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void WarningSoundVolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
    }
}
