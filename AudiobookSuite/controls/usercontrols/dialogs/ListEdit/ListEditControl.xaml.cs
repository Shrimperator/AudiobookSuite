﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace AudiobookSuite.controls.usercontrols
{
    public class EditControlEventArgs : EventArgs
    {
        public ListEditEntry? ChangedEntry;
        public int ChangedIdx;
        public bool handled;

        public EditControlEventArgs(ListEditEntry? changedEntry = null, int changedIdx = -1)
        {
            ChangedEntry = changedEntry;
            ChangedIdx = changedIdx;
        }
    }

    /// <summary>
    /// Interaction logic for ListEdit.xaml
    /// </summary>
    public partial class ListEditControl : UserControl
    {
        public delegate void ChangedDataHandler(int data);
        public List<string> DataList { get; set; }
        public List<ListEditEntry> Entries { get; set; }
        public string DefaultValue;

        public ListEditControl(List<string> dataList, string defaultValue)
        {
            DataList = dataList;
            DefaultValue = defaultValue;

            InitializeComponent();

            ConstructFromData();
        }

        public ListEditControl()
        {
            DataList = new List<string>();
            Entries = new List<ListEditEntry>();
            DefaultValue = "NaN";

            InitializeComponent();
        }

        public void Initialize(List<string> dataList, string defaultValue)
        {
            DataList = dataList;
            DefaultValue = defaultValue;

            ConstructFromData();
        }

        public void ConstructFromData()
        {
            ListAnchor.Children.Clear();
            Entries.Clear();

            for (int i = 0; i < DataList.Count; i++)
            {
                var entry = ConstructEntry(i);
                Entries.Add(entry);
            }
        }

        public bool ContainsEntry(string key)
        {
            return DataList.Contains(key);
        }

        public int AddEntry()
        {
            DataList.Add(DefaultValue);

            ListEditEntry entry = ConstructEntry(DataList.Count - 1);
            Entries.Add(entry);

            OnAddedData(entry.ContainingListIdx);
            return entry.ContainingListIdx;
        }

        public void SetEntry(int idx, string value)
        {
            if (idx < 0 || idx >= DataList.Count)
                return;

            DataList[idx] = value;

            ConstructFromData();
        }

        public void RemoveEntry(ListEditEntry entry)
        {
            DataList.RemoveAt(entry.ContainingListIdx);
            Entries.RemoveAt(entry.ContainingListIdx);

            entry.Remove -= OnEntryClickedRemoved;

            ListAnchor.Children.Remove(entry);
            UpdateIndices();

            OnRemovedData(entry.ContainingListIdx);
        }

        public void RemoveEntry(int idx)
        {
            if (idx < 0 || idx >= DataList.Count)
                return;

            Entries[idx].Remove -= OnEntryClickedRemoved;

            ListAnchor.Children.Remove(Entries[idx]);

            DataList.RemoveAt(idx);
            Entries.RemoveAt(idx);

            UpdateIndices();

            OnRemovedData(idx);
        }

        public void UpdateIndices()
        {
            for (int i = 0; i < DataList.Count && i < ListAnchor.Children.Count; i++)
            {
                ListEditEntry entry = (ListEditEntry)(ListAnchor.Children[i]);
                if (entry != null)
                {
                    entry.ContainingListIdx = i;
                }
            }
        }

        private ListEditEntry ConstructEntry(int idx)
        {
            ListEditEntry entry = new ListEditEntry(this, idx);
            entry.Remove += OnEntryClickedRemoved;

            ListAnchor.Children.Add(entry);

            return entry;
        }

        public void FocusLast()
        {
            if (ListAnchor.Children.Count == 0)
                return;

            ListEditEntry entry = (ListEditEntry) ListAnchor.Children[ListAnchor.Children.Count - 1];

            if (entry != null)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Input,
                new Action(delegate ()
                {
                    entry.DataInput.Focus();
                    Keyboard.Focus(entry.DataInput);
                    entry.DataInput.SelectAll();
                }));
            }
        }

        public void OnEntryClickedRemoved(ListEditEntry entry)
        {
            EditControlEventArgs args = new EditControlEventArgs(entry, entry.ContainingListIdx);
            OnClickedRemoveData(this, args);

            if (!args.handled)
                RemoveEntry(entry);
        }

        private void ButtonAddEntry_Click(object sender, RoutedEventArgs e)
        {
            EditControlEventArgs args = new EditControlEventArgs(null, DataList.Count);
            OnClickedRemoveData(this, args);

            if (!args.handled)
            {
                int entryIdx = AddEntry();
                FocusLast();

                OnUserAddedEntry(entryIdx);
            }
        }

        public event EventHandler ClickedRemoveData;
        public void OnClickedRemoveData(object sender, EditControlEventArgs e)
        {
            ClickedRemoveData?.Invoke(sender, e);
        }

        public event EventHandler ClickedAddData;
        public void OnClickedAddData(object sender, EditControlEventArgs e)
        {
            ClickedAddData?.Invoke(sender, e);
        }

        public event ChangedDataHandler RemovedData;
        public void OnRemovedData(int dataIdx)
        {
            RemovedData?.Invoke(dataIdx);
        }

        public event ChangedDataHandler AddedData;
        public void OnAddedData(int dataIdx)
        {
            AddedData?.Invoke(dataIdx);
        }

        public event ChangedDataHandler UserAddedEntry;
        public void OnUserAddedEntry(int dataIdx)
        {
            UserAddedEntry?.Invoke(dataIdx);
        }
    }
}
