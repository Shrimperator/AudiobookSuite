﻿using AudiobookSuite.Commons;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for ListEditDialog.xaml
    /// </summary>
    public partial class GroupEditDialog : Window
    {
        public List<string> GroupNameList { get; set; }

        public string DefaultValue { get; set; }
        public string DialogTitle { get; set; }

        public GroupEditDialog(List<ISortingGroup> groupList, string Title, string defaultValue)
        {
            GroupNameList = new List<string>();
            foreach (ISortingGroup group in groupList)
                GroupNameList.Add(group.GroupName);

            DefaultValue = defaultValue;
            DialogTitle = Title;

            InitializeComponent();

            _ListEditControl.Initialize(GroupNameList, DefaultValue);
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void ButtonConfirm_Click(object sender, RoutedEventArgs e)
        {
            GroupNameList = _ListEditControl.DataList;

            DialogResult = true;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        private void Button_AddExisting_Click(object sender, RoutedEventArgs e)
        {
            Dialog_SelectGroup newWindow = new Dialog_SelectGroup();
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            if (newWindow.ShowDialog() == true)
            {
                foreach (ISortingGroup group in newWindow.SelectedGroups)
                {
                    if (!(GroupNameList.Contains(group.GroupName)))
                    {
                        GroupNameList.Add(group.GroupName);
                    }
                }

                _ListEditControl.ConstructFromData();
            }
        }
    }
}
