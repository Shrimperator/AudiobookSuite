﻿using AudiobookSuite.Commons;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_ManageBookMarks.xaml
    /// </summary>
    public partial class Dialog_ManageBookMarks : Window, INotifyPropertyChanged
    {
        public IAudiobook AssociatedAudiobook { get; set; }
        public List<IBookMark> TempBookMarks { get; set; }

        public string DialogTitle { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public Dialog_ManageBookMarks(IAudiobook audiobook)
        {
            InitializeComponent();

            AssociatedAudiobook = audiobook;
            TempBookMarks = new List<IBookMark>();

            if (AssociatedAudiobook != null)
            {
                for (int i = 0; i < AssociatedAudiobook.BookMarks.Count; i++)
                {
                    TempBookMarks.Add(AssociatedAudiobook.BookMarks[i]);
                }

                CreateBookMarkLines();

                DialogTitle = "Bookmarks: " + AssociatedAudiobook.Title;
                NotifyPropertyChanged("DialogTitle");
            }
        }

        public void OnRemoveBookMark(BookMarkLine bookMarkLine)
        {
            if (bookMarkLine == null || bookMarkLine.AssociatedBookMark == null)
                return;

            IBookMark mark = bookMarkLine.AssociatedBookMark;
            RemoveBookMark(mark);
        }

        private void ClearBookMarkLines()
        {
            while (Anchor_BookMarks.Children.Count > 0)
            {
                BookMarkLine line = (BookMarkLine) Anchor_BookMarks.Children[Anchor_BookMarks.Children.Count - 1];
                if (line != null)
                    line.Remove -= OnRemoveBookMark;

                Anchor_BookMarks.Children.RemoveAt(Anchor_BookMarks.Children.Count - 1);
            }
        }

        private void CreateBookMarkLines()
        {
            if (AssociatedAudiobook == null)
                return;

            ClearBookMarkLines();

            for (int i = 0; i < TempBookMarks.Count; i++)
            {
                IBookMark mark = TempBookMarks[i];
                BookMarkLine line = new BookMarkLine(mark);
                line.Width = Anchor_BookMarks.Width;

                Anchor_BookMarks.Children.Add(line);

                line.Remove += OnRemoveBookMark;
            }
        }

        public void RemoveBookMark(IBookMark bookMark)
        {
            TempBookMarks.Remove(bookMark);

            CreateBookMarkLines();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void ButtonConfirm_Click(object sender, RoutedEventArgs e)
        {
            foreach (BookMarkLine line in Anchor_BookMarks.Children)
            {
                if (line != null)
                {
                    line.AssociatedBookMark.Identifier = line.TxtBookMarkId.Text;
                }
            }

            DialogResult = true;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
    }
}
