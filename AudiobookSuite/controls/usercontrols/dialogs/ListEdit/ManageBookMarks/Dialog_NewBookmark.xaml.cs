﻿using AudiobookSuite.Commons;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_NewAudiobook.xaml
    /// </summary>
    public partial class Dialog_NewBookMark : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public IAudiobook AssociatedAudiobook;
        public string BookMarkNameText { get; set; }

        /**/

        public Dialog_NewBookMark(IAudiobook audiobook)
        {
            DataContext = this;

            AssociatedAudiobook = audiobook;

            BookMarkNameText = AssociatedAudiobook.GetFreeBookMarkIdentifier();

            InitializeComponent();

            Textbox_BookMarkName.Text = BookMarkNameText;
        }

        /**/

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Create_Click(object sender, RoutedEventArgs e)
        {
            if (BookMarkNameText.Length > 0)
            {
                DialogResult = true;
            }
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Return)
            {
                DialogResult = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Keyboard.Focus(Textbox_BookMarkName);
        }
    }
}