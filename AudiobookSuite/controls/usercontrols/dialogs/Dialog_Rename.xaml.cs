﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_NewAudiobook.xaml
    /// </summary>
    public partial class Dialog_Rename : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string NewNameText { get; set; }

        public Dialog_Rename(string initialText = "")
        {
            DataContext = this;
            NewNameText = initialText;

            InitializeComponent();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Confirm_Click(object sender, RoutedEventArgs e)
        {
            if (NewNameText.Length > 0)
            {
                DialogResult = true;
            }
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Return)
            {
                DialogResult = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Keyboard.Focus(Textbox_AudiobookName);
        }
    }
}
