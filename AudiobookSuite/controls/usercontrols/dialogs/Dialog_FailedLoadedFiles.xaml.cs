﻿using AudiobookSuite.Commons;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_FailedLoadedFiles.xaml
    /// </summary>
    public partial class Dialog_FailedLoadedFiles : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private List<IAudioFile> MissingFiles { get; set; }

        public Dialog_FailedLoadedFiles(List<IAudioFile> missingFiles)
        {
            DataContext = this;
            MissingFiles = missingFiles;

            InitializeComponent();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void InitFailedFileList()
        {
            for (int i = 0; i < MissingFiles.Count; i++)
            {
                IAudioFile file = MissingFiles[i];

                if (file != null)
                {
                    TextBlock fileNameTxt = new TextBlock();
                    fileNameTxt.Text = file.AudioFilePath;
                    fileNameTxt.ToolTip = file.AudioFilePath;
                    FileNamesAnchor.Children.Add(fileNameTxt);

                    if (file.AssociatedAudiobook != null)
                    {
                        TextBlock audiobookNameTxt = new TextBlock();
                        audiobookNameTxt.Text = file.AssociatedAudiobook.Title;
                        audiobookNameTxt.ToolTip = file.AssociatedAudiobook.Title;
                        AudiobookNamesAnchor.Children.Add(audiobookNameTxt);
                    }
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitFailedFileList();
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
        }

        private void WindowHeader_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Confirm_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
