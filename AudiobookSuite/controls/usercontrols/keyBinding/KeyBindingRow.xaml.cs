﻿using AudiobookSuite.Commons;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AudiobookSuite.controls
{
    /// <summary>
    /// Interaction logic for KeyBindingRow.xaml
    /// </summary>
    public partial class KeyBindingRow : UserControl
    {
        public IKeyBinding AssociatedKeyBinding { get; set; }
        public KeyBindingPanel AssociatedPanel { get; set; }

        public KeyBindingRow()
        {
            DataContext = this;
            InitializeComponent();

            Reconstruct();
        }

        public KeyBindingRow(KeyBindingPanel associatedPanel, IKeyBinding keyBinding)
        {
            AssociatedPanel = associatedPanel;
            AssociatedKeyBinding = keyBinding;

            DataContext = this;
            InitializeComponent();

            Reconstruct();
        }

        public void Reconstruct()
        {
            KeyBindingsAnchor.Children.Clear();

            RowTitleText.Text = "None";

            if (AssociatedKeyBinding == null)
                return;

            RowTitleText.Text = AssociatedKeyBinding.Identifier;
            RowTitleText.ToolTip = AssociatedKeyBinding.Identifier;

            foreach (var trigger in AssociatedKeyBinding.Triggers)
            {
                var newCell = new KeyBindingCell(this, trigger);
                KeyBindingsAnchor.Children.Add(newCell);
            }
        }

        public void OnStartEditing(KeyBindingCell callingCell)
        {
            AssociatedPanel.OnStartEditing(callingCell);
        }

        public void OnStopEditing(KeyBindingCell callingCell)
        {
            AssociatedPanel.OnStopEditing(callingCell);
        }

        private void ButtonAddBinding_Click(object sender, RoutedEventArgs e)
        {
            AssociatedKeyBinding.Triggers.Add(new lib.Trigger(Key.None));
            Reconstruct();
        }

        public void RemoveTrigger(KeyBindingCell callingCell)
        {
            for (int i = 0; i < AssociatedKeyBinding.Triggers.Count; i++)
            {
                var currTrigger = AssociatedKeyBinding.Triggers[i];
                if (currTrigger == callingCell.AssociatedTrigger)
                {
                    AssociatedKeyBinding.Triggers.RemoveAt(i);
                    Reconstruct();
                    return;
                }
            }
        }
    }
}
