﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace AudiobookSuite
{
    /// <summary>
    /// Interaction logic for LibraryGridColumn.xaml
    /// </summary>
    public partial class LibraryGridColumn : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /**/

        public LibraryGridColumn()
        {
            DataContext = this;

            InitializeComponent();
        }

        ~LibraryGridColumn()
        {
        }

        public void FinishedInit()
        {
        }

        void OnUnloaded(object sender, EventArgs e)
        {
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
        }

        public void SettingsSetOption(string identifier)
        {
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
