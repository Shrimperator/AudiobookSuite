﻿using System.Collections.Generic;

namespace AudiobookSuite
{
    public class LibraryExpanderBehavior : PersistGroupExpandedStateBehavior
    {
        public override Dictionary<string, bool> GetExpanderStates()
        {
            return App.Globals.settings.LibraryExpanderStates;
        }
    }
}
