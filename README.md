# AudiobookSuite
## Overview
AudiobookSuite is a [WPF](https://wikipedia.org/wiki/Windows_Presentation_Foundation) application with the goal of providing an easy to use local audiobook library and player for modern Windows systems. Most music players are either not geared towards audiobooks at all or they are an afterthought at best.

AudiobookSuite aims to import libraries from disk without any additional work on the side of the user, given no more information than a number of files and their metadata. Multiple related audiofiles should automatically be sorted into the same audiobook and create expander groups from the genre metadata if desired. The goal is a plug-and-play interface for a user's local audiobook library.

Current releases are uploaded to [Microsoft Store](https://apps.microsoft.com/detail/9nf6mh98j2cj). However, at [releases](https://gitlab.com/Shrimperator/AudiobookSuite/-/releases), an older version is available for free. You can also build a current version from source yourself.

## Getting Started
- [Download](https://gitlab.com/Shrimperator/AudiobookSuite/-/releases) the most recent release of the application, extract into a separate folder and start the executable.
- go into the settings and click the big "Change Paths" button.
- in the newly opened dialog, click on the green plus and select where your files are.
	- you may repeat this step to add more directories if you so desire.
- press "confirm" in the dialog
- go to the library and hit the refresh button on the top right to scan for new files and create Audiobooks automatically for you. You can also manually create audiobooks and add files to them, or drag/drop files and folders into the library to add them and create audiobooks from them.
- whenever you add a new library path or add new files, hit the refresh button in the library again to look for new files. AudiobookSuite won't scan for new files automatically.
- if you want to take a closer look at one of the imported audiobooks, right click it in the library and select "details".

## Additional Instructions

Should the scanner ever fail to sort files into a single audiobook correctly, there are two approaches you can take:

1.
- put all the files into one sub folder, and rename the files to something similar, like "audiobook name - <number>". If this still doesn't work, the metadata for the files might be different. You can either find a way to change the metadata, or go with approach 2.
2.
- in the library, click "File" -> "New Audiobook". This will create an empty container where you can add new files to.
- right click your newly created audiobook, and select "details."
- now either drag/drop your files onto this page, or select "File" -> "Add Audiofiles" and import the files from there.

AudiobookSuite caches a lot of data to speed up loading times. This especially applies to file metadata. If files change outside of the application, the displayed metadata might not be up-to-date. This includes titles, authors/narrators and chapter data. In that case you can rightclick the audiobook and select "Reset metadata" - which will read the relevant metadata back from the relevant files on disk.

## Third party software
AudiobookSuite uses [VLC](https://github.com/videolan/libvlcsharp) for its audio playback. If VLC supports an audio format, chances are AudiobookSuite does too.

Audiofile metadata is parsed using [ATL dotnet](https://github.com/Zeugma440/atldotnet)
